<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Product\ProductList;
use App\Http\Controllers\Product\PurchaseList;
use App\Http\Controllers\Account\AccountSetup;
use App\Http\Controllers\Account\Transaction;
use App\Http\Controllers\Product\Quotation;
use App\Http\Controllers\Sales\SalesQuotation;
use App\Http\Controllers\Report\Product;
use App\Http\Controllers\Report\Sales;
use App\Http\Controllers\Report\Customer;
use App\Http\Controllers\Report\Employee;
use App\Http\Controllers\Report\Account;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/index', function () {
    return view('index');
});

Route::get('product-category/edit-category/{id}', function ($id) {
    return view('product.edit-category', ['cat_id' => $id]);
});

Route::get('product-category/groups', 'App\Http\Controllers\Product\CategorySetup@groups');
Route::get('product-category/groupWiseCategory', 'App\Http\Controllers\Product\CategorySetup@groupWiseCategory');
Route::get('product-category/catGroupWiseCategory', 'App\Http\Controllers\Product\CategorySetup@catGroupWiseCategory');
Route::resource('product-category', 'App\Http\Controllers\Product\CategorySetup');

Route::get('/new-product', function () {
    return view('product/product/new-product');
});

Route::get('product/view-product/{id}', function ($id) {
    return view('product/product/view-product', ['pro_id' => $id]);
});
Route::get('product-list/catWiseProduct', 'App\Http\Controllers\Product\ProductList@catWiseProduct');
Route::get('product-list/search', 'App\Http\Controllers\Product\ProductList@search');
Route::resource('product-list', ProductList::class);
Route::prefix('product-list')->group(function () {
    Route::get('/info/{sup_id}/{color_id}/{pro_id}', [ProductList::class, 'info']);
    Route::get('/color/{id}', [ProductList::class, 'colorList']);
});
Route::get('/print-barcode', function () {
    return view('product/product/print-barcode');
});
Route::get('/product-adjustment', function () {
    return view('product/product/product-adjustment');
});
Route::get('/product-stock', function () {
    return view('product/product/product-stock');
});

Route::get('purchase-list/stock', 'App\Http\Controllers\Product\PurchaseList@stock');
Route::post('purchase-list/productAdd', 'App\Http\Controllers\Product\PurchaseList@productAdd');
Route::get('purchase-list/productUpdate', 'App\Http\Controllers\Product\PurchaseList@productUpdate');
Route::get('purchase-list/productColor', 'App\Http\Controllers\Product\PurchaseList@productColor');
Route::get('purchase-list/productRemove', 'App\Http\Controllers\Product\PurchaseList@productRemove');
Route::get('purchase-list/productRemoveAll', 'App\Http\Controllers\Product\PurchaseList@productRemoveAll');
Route::get('purchase-list/invoiceAdd', 'App\Http\Controllers\Product\PurchaseList@invoiceAdd');
Route::get('/purchase-list/edit-product/{id}', [PurchaseList::class, 'product_edit']);
Route::resource('purchase-list', PurchaseList::class);
Route::get('/new-purchase', function () {
    return view('product/purchase/new-purchase');
});
Route::get('/purchase-products', function () {
    return view('product/purchase/product-list');
});

// Route::get('/purchase-list', function () {
//     return view('product/purchase/purchase-list');
// });
Route::get('/purchase-adjustment', function () {
    return view('product/purchase/purchase-adjustment');
});
Route::get('/purchase-return', function () {
    return view('product/purchase/purchase-return');
});
Route::get('/purchase-import', function () {
    return view('product/purchase/purchase-import');
});
Route::get('/sales-pos', function () {
    return view('sales/sales-pos');
});

Route::get('sales-list/productAdd', 'App\Http\Controllers\Sales\SalesList@productAdd');
Route::get('sales-list/productEdit', 'App\Http\Controllers\Sales\SalesList@productEdit');
Route::get('sales-list/productDel', 'App\Http\Controllers\Sales\SalesList@productDel');
Route::get('sales-list/productClear', 'App\Http\Controllers\Sales\SalesList@productClear');
Route::resource('sales-list', 'App\Http\Controllers\Sales\SalesList');
Route::get('/invoice-print', function () {
    return view('sales/sales/sales-list');
});
Route::get('/print-invoice', function () {
    return view('sales/sales/print-invoice');
});
Route::get('/sales-due', function () {
    return view('sales/sales/sales-due');
});
Route::get('/delivery-manage', function () {
    return view('sales/sales/delivery-manage');
});
Route::get('/sales-done', function () {
    return view('sales/sales/sales-done');
});
Route::get('/sales-return', function () {
    return view('sales/sales/sales-return');
});
Route::get('/sales-cancel', function () {
    return view('sales/sales/sales-cancel');
});
Route::get('/sales-import', function () {
    return view('sales/sales/sales-import');
});

Route::get('/new-customer', function () {
    return view('customer/customer/customer-add');
});

Route::resource('customer-list', 'App\Http\Controllers\Customer\CustomerSetup');
// Route::get('customer-list/typesWiseGroup', 'App\Http\Controllers\Setting\CustomerGroup');
Route::get('setting-customer-type/group_info', 'App\Http\Controllers\Setting\CustomerType@typesWiseGroup');
Route::get('/new-user', function () {
    return view('customer/users/users-add');
});
Route::get('/user-list', function () {
    return view('customer/users/users-list');
});
Route::get('/new-biller', function () {
    return view('customer/biller/new-biller');
});
Route::get('/biller-list', function () {
    return view('customer/biller/biller-list');
});
Route::resource('supplier-list', 'App\Http\Controllers\Supplier\SupplierList');
// Route::resource('supplier-list', 'App\Http\Controllers\Supplier\SupplierList', ['names' => [
//     'show' => 'supplier.info'
// ]]);
Route::get('/new-supplier', function () {
    return view('supplier/supplier/new-supplier');
});
// Route::get('/supplier-list', function () {
//     return view('supplier/supplier/supplier-list');
// });
Route::get('/issue-discount', function () {
    return view('supplier/discount/issue-discount');
});
Route::get('/discount-product-list', function () {
    return view('supplier/discount/discount-product-list');
});
Route::resource('employee-department', 'App\Http\Controllers\Employee\DepartmentSetup');
Route::get('/new-employee', function () {
    return view('employee/employee/new-employee');
});
Route::resource('employee-list', 'App\Http\Controllers\Employee\EmployeeList');

Route::get('/daily-attendance', function () {
    return view('employee/attendance/daily-attendance');
});
Route::get('/attendance-history', function () {
    return view('employee/attendance/attendance-history');
});

Route::get('/account-category', function () {
    return view('account/setup/new-category');
});
Route::get('/income-category', function () {
    return view('account/setup/income-category');
});
Route::get('/expense-category', function () {
    return view('account/setup/expense-category');
});
Route::get('{name}/cat-update/{id}', function ($name, $id) {
    return view('account.setup.edit-category', ['cat_id' => $id]);
});
Route::get('/new-transaction', function () {
    return view('account/transaction/new-transaction');
});
Route::get('transaction/categoryList', 'App\Http\Controllers\Account\Transaction@categoryList');
Route::get('transaction/payerInfo', 'App\Http\Controllers\Account\Transaction@payerInfo');
Route::get('transaction/receiverInfo', 'App\Http\Controllers\Account\Transaction@receiverInfo');
Route::resource('transaction', Transaction::class);
Route::resource('account-setup', AccountSetup::class);
Route::get('/income-transaction', function () {
    return view('account/transaction/income-list');
});
Route::get('/expense-transaction', function () {
    return view('account/transaction/expense-list');
});
Route::prefix('account-records')->group(function () {
    Route::resource('account-records', Transaction::class);
    Route::get('/income-records', [Transaction::class, 'income_records']);
    Route::get('/expense-records', [Transaction::class, 'expense_records']);
    Route::get('/balance-sheet', [Transaction::class, 'balance_sheet']);
    Route::get('/daily-statement', [Transaction::class, 'daily_statement']);
    Route::get('/monthly-statement', [Transaction::class, 'monthly_statement']);
    Route::get('/analysis', [Transaction::class, 'account_analysis']);
});
// Report section
Route::prefix('report-product')->group(function () {
    Route::resource('report-product', Product::class);
    Route::get('/stock-details', [Product::class, 'stock_details']);
    Route::get('/minimum-stock', [Product::class, 'minimum_stock']);
    Route::get('/daily-purchase', [Product::class, 'daily_purchase']);
    Route::get('/weekly-purchase', [Product::class, 'weekly_purchase']);
    Route::get('/monthly-purchase', [Product::class, 'monthly_purchase']);
    Route::get('/search-purchase', [Product::class, 'search_purchase']);
});
Route::prefix('report-sales')->group(function () {
    Route::resource('report-sales', Sales::class);
    Route::get('/best-sales', [Sales::class, 'best_sales']);
    Route::get('/daily-sales', [Sales::class, 'daily_sales']);
    Route::get('/weekly-sales', [Sales::class, 'weekly_sales']);
    Route::get('/monthly-sales', [Sales::class, 'monthly_sales']);
    Route::get('/lowest-sales', [Sales::class, 'lowest_sales']);
    Route::get('/search-sales', [Sales::class, 'search_sales']);
});
Route::prefix('report-customer')->group(function () {
    Route::resource('report-customer', Customer::class);
    Route::get('/best-buyer', [Customer::class, 'best_buyer']);
    Route::get('/sales-history', [Customer::class, 'sales_history']);
});
Route::prefix('report-employee')->group(function () {
    Route::resource('report-employee', Employee::class);
    Route::get('/salary', [Employee::class, 'salary_report']);
    Route::get('/leave', [Employee::class, 'leave_report']);
});
Route::prefix('report-account')->group(function () {
    Route::resource('report-account', Account::class);
    Route::get('/purchase', [Account::class, 'purchase_report']);
    Route::get('/sales', [Account::class, 'sales_report']);
    Route::get('/due', [Account::class, 'due_report']);
    Route::get('/income', [Account::class, 'income_report']);
    Route::get('/expense', [Account::class, 'expense_report']);
});

Route::resource('company', 'App\Http\Controllers\Setting\CompanySetup');
Route::resource('setting-role-setup', 'App\Http\Controllers\Setting\RoleSetup');
Route::get('setting-product-brand/brandWiseProduct', 'App\Http\Controllers\Setting\BrandSetup@brandWiseProduct');
Route::resource('setting-product-brand', 'App\Http\Controllers\Setting\BrandSetup');
Route::resource('setting-product-type', 'App\Http\Controllers\Setting\ProductType');
Route::resource('setting-product-group', 'App\Http\Controllers\Setting\GroupSetup');
Route::resource('setting-product-unit', 'App\Http\Controllers\Setting\UnitSetup');
Route::resource('setting-customer-group', 'App\Http\Controllers\Setting\CustomerGroup');
Route::resource('setting-customer-type', 'App\Http\Controllers\Setting\CustomerType');
Route::resource('setting-employee-type', 'App\Http\Controllers\Setting\EmployeeType');
Route::resource('setting-employee-hrm', 'App\Http\Controllers\Setting\HrmSetup');
Route::resource('setting-account-currency', 'App\Http\Controllers\Setting\CurrencySetup');
Route::resource('setting-account-tax', 'App\Http\Controllers\Setting\TaxSetup');
Route::resource('setting-account-payment', 'App\Http\Controllers\Setting\PaymentMethodSetup');
Route::resource('setting-mail-setup', 'App\Http\Controllers\Setting\MailSetup');
Route::resource('setting-pos-setup', 'App\Http\Controllers\Setting\PosSetup');

Route::get('/setting-notification-setup', function () {
    return view('setting/others-setup/notification');
});
Route::get('/setting-download-backup', function () {
    return view('setting/others-setup/download-backup');
});

// Updated 
Route::prefix('purchase-quotation')->group(function () {
    Route::resource('purchase-quotation', Quotation::class);
    Route::get('/list', [Quotation::class, 'index']);
    Route::get('/new', [Quotation::class, 'create']);
    Route::get('/view/{id}', [Quotation::class, 'show']);
    Route::get('/edit/{id}', [Quotation::class, 'edit']);
});
Route::prefix('sales-quotation')->group(function () {
    Route::resource('sales-quotation', SalesQuotation::class);
    Route::get('/list', [SalesQuotation::class, 'index']);
    Route::get('/new', [SalesQuotation::class, 'create']);
    Route::get('/view/{id}', [SalesQuotation::class, 'show']);
    Route::get('/edit/{id}', [SalesQuotation::class, 'edit']);
});

Route::get('barcode/{array}', [ProductList::class, 'barcode']);


//Run this url to create storage link in production - remote server
// Route::get('/linkstorage', function () {
//     Artisan::call('storage:link');
// });
