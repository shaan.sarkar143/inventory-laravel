<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Cus_type;
use App\Models\Setting\Cus_group;

class CustomerType extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cus_types = DB::select('SELECT * FROM `cus_types`');
        return view('setting.customer-setup.customer-type', ['cus_types' => $cus_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = DB::select('SELECT * FROM `cus_types`');
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'group_id' => 'required'
        ]);
        $dataInsert = new Cus_type([
            "name" => $request->get('name'),
            "details" => $request->get('details'),
            "group_id" => $request->get('group_id')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-customer-type';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Cus_type::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Cus_type::find($id);
        $groups = DB::table('cus_groups')->where('status', 1)->get();
        return view('setting.customer-setup.edit-type', ['type' => $result, 'groups' => $groups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'group_id' => 'required'
        ]);
        $cus_type = Cus_type::find($id);
        $cus_type->name = $request->get('name');
        $cus_type->details = $request->get('details');
        $cus_type->group_id = $request->get('group_id');
        try {
            $cus_type->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-customer-type';
            $this->result['message'] = 'Successfully updated data.';
            $this->result['modal'] = 'typeModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            $this->result['modal'] = 'typeModal';
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::table('cus_types')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-customer-type';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    public function typesWiseGroup(Request $request)
    {
        $id = $request->get('cat_id');
        try {
            $getData = Cus_type::find($id);
            $group_id = $getData->group_id;
            $getData = Cus_group::find($group_id);
            $result = ($getData !== null && !empty($getData)) ? $getData : [];
        } catch (Exception $e) {
            $result = $e->getMessage();
        }
        return $result;
    }
}
