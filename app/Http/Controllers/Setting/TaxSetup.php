<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Tax;

class TaxSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::table('taxes')->orderBy('id', 'desc')->get();
        return view('setting.account-setup.tax', ['tax' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = DB::select('SELECT * FROM `taxes`');
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'used_for' => 'required'
        ]);
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'mimes:jpeg,png,jpg'
            ]);

            $request->image->store('public');

            $dataInsert = new Tax([
                "name" => $request->get('name'),
                "used_for" => $request->get('used_for'),
                "image" => $request->image->hashName()
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'setting-account-tax';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $dataInsert = new Tax([
                "name" => $request->get('name'),
                "used_for" => $request->get('used_for')
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'setting-account-tax';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Tax::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Tax::find($id);
        return view('setting.account-setup.edit-tax', ['tax' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'used_for' => 'required'
        ]);
        $tax = Tax::find($id);
        if ($request->hasFile('image')) {
            if ($tax->image != null) {
                $path = public_path("public/storage/" . $tax->image);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $tax->image);
                }
            }
            $request->image->store('public');
            $tax->image = $request->image->hashName();
        }

        $tax->name = $request->get('name');
        $tax->used_for = $request->get('used_for');
        $tax->updated = date("Y-m-d h:i:sa");

        try {
            $tax->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-tax';
            $this->result['message'] = 'Successfully updated data.';
            $this->result['modal'] = 'taxModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['modal'] = 'taxModal';
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getData = DB::table('taxes')->where('id', $id)->first();
        try {
            if ($getData->image != null) {
                $filePath = "public/storage/" . $getData->image;
                if (is_file($filePath) && file_exists($filePath)) {
                    unlink($filePath);
                }
            }
            DB::table('taxes')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-tax';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
