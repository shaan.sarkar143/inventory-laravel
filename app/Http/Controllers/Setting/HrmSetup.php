<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Time_table;

class HrmSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::select('SELECT * FROM `time_tables`');
        return view('setting.employee-setup.hrm-setup', ['time_table' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'day_no' => 'required'
        ]);

        $dataInsert = new Time_table([
            "day_no" => $request->get('day_no'),
            "start_time" => $request->get('start_time'),
            "end_time" => $request->get('end_time')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-employee-hrm';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Time_table::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Time_table::find($id);
        return view('setting.employee-setup.edit-hrm', ['hrm' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'day_no' => 'required'
        ]);
        $time_table = Time_table::find($id);
        $time_table->day_no = $request->get('day_no');
        $time_table->start_time = $request->get('start_time');
        $time_table->end_time = $request->get('end_time');
        try {
            $time_table->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-employee-hrm';
            $this->result['message'] = 'Successfully updated data.';
            $this->result['modal'] = 'hrmModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            $this->result['modal'] = 'hrmModal';
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::table('time_tables')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-employee-hrm';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
