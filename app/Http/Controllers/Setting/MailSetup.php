<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Mail_setup;

class MailSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Mail_setup::orderBy('id')->get();
        return view('setting.others-setup.mail-setup', ['mails' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = Mail_setup::orderBy('id')->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'used_for' => 'required',
            'mail_from' => 'required',
            'reply_to' => 'required'
        ]);

        $dataInsert = new Mail_setup([
            "used_for" => $request->get('used_for'),
            "mail_from" => $request->get('mail_from'),
            "reply_to" => $request->get('reply_to')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-mail-setup';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Mail_setup::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Mail_setup::find($id);
        return view('setting.others-setup.edit-mail', ['mail' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'used_for' => 'required',
            'mail_from' => 'required',
            'reply_to' => 'required'
        ]);

        $mail_setup = Mail_setup::find($id);

        $mail_setup->used_for = $request->get('used_for');
        $mail_setup->mail_from = $request->get('mail_from');
        $mail_setup->reply_to = $request->get('reply_to');
        try {
            $mail_setup->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-mail-setup';
            $this->result['message'] = 'Successfully saved data.';
            $this->result['modal'] = 'mailModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            $this->result['modal'] = 'mailModal';
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mail = Mail_setup::find($id);
            $mail->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-mail-setup';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
