<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Cat_group;

class GroupSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Cat_group::orderBy('id', 'Desc')->get();
        return view('setting.product-setup.group-setup', ['groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = Cat_group::orderBy('id', 'Asc')->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'group_name' => 'required'
        ]);
        $dataInsert = new Cat_group([
            "name" => $request->get('group_name')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-product-group';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Cat_group::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Cat_group::find($id);
        return view('setting.product-setup.group-edit', ['group' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'group_name' => 'required'
        ]);
        $cat_group = Cat_group::find($id);
        $cat_group->name = $request->get('group_name');
        try {
            $cat_group->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-product-group';
            $this->result['message'] = 'Successfully updated data.';
            $this->result['modal'] = 'groupModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            $this->result['modal'] = 'groupModal';
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $catgories = DB::table('categories')->where('group_id', $id)->get();
            foreach ($catgories as $row => $v) {
                $cat_id = $v->id;
                $getData = DB::table('categories')->where('id', $cat_id)->first();
                if ($getData->img_file != null) {
                    $filePath = "public/storage/" . $getData->img_file;
                    if (is_file($filePath) && file_exists($filePath)) {
                        unlink($filePath);
                    }
                }
                DB::table('categories')->where('id', $id)->delete();
            }
            $cat_group = Cat_group::find($id);
            $cat_group->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-product-group';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
