<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Pos_setup;

class PosSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = 1;
        $getData = Pos_setup::find($id);
        return view('setting.sales-setup.pos-setup', ['pos' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = Pos_setup::orderBy('id')->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text1' => 'required',
            'text2' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $pos_setup = new Pos_setup([
            "id" => 1,
            "text1" => $request->get('text1'),
            "text2" => $request->get('text2'),
            "email" => $request->get('email'),
            "web" => $request->get('web'),
            "phone" => $request->get('phone')
        ]);
        try {
            $pos_setup->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-pos-setup';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Pos_setup::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Pos_setup::find($id);
        return view('setting.sales-setup.edit-pos', ['pos' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text1' => 'required',
            'text2' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $pos_setup = Pos_setup::find($id);
        if ($pos_setup == null || empty($pos_setup)) {
            return $this->store($request);
        } else {
            $pos_setup->text1 = $request->get('text1');
            $pos_setup->text2 = $request->get('text2');
            $pos_setup->email = $request->get('email');
            $pos_setup->web = $request->get('web');
            $pos_setup->phone = $request->get('phone');
            try {
                $pos_setup->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'setting-pos-setup';
                $this->result['message'] = 'Successfully updated data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
            return $this->result;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $pos = Pos_setup::find($id);
            $pos->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-pos-setup';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
