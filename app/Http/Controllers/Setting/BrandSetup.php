<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Brand;

class BrandSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = DB::select('SELECT * FROM `brands`');
        return view('setting.product-setup.brands-setup', ['brands' => $brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = DB::select('SELECT * FROM `brands`');
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required'
        ]);
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'mimes:jpeg,png,jpg'
            ]);

            $request->image->store('public');

            $dataInsert = new Brand([
                "name" => $request->get('name'),
                "img_file" => $request->image->hashName()
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'setting-product-brand';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $this->result['status'] = 0;
            $this->result['message'] = 'You must upload brand logo';
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Brand::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Brand::find($id);
        return view('setting.product-setup.brands-edit', ['brands' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $brand = Brand::find($id);
        if ($request->hasFile('image')) {
            if ($brand->image != null) {
                $path = public_path("public/storage/" . $brand->image);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $brand->image);
                }
            }
            $request->image->store('public');
            $brand->img_file = $request->image->hashName();
        }
        $brand->name = $request->get('name');
        try {
            $brand->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-product-brand';
            $this->result['message'] = 'Successfully saved data.';
            $this->result['modal'] = 'brandModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            $this->result['modal'] = 'brandModal';
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getData = DB::table('brands')->where('id', $id)->first();
        try {
            if ($getData->img_file != null) {
                $filePath = "public/storage/" . $getData->img_file;
                if (is_file($filePath) && file_exists($filePath)) {
                    unlink($filePath);
                }
            }
            DB::table('brands')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-product-brand';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    public function brandWiseProduct(Request $request)
    {
        $request->validate([
            'brand_id' => 'required'
        ]);
        $brand_id = $request->get('brand_id');
        $products = DB::table('products')->where('brand_id', $brand_id)->get();
        $this->result['products'] = [];
        $x = 0;
        foreach ($products as $row) {
            $pro_id = $row->id;
            $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
            $this->result['products'][] = $row;
            $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
            $x++;
        }
        return $this->result;
    }
}
