<?php

namespace App\Http\Controllers\Setting;


use App\Http\Controllers\Controller;
use App\Models\Setting\Company;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CompanySetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = DB::select('SELECT * FROM `companies` WHERE `id` = 1');
        return view('setting.company', ['company' => $company]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.company');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        if ($request->hasFile('logo')) {
            $request->validate([
                'logo' => 'mimes:jpeg,png,jpg'
            ]);

            $request->logo->store('public');

            $company = new Company([
                "name" => $request->get('name'),
                "email" => $request->get('email'),
                "phone" => $request->get('phone'),
                "city" => $request->get('city'),
                "state" => $request->get('state'),
                "zip" => $request->get('zip'),
                "address" => $request->get('address'),
                "est_year" => $request->get('est_year'),
                "started" => $request->get('started'),
                "logo" => $request->logo->hashName(),
                "invest" => $request->get('invest'),
                "expense" => $request->get('expense'),
                "income" => $request->get('income'),
                "balance" => $request->get('balance'),
                "amount_due" => $request->get('amount_due'),
                "amount_pay" => $request->get('amount_pay'),
                "invoice_start" => $request->get('invoice_start'),
                "code_start" => $request->get('code_start')
            ]);
            try {
                $company->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'company';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $company = new Company([
                "name" => $request->get('name'),
                "email" => $request->get('email'),
                "phone" => $request->get('phone'),
                "city" => $request->get('city'),
                "state" => $request->get('state'),
                "zip" => $request->get('zip'),
                "address" => $request->get('address'),
                "est_year" => $request->get('est_year'),
                "started" => $request->get('started'),
                "invest" => $request->get('invest'),
                "expense" => $request->get('expense'),
                "income" => $request->get('income'),
                "balance" => $request->get('balance'),
                "amount_due" => $request->get('amount_due'),
                "amount_pay" => $request->get('amount_pay'),
                "invoice_start" => $request->get('invoice_start'),
                "code_start" => $request->get('code_start')
            ]);
            try {
                $company->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'company';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $company = Company::find($id);
        return $company;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Company::find($id);
        return view('setting.edit-company', ['info' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $request->validate([
            'name' => 'required'
        ]);
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->phone = $request->get('phone');
        $company->city = $request->get('city');
        $company->state = $request->get('state');
        $company->zip = $request->get('zip');
        $company->address = $request->get('address');
        $company->est_year = $request->get('est_year');
        $company->started = $request->get('started');
        $company->invest = $request->get('invest');
        $company->expense = $request->get('expense');
        $company->income = $request->get('income');
        $company->balance = $request->get('balance');
        $company->amount_due = $request->get('amount_due');
        $company->amount_pay = $request->get('amount_pay');
        $company->invoice_start = $request->get('invoice_start');
        $company->code_start = $request->get('code_start');
        if ($request->hasFile('logo')) {
            if ($company->logo != null) {
                $path = public_path("public/storage/" . $company->logo);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $company->logo);
                }
            }
            $request->logo->store('public');
            $company->logo = $request->logo->hashName();
        }
        $company->updated = date("Y-m-d h:i:sa");
        try {
            $company->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'company';
            $this->result['modal'] = 'companyModal';
            $this->result['message'] = 'Successfully updated data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['modal'] = 'companyModal';
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
