<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Currency;

class CurrencySetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::table('currencies')->orderBy('id', 'desc')->get();
        return view('setting.account-setup.currency', ['currency' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'amount' => 'required'
        ]);

        $dataInsert = new Currency([
            "name" => $request->get('name'),
            "amount" => $request->get('amount')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-currency';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Currency::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Currency::find($id);
        return view('setting.account-setup.edit-currency', ['currency' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'amount' => 'required'
        ]);
        $currency = Currency::find($id);
        $currency->name = $request->get('name');
        $currency->amount = $request->get('amount');
        try {
            $currency->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-currency';
            $this->result['modal'] = 'currencyModal';
            $this->result['message'] = 'Successfully updated data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['modal'] = 'currencyModal';
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::table('currencies')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-currency';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
