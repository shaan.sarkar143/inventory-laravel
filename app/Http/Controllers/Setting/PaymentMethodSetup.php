<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\Payment_method;

class PaymentMethodSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::select('SELECT * FROM `payment_methods`');
        return view('setting.account-setup.payment-method', ['payment' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'used_for' => 'required'
        ]);
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'mimes:jpeg,png,jpg'
            ]);

            $request->image->store('public');

            $dataInsert = new Payment_method([
                "name" => $request->get('name'),
                "used_for" => $request->get('used_for'),
                "image" => $request->image->hashName()
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'setting-account-payment';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $dataInsert = new Payment_method([
                "name" => $request->get('name'),
                "used_for" => $request->get('used_for')
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'setting-account-payment';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Payment_method::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Payment_method::find($id);
        return view('setting.account-setup.edit-payment', ['payment' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'used_for' => 'required'
        ]);
        $payment_method = Payment_method::find($id);
        if ($request->hasFile('image')) {
            if ($payment_method->image != null) {
                $path = public_path("public/storage/" . $payment_method->image);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $payment_method->image);
                }
            }
            $request->image->store('public');
            $payment_method->image = $request->image->hashName();
        }

        $payment_method->name = $request->get('name');
        $payment_method->used_for = $request->get('used_for');
        try {
            $payment_method->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-payment';
            $this->result['message'] = 'Successfully saved data.';
            $this->result['modal'] = 'paymentModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['modal'] = 'paymentModal';
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getData = DB::table('payment_methods')->where('id', $id)->first();
        try {
            if ($getData->image != null) {
                $filePath = "public/storage/" . $getData->image;
                if (is_file($filePath) && file_exists($filePath)) {
                    unlink($filePath);
                }
            }
            DB::table('payment_methods')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'setting-account-payment';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }
}
