<?php

namespace App\Http\Controllers\Sales;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Sales\Sales_product;
use App\Models\Sales\Sales_list;

class SalesList extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::table('sales_lists')->get();
        return view('sales.sales.sales-list', ['sales' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "invoice" => "required",
            "cus_id" => "required",
            "total_cost" => "required",
            "total_paid" => "required",
            "inv_due" => "required",
            "_token" => "required"
        ]);

        $invoice = $request->get('invoice');
        $cus_id = $request->get('cus_id');
        $balance = $request->get('balance');
        $salestype = 1;
        $insertData = new Sales_list([
            "invoice" => $invoice,
            "customer" => $cus_id,
            "total_cost" => $request->get('total_cost'),
            "total_paid" => $request->get('total_paid'),
            "inv_due" => $request->get('inv_due'),
            "remarks" => $request->get('remarks'),
            "sales_type" => $salestype
        ]);
        try {
            $insertData->save();
            $salesUpdate = DB::table('sales_products')->where('invoice', $invoice)->update(['status' => 1]);
            $customerUpdate = DB::table('cus_lists')->where('id', $cus_id)->update(['balance' => $balance]);
            $this->result['status'] = 1;
            $this->result['load'] = 'sales-pos';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function invoice()
    {
        $invoice = DB::table('sales_lists')->orderBy('id', 'desc')->first('invoice');
        if (empty($invoice)) {
            $invoice = DB::table('companies')->where('id', 1)->first('invoice_start');
            if (!empty($invoice)) {
                return $invoice->invoice_start;
            } else {
                return '';
            }
        } else {
            return $invoice->invoice + 1;
        }
    }

    public function productAdd(Request $request)
    {
        $pro_id = $request->get('pro_id');
        $invoice = $request->get('invoice');
        $pro_info = DB::table('products')->where('id', $pro_id)->first('max_price');
        $price = (!empty($pro_info)) ? $pro_info->max_price : null;
        $pro_qty = DB::table('sales_products')->where('invoice', $invoice)->where('pro_id', $pro_id)->first();
        if (empty($price) || $price == null) {
            $price = (!empty($pro_info)) ? $pro_info->min_price : null;
            if (!empty($price) && $price != null) {
                if (!empty($pro_qty)) {
                    $pro_qty = $pro_qty->pro_qty + 1;
                    $total_cost = $price * $pro_qty;
                    $updateData = array(
                        "pro_qty" => $pro_qty,
                        "pro_price" => $price,
                        "total_cost" => $total_cost,
                        "sale_disc" => 0
                    );
                    DB::table('sales_products')->where('invoice', $invoice)->where('pro_id', $pro_id)->update($updateData);
                } else {
                    $pro_qty = 1;
                    $total_cost = $price * $pro_qty;
                    $insertData = new Sales_product([
                        "invoice" => $invoice,
                        "pro_id" => $pro_id,
                        "pro_qty" => $pro_qty,
                        "pro_price" => $price,
                        "total_cost" => $total_cost,
                        "sale_disc" => 0
                    ]);
                    $insertData->save();
                }
                $salesInfo = DB::table('sales_products')->where('invoice', $invoice)->get();
                $total_product = 0;
                $total_items = 0;
                $total_amount = 0;
                $pro_info = array();
                foreach ($salesInfo as $row) {
                    $id = $row->pro_id;
                    $total_product += 1;
                    $total_items += $row->pro_qty;
                    $total_amount += $row->total_cost;
                    $name = DB::table('products')->where('id', $id)->first('name');
                    if (!empty($name)) {
                        $pro_info[] = [
                            'name' => $name->name
                        ];
                    }
                }
            } else {
                $total_product = 0;
                $total_items = 0;
                $total_amount = 0;
            }
        } else {
            if (!empty($pro_qty)) {
                $pro_qty = $pro_qty->pro_qty + 1;
                $total_cost = $price * $pro_qty;
                $updateData = array(
                    "pro_qty" => $pro_qty,
                    "pro_price" => $price,
                    "total_cost" => $total_cost,
                    "sale_disc" => 0
                );
                DB::table('sales_products')->where('invoice', $invoice)->where('pro_id', $pro_id)->update($updateData);
            } else {
                $pro_qty = 1;
                $total_cost = $price * $pro_qty;
                $insertData = new Sales_product([
                    "invoice" => $invoice,
                    "pro_id" => $pro_id,
                    "pro_qty" => $pro_qty,
                    "pro_price" => $price,
                    "total_cost" => $total_cost,
                    "sale_disc" => 0
                ]);
                $insertData->save();
            }
            $salesInfo = DB::table('sales_products')->where('invoice', $invoice)->get();
            $total_product = 0;
            $total_items = 0;
            $total_amount = 0;
            $pro_info = array();
            foreach ($salesInfo as $row) {
                $id = $row->pro_id;
                $total_product += 1;
                $total_items += $row->pro_qty;
                $total_amount += $row->total_cost;
                $name = DB::table('products')->where('id', $id)->first('name');
                if (!empty($name)) {
                    $pro_info[] = [
                        'name' => $name->name
                    ];
                }
            }
        }
        $totalInfo = array(
            'total_product' => $total_product,
            'total_items' => $total_items,
            'total_amount' => $total_amount
        );
        $this->result['salesInfo'] = !empty($salesInfo) ? $salesInfo : [];
        $this->result['pro_info'] = !empty($pro_info) ? $pro_info : [];
        $this->result['totalInfo'] = !empty($totalInfo) ? $totalInfo : [];
        return $this->result;
    }

    public function productEdit(Request $request)
    {
        $id = $request->get('row_id');
        $product = Sales_product::find($id);
        $product->pro_qty = $request->get('pro_qty');
        $pro_qty = $product->pro_qty;
        $pro_info = DB::table('products')->where('id', $product->pro_id)->first('max_price');
        $total_cost = $pro_info->max_price * $pro_qty;
        $product->total_cost = $total_cost;
        try {
            $product->save();
            $invoice = $product->invoice;
            $salesInfo = DB::table('sales_products')->where('invoice', $invoice)->get();
            $total_product = 0;
            $total_items = 0;
            $total_amount = 0;
            $pro_info = array();
            foreach ($salesInfo as $row) {
                $id = $row->pro_id;
                $total_product += 1;
                $total_items += $row->pro_qty;
                $total_amount += $row->total_cost;
                $name = DB::table('products')->where('id', $id)->first('name');
                $pro_info[] = [
                    'name' => $name->name
                ];
            }
            $totalInfo = array(
                'total_product' => $total_product,
                'total_items' => $total_items,
                'total_amount' => $total_amount
            );
            $this->result['salesInfo'] = $salesInfo;
            $this->result['pro_info'] = $pro_info;
            $this->result['totalInfo'] = $totalInfo;
        } catch (Exception $e) {
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    public function productDel(Request $request)
    {
        $id = $request->get('row_id');
        $product = Sales_product::find($id);
        if (!empty($product)) {
            $invoice = $product->invoice;
            $deleted = DB::table('sales_products')->where('id', $id)->delete();
            $salesInfo = DB::table('sales_products')->where('invoice', $invoice)->get();
            $total_product = 0;
            $total_items = 0;
            $total_amount = 0;
            $pro_info = array();
            foreach ($salesInfo as $row) {
                $id = $row->pro_id;
                $total_product += 1;
                $total_items += $row->pro_qty;
                $total_amount += $row->total_cost;
                $name = DB::table('products')->where('id', $id)->first('name');
                $pro_info[] = [
                    'name' => $name->name
                ];
            }
            $totalInfo = array(
                'total_product' => $total_product,
                'total_items' => $total_items,
                'total_amount' => $total_amount
            );
            $this->result['salesInfo'] = $salesInfo;
            $this->result['pro_info'] = $pro_info;
            $this->result['totalInfo'] = $totalInfo;
        } else {
            $totalInfo = array(
                'total_product' => 0,
                'total_items' => 0,
                'total_amount' => 0
            );
            $this->result['totalInfo'] = $totalInfo;
        }
        return $this->result;
    }

    public function productClear(Request $request)
    {
        $invoice = $request->get('invoice');
        $deleted = DB::table('sales_products')->where('invoice', $invoice)->delete();
        $totalInfo = array(
            'total_product' => 0,
            'total_items' => 0,
            'total_amount' => 0
        );
        $this->result['totalInfo'] = $totalInfo;
        return $this->result;
    }

    public static function product_info($invoice)
    {
        $getData = DB::table('sales_products')->where('invoice', $invoice)->get();
        $total_products = count($getData);
        $total_items = 0;
        foreach ($getData as $row => $v) {
            $total_items += $v->pro_qty;
        }

        $result = array(
            'total_products' => $total_products,
            'total_items' => $total_items,
            'pro_info' => $getData
        );

        return $result;
    }
}
