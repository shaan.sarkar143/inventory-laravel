<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Account\Acc_setup;

class AccountSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type');
        $getData = DB::table('acc_setups')->where('acc_type', $type)->get();
        if ($type == '1') {
            return view('account.setup.income-category', ['category' => $getData]);
        } else if ($type == '2') {
            return view('account.setup.expense-category', ['category' => $getData]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'acc_type' => 'required',
            'cat_name' => 'required'
        ]);
        $dataInsert = new Acc_setup([
            "acc_type" => $request->get('acc_type'),
            "title" => $request->get('cat_name'),
            "remarks" => $request->get('remarks')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'account-category';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }

        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Acc_setup::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'acc_type' => 'required',
            'cat_name' => 'required'
        ]);
        $acc_type = $request->get('acc_type');
        $acc_setup = Acc_setup::find($id);
        $acc_setup->acc_type = $acc_type;
        $acc_setup->title = $request->get('cat_name');
        $acc_setup->remarks = $request->get('remarks');
        $load_url = ($acc_type == 1) ? 'income-category' : (($acc_type == 2) ? 'expense-category' : '');
        try {
            $acc_setup->save();
            $this->result['status'] = 1;
            $this->result['load'] = $load_url;
            $this->result['message'] = 'Successfully saved data.';
            $this->result['modal'] = 'catModal';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            $this->result['modal'] = 'catModal';
        }

        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getData = DB::table('acc_setups')->where('id', $id)->first();
        $type = $getData->acc_type;
        try {
            if ($type == '1') {
                $url = 'income-category';
            } else if ($type == '2') {
                $url = 'expense-category';
            }
            DB::table('acc_setups')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = $url;
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    public static function categoryList($type)
    {
        $result = DB::table('acc_setups')->where('acc_type', $type)->get();
        return $result;
    }
}
