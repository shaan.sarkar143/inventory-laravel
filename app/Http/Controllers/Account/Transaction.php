<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Account\Transactions;

class Transaction extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public static function transactionList($type)
    {
        $result = Transactions::where('trans_type', $type)->get();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'trans_date' => 'required',
            'trans_no' => 'required',
            'trans_type' => 'required',
            'trans_cat' => 'required',
            'pay_from' => 'required',
            'pay_info' => 'required',
            'pay_to' => 'required',
            'rec_info' => 'required',
            'paid_amount' => 'required',
            'remarks' => 'required'
        ]);
        $dataInsert = new Transactions([
            "trans_date" => $request->get('trans_date'),
            "trans_no" => $request->get('trans_no'),
            "trans_type" => $request->get('trans_type'),
            "trans_cat" => $request->get('trans_cat'),
            "pay_from" => $request->get('pay_from'),
            "pay_info" => $request->get('pay_info'),
            "pay_to" => $request->get('pay_to'),
            "rec_info" => $request->get('rec_info'),
            "paid_amount" => $request->get('paid_amount'),
            "remarks" => $request->get('remarks')
        ]);
        try {
            $dataInsert->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'new-transaction';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }

        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function categoryList(Request $request)
    {
        $type = $request->get('type');
        $getData = DB::table('acc_setups')->where('acc_type', $type)->where('status', 1)->get();
        foreach ($getData as $row) {
            $result[] = array(
                'id' => $row->id,
                'name' => ucfirst($row->title)
            );
        }
        return $result;
    }

    public function payerInfo(Request $request)
    {
        $from = $request->get('payer');
        $type = $request->get('acc_type');
        if ($from == 1) {
            $getData = DB::table('payment_methods')->where('used_for', $type)->orwhere('used_for', 0)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->name))
                );
            }
        } else if ($from == 2) {
            $getData = DB::table('cus_lists')->where('status', 1)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->fname)) . ' ' . ucfirst(strtolower($row->lname)) . '-' . $row->phone
                );
            }
        } else if ($from == 3) {
            $getData = DB::table('supplier_profiles')->where('status', 1)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->fname)) . ' ' . ucfirst(strtolower($row->lname)) . '-' . $row->phone
                );
            }
        } else if ($from == 4) {
            $getData = DB::table('emp_lists')->where('status', 1)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->fname)) . ' ' . ucfirst(strtolower($row->lname)) . '-' . $row->phone
                );
            }
        } else if ($from == 5) {
            $result = array(
                array(
                    'id' => $from . '@other',
                    'name' => 'Other'
                )
            );
        }
        return $result;
    }
    public function receiverInfo(Request $request)
    {
        $from = $request->get('receiver');
        $type = $request->get('acc_type');
        if ($from == 1) {
            $getData = DB::table('payment_methods')->where('used_for', $type)->orwhere('used_for', 0)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->name))
                );
            }
        } else if ($from == 2) {
            $getData = DB::table('cus_lists')->where('status', 1)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->fname)) . ' ' . ucfirst(strtolower($row->lname)) . '-' . $row->phone
                );
            }
        } else if ($from == 3) {
            $getData = DB::table('supplier_profiles')->where('status', 1)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->fname)) . ' ' . ucfirst(strtolower($row->lname)) . '-' . $row->phone
                );
            }
        } else if ($from == 4) {
            $getData = DB::table('emp_lists')->where('status', 1)->get();
            foreach ($getData as $row) {
                $result[] = array(
                    'id' => $row->id,
                    'name' => ucfirst(strtolower($row->fname)) . ' ' . ucfirst(strtolower($row->lname)) . '-' . $row->phone
                );
            }
        } else if ($from == 5) {
            $result = array(
                array(
                    'id' => $from . '@other',
                    'name' => 'Other'
                )
            );
        }
        return $result;
    }

    public function income_records()
    {
        return view('account/report/income-records');
    }
    public function expense_records()
    {
        return view('account/report/expense-records');
    }
    public function balance_sheet()
    {
        return view('account/report/balance-sheet');
    }
    public function daily_statement()
    {
        return view('account/report/daily-statement');
    }
    public function monthly_statement()
    {
        return view('account/report/monthly-statement');
    }
    public function account_analysis()
    {
        return view('account/report/account-analysis');
    }
}
