<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Supplier\Supplier_profile;
use App\Models\Supplier\Supplier_business;

class SupplierList extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_data = DB::table('supplier_profiles')->get();
        return view('supplier.supplier.supplier-list', ['suppliers' => $get_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = DB::table('supplier_profiles')->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "first_name" => "required",
            "phone" => "required",
            "email" => "required",
            "address" => "required",
            "zip" => "required",
            "thana" => "required",
            "district" => "required",
            "advanced" => "required",
            "due" => "required",
            "balance" => "required",
            "remarks" => "required"
        ]);

        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'mimes:jpeg,png,jpg'
            ]);

            $request->photo->store('public');

            $insertData = new Supplier_profile([
                "fname" => $request->get('first_name'),
                "lname" => $request->get('last_name'),
                "phone" => $request->get('phone'),
                "email" => $request->get('email'),
                "address" => $request->get('address'),
                "zip" => $request->get('zip'),
                "thana" => $request->get('thana'),
                "district" => $request->get('district'),
                "advanced" => $request->get('advanced'),
                "due" => $request->get('due'),
                "balance" => $request->get('balance'),
                "photo" => $request->photo->hashName(),
                "remarks" => $request->get('remarks')
            ]);
            try {
                $insertData->save();
                $supplier = $insertData->id;
                if ($request->hasFile('attachment')) {
                    $request->validate([
                        'attachment' => 'mimes:csv,txt,xlx,xls,pdf|max:2048'
                    ]);

                    $request->attachment->store('public');
                    $insertData = new Supplier_business([
                        "supplier" => $supplier,
                        "name" => $request->get('name'),
                        "bank_ac" => $request->get('bank_ac'),
                        "trade_no" => $request->get('trade_no'),
                        "tin_bin" => $request->get('tin_bin'),
                        "attachment" => $request->attachment->hashName()
                    ]);
                    try {
                        $insertData->save();
                        $this->result['status'] = 1;
                        $this->result['load'] = 'new-supplier';
                        $this->result['message'] = 'Successfully saved data.';
                    } catch (Exception $e) {
                        $this->result['status'] = 0;
                        $this->result['message'] = $e->getMessage();
                    }
                } else {
                    $insertData = new Supplier_business([
                        "supplier" => $supplier,
                        "name" => $request->get('name'),
                        "bank_ac" => $request->get('bank_ac'),
                        "trade_no" => $request->get('trade_no'),
                        "tin_bin" => $request->get('tin_bin')
                    ]);
                    try {
                        $insertData->save();
                        $this->result['status'] = 1;
                        $this->result['load'] = 'new-supplier';
                        $this->result['message'] = 'Successfully saved data.';
                    } catch (Exception $e) {
                        $this->result['status'] = 0;
                        $this->result['message'] = $e->getMessage();
                    }
                }
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $this->result['status'] = 0;
            $this->result['message'] = 'You have to upload Supplier Photo.';
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Supplier_profile::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Supplier_profile::find($id);
        $business = Supplier_business::where('supplier', $id)->first();
        return view('supplier.supplier.supplier-edit', ['profile' => $profile, 'business' => $business, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function business($id)
    {
        $result = Supplier_business::find($id);
        return $result;
    }
}
