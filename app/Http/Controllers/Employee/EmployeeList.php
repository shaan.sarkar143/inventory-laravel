<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Employee\Emp_list;

class EmployeeList extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_data = DB::table('emp_lists')->get();
        return view('employee.employee.employee-list', ['employee' => $get_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "first_name" => "required",
            "phone" => "required",
            "email" => "required",
            "address" => "required",
            "zip" => "required",
            "thana" => "required",
            "district" => "required",
            "dept_id" => "required",
            "designation" => "required",
            "type_id" => "required",
            "salary" => "required",
            "joining_date" => "required",
            "photo" => "required",
            "attachment" => "required",
            "remarks" => "required"
        ]);

        $fileUpload = 0;

        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'mimes:jpeg,png,jpg'
            ]);

            $request->photo->store('public');
            $fileUpload += 1;
        }
        if ($request->hasFile('attachment')) {
            $request->validate([
                'attachment' => 'mimes:csv,txt,xlx,xls,pdf|max:2048'
            ]);

            $request->attachment->store('public');
            $fileUpload += 1;
        }
        if ($fileUpload == 2) {
            $insertData = new Emp_list([
                "fname" => $request->get('first_name'),
                "lname" => $request->get('last_name'),
                "phone" => $request->get('phone'),
                "email" => $request->get('email'),
                "address" => $request->get('address'),
                "zip" => $request->get('zip'),
                "thana" => $request->get('thana'),
                "district" => $request->get('district'),
                "dept_id" => $request->get('dept_id'),
                "type_id" => $request->get('type_id'),
                "designation" => $request->get('designation'),
                "salary" => $request->get('salary'),
                "joining_date" => $request->get('joining_date'),
                "photo" => $request->photo->hashName(),
                "attachment" => $request->attachment->hashName(),
                "remarks" => $request->get('remarks')
            ]);
            try {
                $insertData->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'new-employee';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $this->result['status'] = 0;
            $this->result['message'] = 'You have to upload Employee Photo and Upload Attachment (CV).';
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
