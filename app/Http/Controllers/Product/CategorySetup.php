<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Product\Category;

class CategorySetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = DB::select('SELECT * FROM `categories`');
        return view('product.category', ['category' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $result = DB::select('SELECT * FROM `categories`');
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cat_name' => 'required',
            'group_id' => 'required'
        ]);
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'mimes:jpeg,png,jpg'
            ]);

            $request->image->store('public');

            $dataInsert = new Category([
                "group_id" => $request->get('group_id'),
                "name" => $request->get('cat_name'),
                "img_file" => $request->image->hashName()
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'product-category';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $dataInsert = new Category([
                "group_id" => $request->get('group_id'),
                "name" => $request->get('cat_name')
            ]);
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['load'] = 'product-category';
                $this->result['message'] = 'Successfully saved data.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Category::find($id);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('product.edit-category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $request->validate([
            'cat_name' => 'required',
            'group_id' => 'required'
        ]);
        $category->group_id = $request->get('group_id');
        $category->name = $request->get('cat_name');
        if ($request->hasFile('image')) {
            if ($category->img_file != null) {
                $path = public_path("public/storage/" . $category->img_file);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $category->img_file);
                }
            }
            $request->image->store('public');
            $category->img_file = $request->image->hashName();
        }
        $category->updated = date("Y-m-d h:i:sa");
        try {
            $category->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'product-category';
            $this->result['modal'] = 'categoryModal';
            $this->result['message'] = 'Successfully updated data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['modal'] = 'categoryModal';
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getData = DB::table('categories')->where('id', $id)->first();
        try {
            if ($getData->img_file != null) {
                $filePath = "public/storage/" . $getData->img_file;
                if (is_file($filePath) && file_exists($filePath)) {
                    unlink($filePath);
                }
            }
            DB::table('categories')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'product-category';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function groups(Request $request)
    {
        $request->validate([
            'cat_id' => 'required'
        ]);
        $cat_id = $request->get('cat_id');
        $cat_info = DB::table('categories')->where('id', $cat_id)->first();
        if ($cat_info !== null) {
            $group_id = $cat_info->group_id;
            $group_info = DB::table('cat_groups')->where('id', $group_id)->first();
            return $this->result = $group_info;
        } else {
            return $this->result = [];
        }
    }

    public static function groupName($id)
    {
        $cat_info = DB::table('categories')->where('id', $id)->first();
        if ($cat_info !== null) {
            $group_id = $cat_info->group_id;
            $group_info = DB::table('cat_groups')->where('id', $group_id)->first();
            return $group_info->name;
        } else {
            return 'N/A';
        }
    }

    public function groupWiseCategory(Request $request)
    {
        $request->validate([
            'group_id' => 'required'
        ]);
        $group = $request->get('group_id');
        $categories = DB::table('categories')->where('group_id', $group)->get();
        $this->result['products'] = [];
        foreach ($categories as $row => $v) {
            $cat_id = $v->id;
            $products = DB::table('products')->where('cat_id', $cat_id)->get();
            $x = 0;
            foreach ($products as $row) {
                $pro_id = $row->id;
                $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $this->result['products'][] = $row;
                $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                $x++;
            }
        }
        $this->result['categories'] = $categories;
        return $this->result;
    }

    public function catGroupWiseCategory(Request $request)
    {
        $request->validate([
            'cat_id' => 'required'
        ]);
        $cat_id = $request->get('cat_id');
        $products = DB::table('products')->where('cat_id', $cat_id)->get();
        $this->result['products'] = [];
        $x = 0;
        foreach ($products as $row) {
            $pro_id = $row->id;
            $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
            $this->result['products'][] = $row;
            $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
            $x++;
        }
        return $this->result;
    }
}
