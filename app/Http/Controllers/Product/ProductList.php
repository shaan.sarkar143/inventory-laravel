<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Product\Product;
use App\Models\Product\Product_color;
use App\Models\Product\PurchaseProduct;

class ProductList extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::select('SELECT * FROM `products`');
        $this->result = $getData;
        $x = 0;
        foreach ($getData as $row) {
            $pro_id = $row->id;
            $colorArray = Product_color::where('pro_id', $pro_id)->get('color');
            foreach ($colorArray as $key => $value) {
                $this->result[$x]->colors[] = $value->color;
            }
            $x++;
        }
        return view('product.product.product-list', ['products' => $this->result]);
    }

    public static function allProducts()
    {
        $getData = DB::select('SELECT * FROM `products`');
        return $getData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create()
    {
        $products = DB::select('SELECT * FROM `products`');
        $x = 0;
        $result = array();
        if (!empty($products)) {
            foreach ($products as $row) {
                $pro_id = $row->id;
                $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $result['products'][] = $row;
                $result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                $x++;
            }
        } else {
            $result['products'] = [];
        }
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "type" => "required",
            "code" => "required",
            "semiology" => "required",
            "cat_id" => "required",
            "brand_id" => "required",
            "colors" => "required",
            "img_1" => "required"
        ]);

        $colors = $request->get('colors');
        $colors = explode(',', $colors);

        if ($request->hasFile('img_1')) {
            $request->validate([
                'img_1' => 'mimes:jpeg,png,jpg'
            ]);

            $request->img_1->store('public');
            if ($request->hasFile('img_2')) {
                if ($request->hasFile('img_3')) {
                    if ($request->hasFile('img_4')) {
                        $request->img_2->store('public');
                        $request->img_3->store('public');
                        $request->img_4->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_2" => $request->img_2->hashName(),
                            "img_3" => $request->img_3->hashName(),
                            "img_4" => $request->img_4->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    } else {
                        $request->img_2->store('public');
                        $request->img_3->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_2" => $request->img_2->hashName(),
                            "img_3" => $request->img_3->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    }
                } else {
                    if ($request->hasFile('img_4')) {
                        $request->img_2->store('public');
                        $request->img_4->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_2" => $request->img_2->hashName(),
                            "img_4" => $request->img_4->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    } else {
                        $request->img_2->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_2" => $request->img_2->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    }
                }
            } else {
                if ($request->hasFile('img_3')) {
                    if ($request->hasFile('img_4')) {
                        $request->img_3->store('public');
                        $request->img_4->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_3" => $request->img_3->hashName(),
                            "img_4" => $request->img_4->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    } else {
                        $request->img_3->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_3" => $request->img_3->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    }
                } else {
                    if ($request->hasFile('img_4')) {
                        $request->img_4->store('public');
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "img_4" => $request->img_4->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    } else {
                        $productData = new Product([
                            "name" => $request->get('name'),
                            "type" => $request->get('type'),
                            "code" => $request->get('code'),
                            "semiology" => $request->get('semiology'),
                            "cat_id" => $request->get('cat_id'),
                            "brand_id" => $request->get('brand_id'),
                            "view_in" => $request->get('view_in'),
                            "img_1" => $request->img_1->hashName(),
                            "remarks" => $request->get('remarks')
                        ]);
                    }
                }
            }
            try {
                $productData->save();
                $last_id = $productData->id;
                foreach ($colors as $row) {
                    $colorData = new Product_color([
                        "color" => $row,
                        "pro_id" => $last_id
                    ]);
                    try {
                        $colorData->save();
                        $this->result['status'] = 1;
                        $this->result['load'] = 'new-product';
                        $this->result['message'] = 'Successfully saved data.';
                    } catch (Exception $e) {
                        $this->destroy($last_id);
                        $this->result['status'] = 0;
                        $this->result['message'] = $e->getMessage();
                    }
                }
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $this->result['status'] = 0;
            $this->result['message'] = 'You have to upload first image for any product.';
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $result = Product::find($id);
        return $result;
    }

    public static function info($sup_id, $color_id, $pro_id)
    {
        $where = [
            'sup_id' => $sup_id,
            'color_id' => $color_id,
            'pro_id' => $pro_id
        ];
        $result_object = array();
        $result = PurchaseProduct::where($where)->first();
        if (!empty($result)) {
            $bsti_info = explode('@', $result->bsti_info);
            $pro_info = Product::find($pro_id)->first('name');
            $result_object = $pro_info;
            $result_object->pro_id = $pro_id;
            $result_object->mrp = $result->sale_max;
            $result_object->mfd = $result->mfd;
            $result_object->exp = $result->exp;
            $result_object->bsti = ($bsti_info[0] == 1) ? 'Yes' : 'No';
            $result_object->bsti_code = ($bsti_info[0] == 1) ? $bsti_info[1] : '0';
            $result_object->bar_code = $sup_id . $color_id . $pro_info->id . $result->id;
            $result_object->sup_id = $sup_id;
            $result_object->color_id = $color_id;
        }
        return $result_object;
    }

    public function colorList($id)
    {
        $colors = DB::table('product_colors')->where('pro_id', $id)->get(['id', 'color']);
        return $colors;
    }

    public static function colors($id)
    {
        $colors = DB::table('product_colors')->where('pro_id', $id)->get('color');
        $colorArray = array();
        foreach ($colors as $key => $value) {
            $colorArray[] = $value->color;
        }
        $result = (!empty($colorArray)) ? implode(',', $colorArray) : 'No color found';
        return $result;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getData = DB::table('products')->where('id', $id)->first();
        $this->result = $getData;
        $colorArray = DB::table('product_colors')->where('pro_id', $id)->get('color');
        foreach ($colorArray as $key => $value) {
            $this->result->colors[] = $value->color;
        }
        return view('product.product.edit-product', ['products' => $this->result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->type = $request->type;
        $product->code = $request->code;
        $product->semiology = $request->semiology;
        $product->cat_id = $request->cat_id;
        $product->brand_id = $request->brand_id;
        $colors = $request->colors;
        $colors = explode(',', $colors);
        $product->view_in = $request->view_in;
        if ($request->hasFile('img_1')) {
            if ($product->img_1 != null) {
                $path = public_path("public/storage/" . $product->img_1);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $product->img_1);
                }
            }
            $request->img_1->store('public');
            $product->img_1 = $request->img_1->hashName();
        }
        if ($request->hasFile('img_2')) {
            if ($product->img_2 != null) {
                $path = public_path("public/storage/" . $product->img_2);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $product->img_2);
                }
            }
            $request->img_2->store('public');
            $product->img_1 = $request->img_2->hashName();
        }
        if ($request->hasFile('img_3')) {
            if ($product->img_3 != null) {
                $path = public_path("public/storage/" . $product->img_3);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $product->img_3);
                }
            }
            $request->img_3->store('public');
            $product->img_1 = $request->img_3->hashName();
        }
        if ($request->hasFile('img_4')) {
            if ($product->img_4 != null) {
                $path = public_path("public/storage/" . $product->img_4);
                $isExists = file_exists($path);
                if ($isExists === true) {
                    unlink("public/storage/" . $product->img_4);
                }
            }
            $request->img_4->store('public');
            $product->img_1 = $request->img_4->hashName();
        }
        $product->remarks = $request->remarks;
        try {
            $product->save();
            Product_color::where('pro_id', $id)->delete();
            foreach ($colors as $row) {
                $colorData = new Product_color([
                    "color" => $row,
                    "pro_id" => $id
                ]);
                try {
                    $colorData->save();
                    $this->result['status'] = 1;
                    $this->result['load'] = 'edit';
                    $this->result['message'] = 'Successfully updated data.';
                } catch (Exception $e) {
                    $this->result['status'] = 0;
                    $this->result['message'] = $e->getMessage();
                }
            }
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getData = Product::find($id);
        try {
            if ($getData->img_1 != null) {
                unlink("public/storage/" . $getData->img_1);
            }
            if ($getData->img_2 != null) {
                unlink("public/storage/" . $getData->img_2);
            }
            if ($getData->img_3 != null) {
                unlink("public/storage/" . $getData->img_3);
            }
            if ($getData->img_4 != null) {
                unlink("public/storage/" . $getData->img_4);
            }
            DB::table('products')->where('id', $id)->delete();
            DB::table('product_stocks')->where('pro_id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'product-list';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    public function addProduct()
    {
        return view('product.product.new-product');
    }

    public function catWiseProduct(Request $request)
    {
        $cat_id = $request->get('cat_id');
        $brand = $request->get('brand');
        if ($brand != '0' && $cat_id != '0') {
            $result = DB::table('products')->where('cat_id', $cat_id)->where('brand_id', $brand)->get();
        } else if ($brand == '0' && $cat_id != '0') {
            $result = DB::table('products')->where('cat_id', $cat_id)->get();
        } else if ($brand != '0' && $cat_id == '0') {
            $result = DB::table('products')->where('brand_id', $brand)->get();
        }
        return $result;
    }

    public function search(Request $request)
    {
        $value = '%' . $request->get('value') . '%';
        $cat_id = $request->get('cat_id');
        $brand_id = $request->get('brand_id');
        $group_id = $request->get('group_id');
        $x = 0;
        if ($cat_id == 0 && $brand_id == 0 && $group_id == 0) {
            $condition = '`name` LIKE "' . $value . '" OR `code` LIKE "' . $value . '"';
            $products = DB::select('SELECT * FROM `products` WHERE ' . $condition);
            foreach ($products as $row) {
                $pro_id = $row->id;
                $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $this->result['products'][] = $row;
                $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                $x++;
            }
        } else if ($cat_id != 0 && $brand_id == 0 && $group_id == 0) {
            $condition = '`cat_id` = "' . $cat_id . '" AND (`name` LIKE "' . $value . '" OR `code` LIKE "' . $value . '")';
            $products = DB::select('SELECT * FROM `products` WHERE ' . $condition);
            foreach ($products as $row) {
                $pro_id = $row->id;
                $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $this->result['products'][] = $row;
                $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                $x++;
            }
        } else if ($cat_id == 0 && $brand_id != 0 && $group_id == 0) {
            $condition = '`brand_id` = "' . $brand_id . '" AND (`name` LIKE "' . $value . '" OR `code` LIKE "' . $value . '")';
            $products = DB::select('SELECT * FROM `products` WHERE ' . $condition);
            foreach ($products as $row) {
                $pro_id = $row->id;
                $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $this->result['products'][] = $row;
                $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                $x++;
            }
        } else if ($cat_id == 0 && $brand_id == 0 && $group_id != 0) {
            $categories = DB::table('categories')->where('group_id', $group_id)->get();
            foreach ($categories as $row => $v) {
                $cat_id = $v->id;
                $condition = '`cat_id` = "' . $cat_id . '" AND (`name` LIKE "' . $value . '" OR `code` LIKE "' . $value . '")';
                $products = DB::select('SELECT * FROM `products` WHERE ' . $condition);
                foreach ($products as $row) {
                    $pro_id = $row->id;
                    $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                    $this->result['products'][] = $row;
                    $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                    $x++;
                }
            }
        } else if ($cat_id != 0 && $brand_id != 0 && $group_id == 0) {
            $condition = '`cat_id` = "' . $cat_id . '" AND `brand_id` = "' . $brand_id . '" AND (`name` LIKE "' . $value . '" OR `code` LIKE "' . $value . '")';
            $products = DB::select('SELECT * FROM `products` WHERE ' . $condition);
            foreach ($products as $row) {
                $pro_id = $row->id;
                $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $this->result['products'][] = $row;
                $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                $x++;
            }
        } else if ($cat_id == 0 && $brand_id != 0 && $group_id != 0) {
            $categories = DB::table('categories')->where('group_id', $group_id)->get();
            foreach ($categories as $row => $v) {
                $cat_id = $v->id;
                $condition = '`cat_id` = "' . $cat_id . '" AND `brand_id` = "' . $brand_id . '" AND (`name` LIKE "' . $value . '" OR `code` LIKE "' . $value . '")';
                $products = DB::select('SELECT * FROM `products` WHERE ' . $condition);
                foreach ($products as $row) {
                    $pro_id = $row->id;
                    $stockInfo = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                    $this->result['products'][] = $row;
                    $this->result['products'][$x]->stock = (!empty($stockInfo)) ? $stockInfo->curr_stock : '--';
                    $x++;
                }
            }
        }


        return $this->result;
    }

    public static function newCode()
    {
        $default = DB::table('companies')->where('id', 1)->first('code_start');
        $checkCode = DB::table('products')->orderBy('id', 'desc')->limit(1)->first('code');
        if (!empty($checkCode)) {
            return $new_code = $checkCode->code + 1;
        } else {
            if (!empty($default)) {
                return $default->code_start;
            } else {
                return '';
            }
        }
    }

    public function barcode($array = [])
    {
        // $this->result = ['123456', '989009', '890769'];
        return view('product.product.barcode-view', ['pro_id' => $array]);
    }
}
