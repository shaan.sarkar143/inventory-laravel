<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Product\PurchaseProduct;
use App\Models\Product\Purchase_list;
use App\Models\Product\Product_stock;
use App\Models\Product\Product_color;
use App\Models\Product\Purs_invoice;

class PurchaseList extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::table('purchase_lists')->get();
        return view('product.purchase.purchase-list', ['purchase' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required',
            'invoice' => 'required',
            'supplier' => 'required',
            'totalCost' => 'required',
            'total_paid' => 'required',
            'due_adv' => 'required',
            'curr_bal' => 'required'
        ]);
        $invoice = $request->get('invoice');
        $supp_id = $request->get('supplier');
        $curr_balance = $request->get('curr_bal');
        $datainsert = new Purchase_list([
            "date" => $request->get('date'),
            "invoice" => $invoice,
            "supplier" => $supp_id,
            "total_cost" => $request->get('totalCost'),
            "total_paid" => $request->get('total_paid'),
            "rest_amount" => $request->get('due_adv'),
            "remarks" => $request->get('remarks'),
            "status" => 1
        ]);
        $supplier = $request->get('supplier');
        try {
            $datainsert->save();
            $purse_id = $datainsert->id;
            $proInfo = DB::table('purchase_products')->where('invoice', $invoice)->where('sup_id', $supp_id)->get();
            foreach ($proInfo as $row => $v) {
                $pro_id = $v->pro_id;
                $color_id = $v->color_id;
                $last_purs = $v->purs_qty;
                $stock_info = DB::table('product_stocks')->where('pro_id', $pro_id)->first();
                $old_stock = (!empty($stock_info)) ? $stock_info->curr_stock : 0;
                $curr_stock = $old_stock + $last_purs;
                $code = DB::table('products')->where('id', $pro_id)->first();
                // $stock = Product_stock::find($pro_id); // This will work only with clumn id
                $dataArray = array(
                    'sup_id' => $supp_id,
                    'color_id' => $color_id,
                    'pro_id' => $pro_id
                );
                $stock = Product_stock::where($dataArray)->value('id');
                if ($stock !== null) {
                    $stock->last_purs = $last_purs;
                    $stock->curr_stock = $curr_stock;
                    $stock->old_stock = $old_stock;
                } else {
                    $stock = new Product_stock([
                        "code" => $code->code,
                        "sup_id" => $supp_id,
                        "color_id" => $color_id,
                        "pro_id" => $pro_id,
                        "last_purs" => $last_purs,
                        "last_sold" => 0,
                        "curr_stock" => $curr_stock,
                        "remarks" => $request->get('remarks')
                    ]);
                }
                try {
                    $stock->save();
                } catch (Exception $e) {
                    $purchase = Purchase_list::find($purse_id);
                    $purchase->delete();
                    $this->result['status'] = 0;
                    $this->result['message'] = $e->getMessage();
                    return $this->result;
                }
            }
            DB::table('supplier_profiles')->where('id', $supplier)->update(['balance' => $curr_balance]);
            DB::table('purs_invoices')->where('invoice', $invoice)->update(['status' => 1]);
            $this->result['status'] = 1;
            $this->result['load'] = 'new-purchase';
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
            return $this->result;
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function product_edit($pro_id)
    {
        $result = PurchaseProduct::find($pro_id);
        $colors = Product_color::where('pro_id', $pro_id)->get();
        return view('product.purchase.purchase-product-edit', ['product' => $result, 'colors' => $colors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = DB::table('purchase_lists')->where('id', $id)->first();
        try {
            DB::table('purchase_lists')->where('id', $id)->delete();
            $this->result['status'] = 1;
            $this->result['load'] = 'product-list';
            $this->result['message'] = 'Successfully Deleted';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    public static function stock($id)
    {
        //
    }

    public static function productAdjustment()
    {
        $products = PurchaseProduct::where('status', '!=', '0')->get();
        return $products;
    }

    public function productAdd(Request $request)
    {
        if ($request->bsti_approve == 1) {
            $bds_no = $request->bds_no;
        } else {
            $bds_no = 0;
        }
        $bds_no = $bds_no;
        $pro_id = $request->get('pro_id');
        $invoice = $request->get('invoice');
        $sup_id = $request->get('supplier');
        $color_id = $request->get('color_id');
        $sale_tax = $request->tax_type . '@' . $request->tax_amount;
        $purse_tax = $request->purse_tax_type . '@' . $request->purse_tax_amount;
        $bsti_info = $request->bsti_approve . '@' . $bds_no;
        $productData = new PurchaseProduct([
            "invoice" => $invoice,
            "sup_id" => $sup_id,
            "color_id" => $color_id,
            "pro_id" => $pro_id,
            "purs_qty" => $request->purchase_qty,
            "purs_unit" => $request->purchase_unit,
            "purs_cost" => $request->purchase_cost,
            "sale_qty" => $request->sale_qty,
            "sale_unit" => $request->sale_unit,
            "sale_min" => $request->sale_min,
            "sale_max" => $request->sale_max,
            "alert_qty" => $request->alert_qty,
            "purs_tax" => $purse_tax,
            "sale_tax" => $sale_tax,
            "bsti_info" => $bsti_info,
            "sale_disc" => $request->sale_disc,
            "discount_type" => $request->discount_type,
            "mfd" => $request->mfd,
            "exp" => $request->exp,
        ]);
        $proArray = array(
            'invoice' => $invoice,
            'sup_id' => $sup_id,
            'pro_id' => $pro_id,
            'color_id' => $color_id
        );
        $checkData = DB::table('purchase_products')->where($proArray)->first();
        if ($checkData !== NULL) {
            $id = $checkData->id;
        } else {
            $id = '';
        }
        if (isset($id) && !empty($id)) {
            $product = PurchaseProduct::find($id);
            $product->color_id = $color_id;
            $product->purs_cost = $request->purchase_cost;
            $product->purs_qty = $request->purchase_qty;
            $product->purs_unit = $request->purchase_unit;
            $product->sale_max = $request->sale_max;
            $product->sale_min = $request->sale_min;
            $product->sale_qty = $request->sale_qty;
            $product->sale_unit = $request->sale_unit;
            $product->alert_qty = $request->alert_qty;
            $product->exp = $request->exp;
            $product->mfd = $request->mfd;
            $product->purs_tax = $purse_tax;
            $product->sale_tax = $sale_tax;
            $product->bsti_info = $bsti_info;
            $product->discount_type = $request->discount_type;
            $product->sale_disc = $request->sale_disc;
            try {
                $product->save();
            } catch (Exception $e) {
                $this->result['message'] = $e->getMessage();
            }
        } else {
            try {
                $productData->save();
            } catch (Exception $e) {
                $this->result['message'] = $e->getMessage();
            }
        }

        $dataArray = array(
            'invoice' => $invoice,
            'sup_id' => $sup_id
        );
        $getData = DB::table('purchase_products')->where($dataArray)->get();
        $table_data[] = array();
        $x = 0;
        $grand_total = 0;
        if (count($getData) > 0) {
            foreach ($getData as $row => $v) {
                $table_data[$x]['row_id'] = $v->id;

                $color_info = Product_color::find($v->color_id);
                $table_data[$x]['pro_color'] = $color_info['color'];

                $pro_info = DB::table('products')->where('id', $v->pro_id)->first();
                $total_cost = ($v->purs_qty * $v->purs_cost);
                $grand_total += $total_cost;
                $table_data[$x]['pro_info'] = $pro_info;
                $table_data[$x]['purs_cost'] = $v->purs_cost;
                $table_data[$x]['purs_qty'] = $v->purs_qty;
                $table_data[$x]['total_cost'] = $total_cost;

                $unit_id = $v->purs_unit;
                $unit_info = DB::table('weight_units')->where('id', $unit_id)->first();
                $table_data[$x]['purs_units'] = $unit_info->name;
                $table_data[$x]['sale_max'] = $v->sale_max;
                $table_data[$x]['sale_min'] = $v->sale_min;
                $table_data[$x]['sale_qty'] = $v->sale_qty;

                $unit_id = $v->sale_unit;
                $unit_info = DB::table('weight_units')->where('id', $unit_id)->first();
                $table_data[$x]['sale_units'] = $unit_info->name;
                $table_data[$x]['exp'] = date('d-m-Y ', strtotime($v->exp));
                $table_data[$x]['mfd'] = date('d-m-Y', strtotime($v->mfd));

                $purs_tax = $this->explode('@', $v->purs_tax);
                $tax_id = $purs_tax[0];
                $tax_info = DB::table('taxes')->where('id', $tax_id)->first();
                $table_data[$x]['purs_tax_amount'] = $purs_tax[1];
                $table_data[$x]['purs_tax_title'] = $tax_info->name;

                $sale_tax = $this->explode('@', $v->sale_tax);
                $tax_id = $sale_tax[0];
                $tax_info = DB::table('taxes')->where('id', $tax_id)->first();

                $table_data[$x]['sale_tax_amount'] = $sale_tax[1];
                $table_data[$x]['sale_tax_title'] = $tax_info->name;

                if ($v->discount_type == '0') {
                    $table_data[$x]['sale_disc'] = '0.00';
                } else if ($v->discount_type == '1') {
                    $table_data[$x]['sale_disc'] = '-' . $v->sale_disc;
                } else if ($v->discount_type == '2') {
                    $table_data[$x]['sale_disc'] = $v->sale_disc . '%';
                }
                $x++;
            }
            $this->result['data']['product'] = $table_data;
        } else {
            $this->result['data']['product'] = [];
        }
        $this->result['data']['grand_total'] = $grand_total;
        $this->result['table'] = 'product_list';
        $this->result['modal'] = 'productModal';
        return $this->result;
    }

    public function productUp(Request $request, $id)
    {
        $product = PurchaseProduct::find($id);
        $product->pro_qty = $request->get('pro_qty');
        $product->pro_unit = $request->get('pro_unit');
        $product->pro_cost = $request->get('pro_cost');
        $product->sale_disc = $request->get('sale_disc');
        $product->save();
        // return 'success';
    }

    public function productColor(Request $request)
    {
        $id = $request->get('id');
        $product = DB::table('product_colors')->where('pro_id', $id)->get();
        return $product;
    }

    public function invoiceAdd(Request $request)
    {
        $inv_no = $request->get('invoice');
        $dataInsert = new Purs_invoice([
            "invoice" => $inv_no
        ]);
        $invoice = Purs_invoice::where('invoice', $inv_no)->value('invoice');
        if ($invoice === null) {
            try {
                $dataInsert->save();
                $this->result['status'] = 1;
                $this->result['message'] = 'Invoice no. successfully added.';
            } catch (Exception $e) {
                $this->result['status'] = 0;
                $this->result['message'] = $e->getMessage();
            }
        } else {
            $this->result['status'] = 0;
            $this->result['message'] = 'Already added to invoice list.';
        }
        $status = 0;
        $invoice_list = DB::table('purs_invoices')->where('status', $status)->orderBy('id', 'desc')->get();
        $this->result['invoices'] = $invoice_list;
        $this->result['check'] = $invoice;
        return $this->result;
    }

    public static function invoiceForPurchase()
    {
        $status = 0;
        $invoices = DB::table('purs_invoices')->where('status', $status)->orderBy('id', 'desc')->get();
        return $invoices;
    }

    public function productUpdate(Request $request)
    {

        return view('product.purchase.purchase-product-edit');
        // $id = $request->get('row_id');
        // $product = PurchaseProduct::find($id);
        // $product->pro_qty = $request->get('pro_qty');
        // $product->pro_unit = $request->get('pro_unit');
        // $product->pro_cost = $request->get('pro_cost');
        // $product->sale_disc = $request->get('sale_disc');
        // $totalQty = 0;
        // $totalCost = 0;
        // $totalDiscount = 0;
        // try {
        //     $product->save();
        //     $checkData = DB::table('purchase_products')->where('id', $id)->first();
        //     $invoice = $checkData->invoice;
        //     $supp_id = $checkData->supplier;
        //     $purchaseProduct = DB::table('purchase_products')->where('invoice', $invoice)->where('sup_id', $supp_id)->get();
        //     foreach ($purchaseProduct as $row => $v) {
        //         $totalQty += $v->pro_qty;
        //         $pro_cost = $v->pro_cost * $totalQty;
        //         $sale_disc = $v->sale_disc; // * $totalQty;
        //         $totalCost += $pro_cost;
        //         $totalDiscount += $sale_disc;
        //     }
        //     $this->result['total'] = array(
        //         'totalQty' => $totalQty,
        //         'totalCost' => $totalCost,
        //         'totalDisc' => $totalDiscount,
        //         'grandTotal' => $totalDiscount
        //     );
        // } catch (Exception $e) {
        //     $this->result['message'] = $e->getMessage();
        // }
        // return $this->result;
    }

    public function productRemove(Request $request)
    {
        $id = $request->get('row_id');
        $product = PurchaseProduct::find($id);
        $grandTotal = 0;
        $totalCost = 0;
        $checkData = DB::table('purchase_products')->where('id', $id)->first();
        if ($checkData !== NULL) {
            $invoice = $checkData->invoice;
            $supp_id = $checkData->sup_id;
            try {
                $product->delete();
                $purchaseProduct = DB::table('purchase_products')->where('invoice', $invoice)->where('sup_id', $supp_id)->get();
                foreach ($purchaseProduct as $row => $v) {
                    $totalCost = $totalCost + ($v->purs_qty * $v->purs_cost);
                    $grandTotal += $totalCost;
                }
                $this->result['total'] = array(
                    'grandTotal' => $grandTotal
                );
            } catch (Exception $e) {
                $this->result['message'] = $e->getMessage();
            }
            return $this->result;
        }
    }

    public function productRemoveAll(Request $request)
    {
        $invoice = $request->get('invoice');
        $supplier = $request->get('supplier');
        $checkData = DB::table('purchase_products')->where('invoice', $invoice)->where('sup_id', $supplier)->get();
        foreach ($checkData as $row => $v) {
            $id = $v->id;
            $product = PurchaseProduct::find($id);
            try {
                $product->delete();
            } catch (Exception $e) {
                $this->result['message'] = $e->getMessage();
            }
        }
        $this->result['total'] = array(
            'grandTotal' => 0
        );
        return $this->result;
    }

    public static function productInfo($invoice, $supplier)
    {
        $purchaseProduct = DB::table('purchase_products')->where('invoice', $invoice)->where('sup_id', $supplier)->get();
        $totalItems = 0;
        $totalProduct = 0;
        foreach ($purchaseProduct as $row => $v) {
            $totalItems += $v->purs_qty;
            $totalProduct += 1;
        }
        $result['total'] = array(
            'totalItems' => $totalItems,
            'totalProduct' => $totalProduct
        );
        return $result;
    }

    private function explode($sign, $data)
    {
        return $arrayData = explode($sign, $data);
    }
}
