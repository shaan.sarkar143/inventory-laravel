<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Customer\Cus_list;

class CustomerSetup extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::select('SELECT * FROM `cus_lists`');
        return view('customer.customer.customer-list', ['customer' => $getData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'group_id' => 'required',
            'type_id' => 'required',
            'balance' => 'required'
        ]);
        $datainsert = new Cus_list([
            "fname" => $request->get('first_name'),
            "lname" =>  $request->get('last_name'),
            "phone" => $request->get('phone'),
            "email" => $request->get('email'),
            "address" => $request->get('address'),
            "city" => $request->get('city'),
            "state" => $request->get('state'),
            "zip" => $request->get('zip'),
            "group_id" => $request->get('group_id'),
            "type_id" => $request->get('type_id'),
            "advanced" => $request->get('advanced'),
            "due" => $request->get('due'),
            "balance" => $request->get('balance'),
            "remarks" => $request->get('remarks')
        ]);
        try {
            $datainsert->save();
            $this->result['status'] = 1;
            $this->result['message'] = 'Successfully saved data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $cus_info = Cus_list::find($id);
        $cus_type = DB::table('cus_types')->where('id', $cus_info->type_id)->first('name');
        $result = array();
        $result['cus_info'] = $cus_info;
        $result['cus_type'] = $cus_type;
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cus_info = Cus_list::find($id);
        $cus_types = $this->cus_types();
        return view('customer.customer.customer-edit', ['info' => $cus_info, 'types' => $cus_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Cus_list = Cus_list::find($id);
        $request->validate([
            'first_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'group_id' => 'required',
            'type_id' => 'required',
            'balance' => 'required'
        ]);

        $Cus_list->fname = $request->get('first_name');
        $Cus_list->lname = $request->get('last_name');
        $Cus_list->phone = $request->get('phone');
        $Cus_list->email = $request->get('email');
        $Cus_list->address = $request->get('address');
        $Cus_list->city = $request->get('city');
        $Cus_list->state = $request->get('state');
        $Cus_list->zip = $request->get('zip');
        $Cus_list->group_id = $request->get('group_id');
        $Cus_list->type_id = $request->get('type_id');
        $Cus_list->advanced = $request->get('advanced');
        $Cus_list->due = $request->get('due');
        $Cus_list->balance = $request->get('balance');
        $Cus_list->remarks = $request->get('remarks');

        try {
            $Cus_list->save();
            $this->result['status'] = 1;
            $this->result['load'] = 'customer-list';
            $this->result['modal'] = 'cusModal';
            $this->result['message'] = 'Successfully updated data.';
        } catch (Exception $e) {
            $this->result['status'] = 0;
            $this->result['modal'] = 'cusModal';
            $this->result['message'] = $e->getMessage();
        }
        return $this->result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function customerList()
    {
        $getData = DB::table('cus_lists')->where('status', 1)->get();
        return $getData;
    }

    private function cus_types()
    {
        $cus_types = DB::select('SELECT * FROM `cus_types`');
        return $cus_types;
    }
}
