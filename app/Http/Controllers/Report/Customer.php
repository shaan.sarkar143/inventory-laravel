<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Customer extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }

    public function best_buyer()
    {
        return view('report/customer/best-buyer');
    }
    public function sales_history()
    {
        return view('report/customer/sales-history');
    }
}
