<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Account extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }

    public function purchase_report()
    {
        return view('report/accounts/purchase-report');
    }
    public function sales_report()
    {
        return view('report/accounts/sales-report');
    }
    public function income_report()
    {
        return view('report/accounts/income-report');
    }
    public function expense_report()
    {
        return view('report/accounts/expense-report');
    }
    public function due_report()
    {
        return view('report/accounts/due-report');
    }
}
