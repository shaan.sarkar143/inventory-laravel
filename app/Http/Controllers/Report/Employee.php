<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Employee extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }

    public function salary_report()
    {
        return view('report/employee/salary-report');
    }
    public function leave_report()
    {
        return view('report/employee/leave-report');
    }
}
