<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Product extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }

    public function stock_details()
    {
        return view('report/product/products-stock');
    }
    public function minimum_stock()
    {
        return view('report/product/minimum-stock');
    }
    public function daily_purchase()
    {
        return view('report/product/daily-purchase');
    }
    public function weekly_purchase()
    {
        return view('report/product/weekly-purchase');
    }
    public function monthly_purchase()
    {
        return view('report/product/monthly-purchase');
    }
    public function search_purchase()
    {
        return view('report/product/search-purchase');
    }
}
