<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Sales extends Controller
{
    public function __construct()
    {
        $this->result = array();
    }

    public function best_sales()
    {
        return view('report/sales/best-sales');
    }
    public function daily_sales()
    {
        return view('report/sales/daily-sales');
    }
    public function weekly_sales()
    {
        return view('report/sales/weekly-sales');
    }
    public function monthly_sales()
    {
        return view('report/sales/monthly-sales');
    }
    public function lowest_sales()
    {
        return view('report/sales/lowest-sales');
    }
    public function search_sales()
    {
        return view('report/sales/search-sales');
    }
}
