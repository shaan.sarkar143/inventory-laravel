<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emp_list extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["name", "phone", "email", "address", "thana", "district", "zip", "dept_id", "type_id", "designation", "salary", "joining_date", "photo", "attachment", "remarks"];
}
