<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emp_dept extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["name", "about", "email", "contact_no", "image"];
}
