<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pos_setup extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["text1", "text2", "email", "phone"];
}
