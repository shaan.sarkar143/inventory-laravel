<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail_setup extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["used_for", "mail_from", "reply_to"];
}
