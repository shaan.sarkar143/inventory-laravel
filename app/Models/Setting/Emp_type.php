<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emp_type extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["name", "details"];
}
