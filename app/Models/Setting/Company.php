<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        "name", "email", "phone", "city", "state", "zip", "address", "est_year", "invoice_start", "code_start",
        "started", "logo", "invest", "expense", "income", "balance", "amount_due", "amount_pay"
    ];
}
