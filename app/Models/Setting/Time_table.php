<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Time_table extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["day_no", "start_time", "end_time"];
}
