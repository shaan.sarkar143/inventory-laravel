<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["trans_date", "trans_no", "trans_type", "trans_cat", "pay_from", "pay_info", "pay_to", "rec_info", "paid_amount", "remarks"];
}
