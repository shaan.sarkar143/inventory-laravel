<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acc_setup extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["acc_type", "title", "remarks"];
}
