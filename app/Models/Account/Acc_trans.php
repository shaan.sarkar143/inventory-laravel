<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acc_trans extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["trans_date", "trans_no", "receiver", "payer", "amount", "acc_type", "acc_category", "remarks"];
}
