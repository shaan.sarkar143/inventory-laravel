<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier_business extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["supplier", "name", "bank_ac", "trade_no", "tin_bin", "attachment"];
}
