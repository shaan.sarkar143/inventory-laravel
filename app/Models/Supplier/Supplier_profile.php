<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier_profile extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["fname", "lname", "phone", "email", "address", "thana", "district", "zip", "photo", "advanced", "due", "balance", "remarks"];
}
