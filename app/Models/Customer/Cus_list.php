<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cus_list extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["fname", "phone", "email", "address", "city", "state", "zip", "group_id", "type_id", "advanced", "due", "balance", "remarks"];
}
