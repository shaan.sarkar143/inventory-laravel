<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase_list extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["date", "invoice", "supplier", "total_cost", "total_paid", "rest_amount", "remarks"];
}
