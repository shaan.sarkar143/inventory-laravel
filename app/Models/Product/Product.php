<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        "name", "type", "code", "semiology", "cat_id", "brand_id", "view_in", "img_1", "img_2", "img_3", "img_4"
    ];
}
