<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewPurchase extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["invoice", "supplier", "total_cost", "total_paid", "rest_amount", "remarks"];
}
