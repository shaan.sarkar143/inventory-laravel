<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_color extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        "pro_id", "color"
    ];
}
