<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseProduct extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'invoice', 'sup_id', 'color_id', 'pro_id', 'purs_qty', 'purs_unit', 'purs_cost', 'sale_qty', 'sale_unit', 'sale_min', 'sale_max',
        'alert_qty', 'purs_tax', 'sale_tax', 'bsti_info', 'sale_disc', 'discount_type', 'mfd', 'exp'
    ];
}
