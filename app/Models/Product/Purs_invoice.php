<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purs_invoice extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["invoice"];
}
