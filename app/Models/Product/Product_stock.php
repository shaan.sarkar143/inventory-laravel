<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_stock extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["code", "color_id", "sup_id", "pro_id", "old_stock", "last_purs", "last_sold", "curr_stock", "remarks"];
}
