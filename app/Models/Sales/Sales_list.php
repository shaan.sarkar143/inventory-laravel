<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales_list extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["invoice", "customer", "total_cost", "total_paid", "inv_due", "remarks", "sales_type"];
}
