<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales_product extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ["invoice", "pro_id", "pro_qty", "pro_price", "total_cost", "sale_disc"];
}
