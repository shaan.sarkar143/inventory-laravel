<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="<?= config('app.url') ?>assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Inventory</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-first-page'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li>
            <a href="{{ url('index') }}">
                <div class="parent-icon"><i class='bx bx-home-alt'></i>
                </div>
                <div class="menu-title">Dashboard</div>
            </a>
        </li>
        <li class="menu-label">Product Management</li>
        <li>
            <a href="{{ url('product-category') }}">
                <div class="parent-icon"><i class='bx bx-home'></i>
                </div>
                <div class="menu-title">Category</div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='lni lni-producthunt'></i>
                </div>
                <div class="menu-title">Product</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('new-product') }}"><i class="bx bx-right-arrow-alt"></i>Add New Product</a>
                </li>
                <li>
                    <a href="{{ url('product-list') }}"><i class="bx bx-right-arrow-alt"></i>Product List</a>
                </li>
                <li>
                    <a href="{{ url('print-barcode') }}"><i class="bx bx-right-arrow-alt"></i>Print Barcode</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-purchase-tag-alt'></i>
                </div>
                <div class="menu-title">Purchase</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('new-purchase') }}"><i class="bx bx-right-arrow-alt"></i>New Purchase</a>
                </li>
                <li>
                    <a href="{{ url('purchase-list') }}"><i class="bx bx-right-arrow-alt"></i>Purchase List</a>
                </li>
                <li>
                    <a href="{{ url('purchase-return') }}"><i class="bx bx-right-arrow-alt"></i>Purchase Return</a>
                </li>
                <li>
                    <a href="{{ url('product-adjustment') }}"><i class="bx bx-right-arrow-alt"></i>Product
                        Adjustment</a>
                </li>
                <li>
                    <a href="{{ url('product-stock') }}"><i class="bx bx-right-arrow-alt"></i>Product Stock</a>
                </li>
                <li>
                    <a href="{{ url('purchase-import') }}"><i class="bx bx-right-arrow-alt"></i>Import Records</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='lni lni-producthunt'></i>
                </div>
                <div class="menu-title">Purchase Quotation</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('purchase-quotation/new') }}"><i class="bx bx-right-arrow-alt"></i>New
                        Quotation</a>
                </li>
                <li>
                    <a href="{{ url('purchase-quotation/list') }}"><i class="bx bx-right-arrow-alt"></i>Quotation
                        List</a>
                </li>
            </ul>
        </li>
        <li class="menu-label">Sales Management</li>
        <li>
            <a href="{{ url('sales-pos') }}">
                <div class="parent-icon"><i class='bx bx-store'></i>
                </div>
                <div class="menu-title">POS</div>
            </a>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-cart"></i>
                </div>
                <div class="menu-title">Sales</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('sales-hold') }}"><i class="bx bx-right-arrow-alt"></i>Hold List</a>
                </li>
                <li>
                    <a href="{{ url('sales-list') }}"><i class="bx bx-right-arrow-alt"></i>Sales List</a>
                </li>
                <li>
                    <a href="{{ url('delivery-manage') }}"><i class="bx bx-right-arrow-alt"></i>Delivery Manage</a>
                </li>
                <li>
                    <a href="{{ url('sales-due') }}"><i class="bx bx-right-arrow-alt"></i>Sales Due</a>
                </li>
                <li>
                    <a href="{{ url('sales-done') }}"><i class="bx bx-right-arrow-alt"></i>Sales Done</a>
                </li>
                <li>
                    <a href="{{ url('sales-return') }}"><i class="bx bx-right-arrow-alt"></i>Sales Return</a>
                </li>
                <li>
                    <a href="{{ url('sales-cancel') }}"><i class="bx bx-right-arrow-alt"></i>Sales Cancel</a>
                </li>
                <li>
                    <a href="{{ url('sales-import') }}"><i class="bx bx-right-arrow-alt"></i>Import Records</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='lni lni-producthunt'></i>
                </div>
                <div class="menu-title">Sales Quotation</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('sales-quotation/new') }}"><i class="bx bx-right-arrow-alt"></i>New
                        Quotation</a>
                </li>
                <li>
                    <a href="{{ url('sales-quotation/list') }}"><i class="bx bx-right-arrow-alt"></i>Quotation
                        List</a>
                </li>
            </ul>
        </li>
        {{-- <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="lni lni-offer"></i>
                </div>
                <div class="menu-title">Offers</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('offers-gift') }}"><i class="bx bx-right-arrow-alt"></i>Gift Card</a>
                </li>
                <li>
                    <a href="{{ url('offers-coupon') }}"><i class="bx bx-right-arrow-alt"></i>Coupon List</a>
                </li>
            </ul>
        </li> --}}
        <li class="menu-label">Customer Management</li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-user-plus"></i>
                </div>
                <div class="menu-title">Customer</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('new-customer') }}"><i class="bx bx-right-arrow-alt"></i>Add New Customer</a>
                </li>
                <li>
                    <a href="{{ url('customer-list') }}"><i class="bx bx-right-arrow-alt"></i>Customer List</a>
                </li>
            </ul>
        </li>
        <li class="menu-label">Supplier Management</li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Supplier</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('new-supplier') }}"><i class="bx bx-right-arrow-alt"></i>New Supplier</a>
                </li>
                <li>
                    <a href="{{ url('supplier-list') }}"><i class="bx bx-right-arrow-alt"></i>Supplier List</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-minus"></i>
                </div>
                <div class="menu-title">Discount</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('issue-discount') }}"><i class="bx bx-right-arrow-alt"></i>Issue Discount</a>
                </li>
                <li>
                    <a href="{{ url('discount-product-list') }}"><i class="bx bx-right-arrow-alt"></i>Product
                        List</a>
                </li>
            </ul>
        </li>
        <li class="menu-label">Employee Management</li>
        <li>
            <a href="{{ url('employee-department') }}">
                <div class="parent-icon"><i class='bx bx-intersect'></i>
                </div>
                <div class="menu-title">Department</div>
            </a>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-group"></i>
                </div>
                <div class="menu-title">Employee</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('new-employee') }}"><i class="bx bx-right-arrow-alt"></i>New Employee</a>
                </li>
                <li>
                    <a href="{{ url('employee-list') }}"><i class="bx bx-right-arrow-alt"></i>Employee List</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-spreadsheet"></i>
                </div>
                <div class="menu-title">Timesheet</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('daily-attendance') }}"><i class="bx bx-right-arrow-alt"></i>Daily Attendance</a>
                </li>
                <li>
                    <a href="{{ url('attendance-history') }}"><i class="bx bx-right-arrow-alt"></i>Timesheet
                        Records</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-analyse"></i>
                </div>
                <div class="menu-title">Payroll</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('payroll-setup') }}"><i class="bx bx-right-arrow-alt"></i>Payroll Setup</a>
                </li>
                <li>
                    <a href="{{ url('holiday-manage') }}"><i class="bx bx-right-arrow-alt"></i>Holiday Manage</a>
                </li>
            </ul>
        </li>
        <li class="menu-label">Accounts Management</li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Accounts Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('account-category') }}"><i class="bx bx-right-arrow-alt"></i>New Category</a>
                </li>
                <li>
                    <a href="{{ url('income-category') }}"><i class="bx bx-right-arrow-alt"></i>Income Category</a>
                </li>
                <li>
                    <a href="{{ url('expense-category') }}"><i class="bx bx-right-arrow-alt"></i>Expense Category</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Transaction</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('new-transaction') }}"><i class="bx bx-right-arrow-alt"></i>New Transaction</a>
                </li>
                <li>
                    <a href="{{ url('income-transaction') }}"><i class="bx bx-right-arrow-alt"></i>Income List</a>
                </li>
                <li>
                    <a href="{{ url('expense-transaction') }}"><i class="bx bx-right-arrow-alt"></i>Expense List</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Account Statements</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('account-records/income-records') }}"><i class="bx bx-right-arrow-alt"></i>Income
                        Records</a>
                </li>
                <li>
                    <a href="{{ url('account-records/expense-records') }}"><i
                            class="bx bx-right-arrow-alt"></i>Expense
                        Records</a>
                </li>
                <li>
                    <a href="{{ url('account-records/daily-statement') }}"><i class="bx bx-right-arrow-alt"></i>Daily
                        Statement</a>
                </li>
                <li>
                    <a href="{{ url('account-records/monthly-statement') }}"><i
                            class="bx bx-right-arrow-alt"></i>Monthly
                        Statement</a>
                </li>
                <li>
                    <a href="{{ url('account-records/balance-sheet') }}"><i class="bx bx-right-arrow-alt"></i>Balance
                        Sheet</a>
                </li>
                <li>
                    <a href="{{ url('account-records/analysis') }}"><i class="bx bx-right-arrow-alt"></i>Account
                        Analysis</a>
                </li>
            </ul>
        </li>
        <li class="menu-label">Report Management</li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-receipt"></i>
                </div>
                <div class="menu-title">Products Report</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('report-product/stock-details') }}"><i class="bx bx-right-arrow-alt"></i>Product
                        Stock</a>
                </li>
                <li>
                    <a href="{{ url('report-product/minimum-stock') }}"><i class="bx bx-right-arrow-alt"></i>Minimum
                        Stock</a>
                </li>
                <li>
                    <a href="{{ url('report-product/daily-purchase') }}"><i class="bx bx-right-arrow-alt"></i>Daily
                        Purchase</a>
                </li>
                <li>
                    <a href="{{ url('report-product/weekly-purchase') }}"><i class="bx bx-right-arrow-alt"></i>Weekly
                        Purchase</a>
                </li>
                <li>
                    <a href="{{ url('report-product/monthly-purchase') }}"><i
                            class="bx bx-right-arrow-alt"></i>Monthly Purchase</a>
                </li>
                <li>
                    <a href="{{ url('report-product/search-purchase') }}"><i class="bx bx-right-arrow-alt"></i>Search
                        Records</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-line-chart"></i>
                </div>
                <div class="menu-title">Sales Report</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('report-sales/best-sales') }}"><i class="bx bx-right-arrow-alt"></i>Best
                        Sales</a>
                </li>
                <li>
                    <a href="{{ url('report-sales/daily-sales') }}"><i class="bx bx-right-arrow-alt"></i>Daily
                        Sales</a>
                </li>
                <li>
                    <a href="{{ url('report-sales/weekly-sales') }}"><i class="bx bx-right-arrow-alt"></i>Weekly
                        Sales</a>
                </li>
                <li>
                    <a href="{{ url('report-sales/monthly-sales') }}"><i class="bx bx-right-arrow-alt"></i>Monthly
                        Sales</a>
                </li>
                <li>
                    <a href="{{ url('report-sales/lowest-sales') }}"><i class="bx bx-right-arrow-alt"></i>Lowest
                        Sales</a>
                </li>
                <li>
                    <a href="{{ url('report-sales/search-sales') }}"><i class="bx bx-right-arrow-alt"></i>Search
                        Records</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Customer Report</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('report-customer/best-buyer') }}">
                        <i class="bx bx-right-arrow-alt"></i>Best Buyer
                    </a>
                </li>
                <li>
                    <a href="{{ url('report-customer/sales-history') }}">
                        <i class="bx bx-right-arrow-alt"></i>Sales History
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Employee Report</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('report-employee/salary') }}">
                        <i class="bx bx-right-arrow-alt"></i>Salary Report
                    </a>
                </li>
                <li>
                    <a href="{{ url('report-employee/leave') }}">
                        <i class="bx bx-right-arrow-alt"></i>Leave Report
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-grid-alt"></i>
                </div>
                <div class="menu-title">Accounts Report</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('report-account/purchase') }}"><i class="bx bx-right-arrow-alt"></i>Purchase
                        Report</a>
                </li>
                <li>
                    <a href="{{ url('report-account/sales') }}"><i class="bx bx-right-arrow-alt"></i>Sales Report</a>
                </li>
                <li>
                    <a href="{{ url('report-account/due') }}"><i class="bx bx-right-arrow-alt"></i>Due Report</a>
                </li>
                <li>
                    <a href="{{ url('report-account/income') }}"><i class="bx bx-right-arrow-alt"></i>Income
                        Report</a>
                </li>
                <li>
                    <a href="{{ url('report-account/expense') }}"><i class="bx bx-right-arrow-alt"></i>Expense
                        Report</a>
                </li>
            </ul>
        </li>
        <li class="menu-label">System Setting</li>
        <li>
            <a href="{{ url('company') }}">
                <div class="parent-icon"><i class='bx bx-buildings'></i>
                </div>
                <div class="menu-title">Company</div>
            </a>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-target-lock"></i>
                </div>
                <div class="menu-title">Role Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-role-setup') }}"><i class="bx bx-right-arrow-alt"></i>Create Role</a>
                </li>
                <li>
                    <a href="{{ url('setting-role-assign-user') }}"><i class="bx bx-right-arrow-alt"></i>Assign
                        User</a>
                </li>
                <li>
                    <a href="{{ url('setting-role-user-manage') }}"><i class="bx bx-right-arrow-alt"></i>User
                        Manage</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-reset"></i>
                </div>
                <div class="menu-title">Product Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-product-brand') }}"><i class="bx bx-right-arrow-alt"></i>Brands</a>
                </li>
                <li>
                    <a href="{{ url('setting-product-type') }}"><i class="bx bx-right-arrow-alt"></i>Product
                        Types</a>
                </li>
                <li>
                    <a href="{{ url('setting-product-group') }}"><i class="bx bx-right-arrow-alt"></i>Groups</a>
                </li>
                <li>
                    <a href="{{ url('setting-product-unit') }}"><i class="bx bx-right-arrow-alt"></i>Unit</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-shopping-bag"></i>
                </div>
                <div class="menu-title">Sales Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-pos-setup') }}"><i class="bx bx-right-arrow-alt"></i>POS Setup</a>
                </li>
                {{-- <li>
                    <a href="{{ url('setting-reward-point') }}"><i class="bx bx-right-arrow-alt"></i>Reward
                        Points</a>
                </li> --}}
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-user-check"></i>
                </div>
                <div class="menu-title">Customer Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-customer-group') }}"><i class="bx bx-right-arrow-alt"></i>Customer
                        Group</a>
                </li>
                <li>
                    <a href="{{ url('setting-customer-type') }}"><i class="bx bx-right-arrow-alt"></i>Customer
                        Type</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-user-voice"></i>
                </div>
                <div class="menu-title">Employee Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-employee-type') }}"><i class="bx bx-right-arrow-alt"></i>Employee
                        Type</a>
                </li>
                <li>
                    <a href="{{ url('setting-employee-hrm') }}"><i class="bx bx-right-arrow-alt"></i>HRM</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-font-color"></i>
                </div>
                <div class="menu-title">Accounts Setup</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-account-currency') }}"><i class="bx bx-right-arrow-alt"></i>Currency</a>
                </li>
                <li>
                    <a href="{{ url('setting-account-tax') }}"><i class="bx bx-right-arrow-alt"></i>TAX</a>
                </li>
                <li>
                    <a href="{{ url('setting-account-payment') }}"><i class="bx bx-right-arrow-alt"></i>Payment
                        Menthod</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon"><i class="bx bx-log-in-circle"></i>
                </div>
                <div class="menu-title">Othes</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('setting-notification-setup') }}"><i
                            class="bx bx-right-arrow-alt"></i>Notification</a>
                </li>
                <li>
                    <a href="{{ url('setting-sms-setup') }}"><i class="bx bx-right-arrow-alt"></i>SMS</a>
                </li>
                <li>
                    <a href="{{ url('setting-mail-setup') }}"><i class="bx bx-right-arrow-alt"></i>Mail Setup</a>
                </li>
                <li>
                    <a href="{{ url('setting-download-backup') }}"><i class="bx bx-right-arrow-alt"></i>Downloads &
                        Backup</a>
                </li>
            </ul>
        </li>
    </ul>
    <!--end navigation-->
</div>
