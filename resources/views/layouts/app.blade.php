<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!--favicon-->
    <link rel="icon" href="<?= config('app.url') ?>assets/images/favicon-32x32.png" type="image/png" />
    <!--plugins-->
    @yield('style')
    <link href="<?= config('app.url') ?>assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="<?= config('app.url') ?>assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="<?= config('app.url') ?>assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
    <!-- loader-->
    <link href="<?= config('app.url') ?>assets/css/pace.min.css" rel="stylesheet" />
    <script src="<?= config('app.url') ?>assets/js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="<?= config('app.url') ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= config('app.url') ?>assets/css/bootstrap-extended.css" rel="stylesheet">
    <link href="<?= config('app.url') ?>assets/css/app.css" rel="stylesheet">
    <link href="<?= config('app.url') ?>assets/css/icons.css" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="<?= config('app.url') ?>assets/css/dark-theme.css" />
    <link rel="stylesheet" href="<?= config('app.url') ?>assets/css/semi-dark.css" />
    <link rel="stylesheet" href="<?= config('app.url') ?>assets/css/header-colors.css" />
    <link rel="stylesheet" href="<?= config('app.url') ?>assets/css/select.css" />
    <!--- Editor --->
    <link href="<?= config('app.url') ?>assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= config('app.url') ?>trumbowyg/dist/ui/trumbowyg.min.css">
    <title>Inventory</title>
</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <!--navigation-->
        @include('layouts.nav')
        <!--end navigation-->
        <!--start header -->
        @include('layouts.header')
        <!--end header -->
        <!--start page wrapper -->
        @yield('wrapper')
        <!--end page wrapper -->
        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i
                class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->
        <footer class="page-footer">
            <p class="mb-0">Copyright © 2021. All right reserved.</p>
        </footer>
    </div>
    <!-- Bootstrap JS -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')
    </script>
    <script src="<?= config('app.url') ?>assets/js/jquery.min.js"></script>
    <script src="<?= config('app.url') ?>assets/js/bootstrap.bundle.min.js"></script>
    <!--plugins-->
    <script src="<?= config('app.url') ?>assets/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="<?= config('app.url') ?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="<?= config('app.url') ?>assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!--app JS-->
    <script src="<?= config('app.url') ?>assets/js/app.js"></script>
    <script src="<?= config('app.url') ?>assets/plugins/input-tags/js/tagsinput.js"></script>
    <script src="<?= config('app.url') ?>trumbowyg/dist/trumbowyg.min.js"></script>

    <script>
        $("html").attr("class", "color-sidebar sidebarcolor3 color-header headercolor0");
    </script>
    @yield('script')
    {{-- @include('layouts.theme-control') --}}
</body>

</html>
