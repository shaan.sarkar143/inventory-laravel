<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Print Invoice - Inventory</title>
    <style>
        * {
            position: relative;
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        html,
        body {
            height: 100%;
            width: 100%;
        }

        html {
            overflow: hidden;
            overflow-y: scroll;
        }

        img {
            width: auto;
            height: auto;
        }

        a {
            text-decoration: none;
        }

        ul li {
            list-style: none;
        }

        .container {
            height: auto;
            width: 100%;
            float: left;
        }

        .container .invoice {
            width: 314px;
            height: auto;
            float: left;
            position: absolute;
            left: 0;
            top: 0;
            background-color: #eeeeee63;
        }

        .container .invoice .header {
            height: auto;
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: space-between;
            padding: 37px 10px 0px;
        }

        .container .invoice .header h2 {
            position: absolute;
            top: 0;
            left: 0;
            font-size: 14px;
            line-height: 18px;
            padding: 3px 10px;
            width: 100%;
            font-weight: 400;
        }

        .container .invoice .header img {
            max-height: 100px;
            max-width: 100px;
        }

        .container .invoice .header ul li {
            font-size: 14px;
            line-height: 18px;
            width: 100%;
        }

        .container .invoice .customer,
        .container .invoice .items,
        .container .invoice .footer {
            padding: 5px 10px;
            height: auto;
            width: 100%;
            float: left;
        }

        .container .invoice .customer h2,
        .container .invoice .items h2 {
            font-size: 16px;
            line-height: 20px;
            padding: 3px 0px;
            width: 100%;
            font-weight: 400;
            float: left;
        }

        .container .invoice .customer ul,
        .container .invoice .items ul,
        .container .invoice .footer ul {
            float: left;
            width: 100%;
            height: auto;
            padding-bottom: 10px;
        }

        .container .invoice .customer ul li,
        .container .invoice .items ul li,
        .container .invoice .footer ul li {
            float: left;
            width: 48.5%;
            height: auto;
            font-size: 13px;
            line-height: 18px;
            border-bottom: 1px dotted #000;
            margin-bottom: 5px;
        }

        .container .invoice .items ul {
            padding-bottom: 0;
        }

        .container .invoice .customer ul li:nth-child(even) {
            float: right;
        }

        .container .invoice .items ul li:first-child {
            width: 39%;
        }

        .container .invoice .items ul li:not(.container .invoice .items ul li:first-child) {
            width: 22%;
            margin-left: 1%;
            text-align: right;
        }

        .container .invoice .items ul li:nth-child(2) {
            width: 14% !important;
            text-align: center !important;
        }

        .container .invoice .items ul:last-child,
        .container .invoice .footer ul:first-child {
            width: 60%;
            float: right;
            margin-top: 10px;
        }

        .container .invoice .items ul:last-child li,
        .container .invoice .footer ul:first-child li {
            width: 100% !important;
            float: right;
            padding: 0 0 0 10px;
        }

        .container .invoice .items ul:last-child li span:first-child,
        .container .invoice .footer ul:first-child li span:first-child {
            float: left;
        }

        .container .invoice .items ul:last-child li span:last-child,
        .container .invoice .footer ul:first-child li span:last-child {
            float: right;
        }

        .container .invoice label {
            width: 100%;
            height: auto;
            font-size: 14px;
            line-height: 17px;
            padding: 0 10px;
            text-align: center;
            float: left;
            margin: 5px 0;
        }

        .container .invoice label:nth-child(5) {
            text-align: right;
            margin: 0;
        }
    </style>
</head>

<body>
    <!--wrapper-->
    <div class="container">
        <div class="invoice">
            <div class="header">
                <h2>Invoice No. #989009<br />11-Jun-2022</h2>
                <img src="<?= config('app.url') ?>assets/images/logo-img.png" class="msg-avatar" alt="user avatar">
                <ul>
                    <li>Cmpany Name</li>
                    <li>Address Line 1</li>
                    <li>Address Line 2</li>
                </ul>
            </div>
            <div class="customer">
                <h2>Retail info</h2>
                <ul>
                    <li>Customer_name</li>
                    <li>Cashier_name</li>
                    <li>Phone_number</li>
                    <li>Other_info</li>
                </ul>
            </div>
            <label></label>
            <div class="items">
                <h2>Items info</h2>
                <ul>
                    <li># Item's Name</li>
                    <li>Qty.</li>
                    <li>Price/Qty</li>
                    <li>Amount</li>
                </ul>
                <ul>
                    <li>1. Item's Name</li>
                    <li>2Pcs</li>
                    <li>1000.00</li>
                    <li>2000.00</li>
                </ul>
                <ul>
                    <li>
                        <span>Total</span>
                        <span>2000.00</span>
                    </li>
                    <li>
                        <span>Vat</span>
                        <span>2%</span>
                    </li>
                    <li>
                        <span>Discount</span>
                        <span>2%</span>
                    </li>
                    <li>
                        <span>Grand Total</span>
                        <span>2000.00</span>
                    </li>
                </ul>
            </div>
            <label>Two thousands taka only</label>
            <div class="footer">
                <ul>
                    <li>
                        <span>Paid Amount</span>
                        <span>2000.00</span>
                    </li>
                    <li>
                        <span>Rest Amount</span>
                        <span>0.00</span>
                    </li>
                    <li>
                        <span>Return</span>
                        <span>0.00</span>
                    </li>
                    <li>
                        <span>Status</span>
                        <span>Clear</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
