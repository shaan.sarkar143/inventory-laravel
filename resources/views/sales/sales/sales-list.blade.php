<?php 
    use \App\Http\Controllers\Customer\CustomerSetup;
    use \App\Http\Controllers\Sales\SalesList;
    function customerInfo($id){
        $info = CustomerSetup::show($id);
        return $info;
    }
    function pro_info($invoice){
        $info = SalesList::product_info($invoice);
        return $info;
    }
?>
@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Sales</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Sales List</li>
            </ol>
          </nav>
        </div>
      </div>
      <!--end breadcrumb-->
      <!-- <h6 class="mb-0 text-uppercase">DataTable Example</h6>
      <hr/> -->
      <div class="row">
          <div class="col-xl-12 mx-auto">
              <div class="card p-3 pt-1">
                <div class="card-body">
                  <div class="table-responsive">
                    <?php 
                      $x = 1;
                      if(count($sales) > 0){
                        foreach($sales as $row => $v){
                          $cus_info = customerInfo($v->customer);
                          $pro_info = pro_info($v->invoice);
                          // echo $pro_info['total_products'];
                          if($v->inv_due < 0){
                              $status = '<span class="status btn-primary">Advanced</span>';
                          }else if($v->inv_due > 0){
                              $status = '<span class="status btn-danger">Due</span>';
                          }else{
                              $status = '<span class="status btn-success">Bill Paid</span>';
                          }
                    ?>
                      <div class="block-view">
                        <div class="code-image">
                          <label style="height:53px;line-height:18px;padding:8px;text-align:left;">Invoice<br/>#<?= $v->invoice ?></label>
                          <label style="height:53px;line-height:18px;padding:8px;top:unset;margin-top:4px;text-align:left;">Date<br/><?= date('d-M-Y', strtotime($v->sales_date)) ?></label>
                        </div>
                        <div class="product-info">
                          <div class="product-head">
                            <label><?= $cus_info['cus_info']->name ?></label>
                            <button class="btn-action btn-dark"><i class="lni lni-printer" style="margin-right:6px"></i>Print</button>
                            <button class="btn-action btn-danger"><i class="lni lni-close" style="margin-right:6px"></i>Cancel</button>
                            <button class="btn-action btn-info"><i class="bx bx-money" style="margin-right:6px"></i>Billing</button>
                            <button class="btn-action btn-secondary"><i class="lni lni-delivery" style="margin-right:6px"></i>Delivery</button>
                            <button class="btn-action btn-warning"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                            <button class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>View</button>
                          </div>
                          <div class="product-body">
                            <div class="col-xl-6 float-left">
                              <label>
                                <span>Phone</span>
                                <span>: <?= $cus_info['cus_info']->phone ?></span>
                              </label>
                              <label>
                                <span>Balance</span>
                                <span>: <?= number_format($cus_info['cus_info']->balance,2,'.','') ?></span>
                              </label>
                              <label>
                                <span>Status</span>
                                <?= $status ?>
                              </label>
                            </div>
                            <div class="col-xl-4 float-left">
                              <label>
                                <span>Products</span>
                                <span>: <?= $pro_info['total_products'] ?></span>
                              </label>
                              <label>
                                <span>Items</span>
                                <span>: <?= $pro_info['total_items'] ?></span>
                              </label>
                              <button class="btn-action btn-info float-left" style="padding:2px 10px;">
                                  <i class="bx bx-list-ul" style="margin-right:6px"></i>Product List
                              </button>
                            </div>
                            <div class="col-xl-2 float-right">
                              <label>
                                <span>Cost</span>
                                <span><?= number_format($v->total_cost,2,'.','') ?></span>
                              </label>
                              <label>
                                <span>Paid</span>
                                <span><?= number_format($v->total_paid,2,'.','') ?></span>
                              </label>
                              <label>
                                <span>Rest</span>
                                <span><?= number_format($v->inv_due,2,'.','') ?></span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php $x++; }}else{?>
                      <h3>No Sales Found..<a href="{{ url('sales-pos') }}" style="float:right;font-size:15px;cursor:pointer;"><u>POS</u></a></h3>
                    <?php }?>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
