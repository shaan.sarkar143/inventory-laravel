@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Sales</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Sales Done</li>
            </ol>
          </nav>
        </div>
      </div>
      <!--end breadcrumb-->
      <!-- <h6 class="mb-0 text-uppercase">DataTable Example</h6>
      <hr/> -->
      <div class="row">
          <div class="col-xl-12 mx-auto">
              <div class="card p-3 pt-1">
                <div class="card-body">
                  <div class="table-responsive">
                    <?php for($x = 1; $x <= 3; $x++){?>
                      <div class="block-view">
                        <div class="code-image">
                          <label style="height:53px;line-height:18px;padding:8px;text-align:left;">Invoice<br/>#989009</label>
                          <label style="height:53px;line-height:18px;padding:8px;top:unset;margin-top:4px;text-align:left;">Date<br/>30-Dec-2021</label>
                        </div>
                        <div class="product-info">
                          <div class="product-head">
                            <label>Customer Name</label>
                            <button class="btn-action btn-danger"><i class="bx bx-undo" style="margin-right:6px"></i>Return</button>
                            <button class="btn-action btn-dark"><i class="lni lni-printer" style="margin-right:6px"></i>Print</button>
                            <button class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>View</button>
                            <button class="btn-action btn-info"><i class="bx bx-list-ul" style="margin-right:6px"></i>Product List</button>
                            <!-- <div class="badge rounded-pill text-success bg-light-success p-2 text-uppercase px-3 float-right" style="margin-right:15px;">
                              <i class="bx bxs-circle me-1"></i>Done</div> -->
                          </div>
                          <div class="product-body">
                            <div class="col-xl-6 float-left">
                              <label>
                                <span>Phone</span>
                                <span>: 01923347911</span>
                              </label>
                              <label>
                                <span>Balance</span>
                                <span>: 5000.00</span>
                              </label>
                              <label>
                                <span>Paid Date</span>
                                <span>: 02-Feb-2022</span>
                              </label>
                            </div>
                            <div class="col-xl-4 float-left">
                              <label>
                                <span>Products</span>
                                <span>: 10</span>
                              </label>
                              <label>
                                <span>Items</span>
                                <span>: 100</span>
                              </label>
                            </div>
                            <div class="col-xl-2 float-right">
                              <label>
                                <span>Cost</span>
                                <span>15000.00</span>
                              </label>
                              <label>
                                <span>Paid</span>
                                <span>15000.00</span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php }?>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
