<?php
use App\Http\Controllers\Product\ProductList;
use App\Http\Controllers\Product\CategorySetup;
use App\Http\Controllers\Setting\GroupSetup;
use App\Http\Controllers\Customer\CustomerSetup;
use App\Http\Controllers\Setting\CustomerGroup;
use App\Http\Controllers\Setting\BrandSetup;
use App\Http\Controllers\Sales\SalesList;
$category = CategorySetup::create();
$products = ProductList::create();
$groups = GroupSetup::create();
$brands = BrandSetup::create();
$customer = CustomerSetup::customerList();
function customer_group($id)
{
    $group = CustomerGroup::show($id);
    return !empty($group) ? $group->name : '';
}
$invoice = SalesList::invoice();
?>
@extends('layouts.app')
@section('style')
    <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
    <link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.min.css">
    <style>
        html,
        body {
            height: 100%;
            width: 100%;
            overflow: hidden;
        }

        .wrapper {
            height: 100%;
            overflow: hidden;
        }

        .pro_con {
            margin: 0;
            top: 189px;
            padding: 18px 11px;
            overflow-y: scroll;
            float: left;
            width: 101%;
            position: absolute;
            height: 77%;
            --bs-gutter-x: 0 !important;
        }

        #product_list .col {
            margin-right: 20px;
        }

        .card_con {
            box-shadow: unset;
            width: 98%;
            border: 1px solid #DDD;
            height: 100%;
            position: relative;
            overflow: hidden;
        }

        #invoice {
            padding: .375rem .75rem !important;
        }

        .empty_row {
            padding: 5px 10px;
            border: 1px solid #ddd;
        }

        input[type=checkbox].toggle {
            display: none;
        }

        input[type=checkbox].toggle+label {
            float: right;
            height: 40px;
            width: 180px;
            border: 3px solid #83f;
            border-radius: 10px;
            padding: 0;
            margin: 0;
            line-height: 32px;
            box-sizing: border-box;
            transition: all .3s ease;
            position: relative;
            font-size: 15px;
            cursor: pointer;
        }

        input[type=checkbox].toggle+label::before {
            position: absolute;
            top: 1px;
            height: 32px;
            width: 40px;
            content: '';
            transition: all .3s ease;
            z-index: 3;
            border-radius: 7px;
        }

        input[type=checkbox].toggle+label::after {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            height: 32px;
            width: 140px;
            content: '';
            transition: all .3s ease;
            z-index: 2;
            text-overflow: ellipsis;
            overflow: hidden;
            text-transform: capitalize;
        }

        input[type=checkbox].toggle:checked+label {
            background-color: #592caa;
            text-align: left;
        }

        input[type=checkbox].toggle:not(:checked)+label {
            background-color: #e9ecef;
            text-align: right;
        }

        input[type=checkbox].toggle:not(:checked)+label::after {
            content: attr(data-unchecked);
            right: 17px;
            left: auto;
            opacity: 1;
            color: #222;
        }

        input[type=checkbox].toggle:checked+label::after {
            content: attr(data-checked);
            right: 20px;
            left: auto;
            opacity: 1;
            color: #FFF;
        }

        input[type=checkbox].toggle:not(:checked)+label::before {
            left: 1px;
            background-color: #83f;
        }

        input[type=checkbox].toggle:checked+label::before {
            left: 133px;
            background-color: #83f;
        }
    </style>
@endsection
@section('wrapper')
    <div class="page-wrapper">
        <div class="page-content" style="height:100%;overflow:hidden;">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Sales</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                POS&nbsp;&nbsp;&raquo;&nbsp;&nbsp;#<?= $invoice ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-md-6">
                    <!-- Modal -->
                    <div class="modal fade" id="exampleLargeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header" style="display: block">
                                    <h5 class="modal-title col-xl-12">
                                        <span class="float-left" style="line-height: 40px;">Customer Info</span>
                                        <input type="checkbox" class="toggle" value="1" id="new_old_customer" />
                                        <label for="new_old_customer" data-checked="New Customer"
                                            data-unchecked="Old Customer"></label>
                                    </h5>
                                </div>
                                <div class="modal-body">
                                    <form class="col-lg-12 p-3" action="#" method="post" name="">
                                        <div class="row">
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Sales Date</label>
                                                <input name="sales_date" class="form-control mb-3" type="date"
                                                    aria-label="Sales Date" />
                                            </div>
                                            <div class="col-xl-6 float-left" style="position: relative">
                                                <label class="col-sm-12 col-form-label">Choose Customer</label>
                                                <select name="customer" id="customer" class="form-control mb-3"
                                                    aria-label="Choose Customer">
                                                    <option value="0" selected disabled>Choose--</option>
                                                    <?php foreach($customer as $row => $v){?>
                                                    <option value="<?= $v->id ?>">
                                                        <?= ucfirst($v->fname) . ' ' . ucfirst($v->lname) . ' # ' . $v->phone . ' # Group:' . customer_group($v->group_id) ?>
                                                    </option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Customer Name</label>
                                                <input id="cus_name" name="cus_name" class="form-control mb-3"
                                                    type="text" placeholder="Customer Name" aria-label="Customer Name"
                                                    readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Customer Phone</label>
                                                <input id="cus_phone" name="cus_phone" class="form-control mb-3"
                                                    type="text" placeholder="Customer Phone" aria-label="Customer Phone"
                                                    readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Customer Balance</label>
                                                <input id="cus_balance" class="form-control mb-3" type="text"
                                                    placeholder="Customer Balance" aria-label="Customer Balance" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Customer Type</label>
                                                <input id="cus_type" class="form-control mb-3" type="text"
                                                    placeholder="Customer Type" aria-label="Customer Type" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Invoice No.</label>
                                                <input id="invoice" class="form-control mb-3" type="text"
                                                    value="<?= $invoice ?>" aria-label="Invoice No." readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Total Cost</label>
                                                <input id="total_cost" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Total Cost" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Vat Amount (%)</label>
                                                <input id="total_cost" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Vat Amount" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Discount</label>
                                                <input id="total_paid" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Discount" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Paid in</label>
                                                <input id="total_paid" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Discount" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Total Paid</label>
                                                <input id="total_paid" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Total Paid" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Total Due</label>
                                                <input id="total_due" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Total Due" readonly>
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Curr. Balance</label>
                                                <input id="curr_balance" class="form-control mb-3" type="text"
                                                    placeholder="0.00" aria-label="Curr. Balance" readonly>
                                            </div>
                                            <div class="col-xl-12 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Remarks</label>
                                                <textarea id="remarks" class="form-control mb-3" readonly></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer" style="display:inline-block">
                                    <label id="message" class="col-sm-6 col-form-label form-message float-left"></label>
                                    <button id="done" type="button" class="btn btn-primary float-right">Done &
                                        Print</button>
                                    <button id="payment" type="button"
                                        class="btn btn-success float-right">Done</button>
                                    <button type="button" class="btn btn-danger float-right"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row float-left col-md-12" style="height:87%;">
                <div class="col-lg-12 mx-auto" style="height:100%;">
                    <div class="col-xl-8 float-left" style="height:100%;">
                        <div class="card card_con">
                            <div class="col-lg-12 float-left"
                                style="position:absolute;top:0px;left:0px;border-bottom:1px solid #ddd;padding: 10px 5px 0;">
                                <div class="col-xl-4 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Sale Type</label>
                                    <select name="sale_type" id="sale_type" class="form-control mb-3 select_search"
                                        aria-label="Product Group">
                                        <option value="" selected disabled>Choose--</option>
                                        <option value="1">Retail Sale</option>
                                        <option value="2">Whole Sale</option>
                                    </select>
                                </div>
                                <div class="col-xl-4 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Product Group</label>
                                    <select name="group_id" id="group_id" class="form-control mb-3 select_search"
                                        aria-label="Product Group">
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($groups as $row => $v){?>
                                        <option value="<?= $v->id ?>"><?= ucfirst($v->name) ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-4 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Product Category</label>
                                    <select name="cat_id" id="cat_id" class="form-control mb-3 select_search"
                                        aria-label="Product Category" disabled>
                                    </select>
                                </div>
                                <div class="col-xl-4 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Brands</label>
                                    <select name="brand_id" id="brand_id" class="form-control mb-3 select_search"
                                        aria-label="Brands">
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($brands as $row => $v){?>
                                        <option value="<?= $v->id ?>"><?= ucfirst($v->name) ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-8 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Product Search</label>
                                    <input id="pro_search" class="form-control mb-3" type="search" />
                                </div>
                            </div>
                            <div id="product_list" class="row row-cols-auto pro_con">
                                <?php foreach($products['products'] as $row => $v){?>
                                <div class="col">
                                    <button type="button" data-id="<?= $v->id ?>"
                                        class="btn btn-outline-dark position-relative me-lg-1 product">
                                        <i class='bx bx-box align-middle'></i> <?= $v->name ?><span
                                            class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary">
                                            <?= $v->stock ?></span>
                                    </button>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 float-right" style="height:100%;position:relative;">
                        <div class="card"
                            style="box-shadow:unset;width:100%;border:1px solid #DDD;height:100%;overflow-y:scroll;">
                            <div class="card-body col-xl-12 float-left" style="margin-bottom:30px;">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:30px;text-align:center;">##</th>
                                            <th>Product Info</th>
                                            <th style="width:40px">###</th>
                                        </tr>
                                    </thead>
                                    <tbody id="sales_product"></tbody>
                                    <tfoot>
                                        <tr class="bg-dark">
                                            <td colspan="3" style="color:#FFF;text-align:center;">Summary : Total
                                                Products <span id="total_product">0</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <span class="col-lg-6 float-left">
                                                    <div class="row">
                                                        <span class="col float-left">Items</span>
                                                        <span class="col float-left" id="total_items">0</span>
                                                    </div>
                                                </span>
                                                <span class="col-lg-6 float-left" style="text-align:right">
                                                    <div class="row">
                                                        <span class="col float-left"
                                                            style="text-align:right;">Amount</span>
                                                        <span class="col float-right" id="total_amount">0.00</span>
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card bg-default"
                            style="position:absolute;bottom:0px;margin:0;height:50px;width:100%;display:inline-block">
                            <div class="col-md-4 p-2 float-left" style="height:100%;">
                                <button class="col-xl-12 btn-action btn-danger" style="height:100%;"
                                    id="clear">Remove All</button>
                            </div>
                            <div class="col-md-4 p-2 float-left" style="height:100%;">
                                <button class="col-xl-12 btn-action btn-warning" style="height:100%;"
                                    id="hold">Hold</button>
                            </div>
                            <div class="col-md-4 p-2 float-left" style="height:100%;">
                                <button id="next" class="col-xl-12 btn-action btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#exampleLargeModal" style="height:100%;">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
@endsection

@section('script')
    <script src="assets/plugins/input-tags/js/tagsinput.js"></script>
    <script>
        $('.select_search').select2({
            height: 'resolved',
            width: '100%'
        });
        $('#customer').select2({
            dropdownParent: $("#exampleLargeModal"),
            width: '100%'
        });
        $('body').on('click', '#new_old_customer', function() {
            if ($(this).prop('checked') == true) {
                $('#customer').prop('disabled', true);
                $('#customer').val(0);
                $('#cus_name').prop('readonly', false);
                $('#cus_phone').prop('readonly', false);
            } else {
                $('#customer').prop('disabled', false);
                $('#cus_name').prop('readonly', true);
                $('#cus_phone').prop('readonly', true);
            }
        });
        $('body').on('change', '#customer', function() {
            console.log($(this).val());
            let data = {
                id: $(this).val()
            }
            $.ajax({
                type: "GET",
                url: "{{ url('customer-list') }}" + '/' + $(this).val(),
                dataType: 'json',
                success: function(response) {
                    result = response;
                    $('#total_paid').prop('readonly', false);
                    $('#remarks').prop('readonly', false);
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#cus_balance').val(result.cus_info.balance);
                    $('#curr_balance').val(result.cus_info.balance);
                    $('#cus_name').val(result.cus_info.fname);
                    $('#cus_phone').val(result.cus_info.phone);
                    $('#cus_type').val(result.cus_type.name);
                }
            });
        });
        $('body').on('change', '#group_id', function() {
            let data = {
                'group_id': $(this).val()
            };
            let result = {};
            let categories = '<option value="" selected disabled>Choose--</option>';
            let products = '';
            $.ajax({
                type: "GET",
                url: "{{ url('product-category/groupWiseCategory') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.categories != undefined) {
                        if (result.categories.length > 0) {
                            $('#cat_id').prop('disabled', false);
                            $.each(result.categories, function(i, val) {
                                categories += '<option value="' + val.id + '">' + val.name +
                                    '</option>';
                            });
                        } else {
                            $('#cat_id').prop('disabled', true);
                            categories += '';
                        }
                    } else {
                        $('#cat_id').prop('disabled', true);
                        categories += '';
                    }
                    if (result.products != undefined) {
                        if (result.products.length > 0) {
                            $.each(result.products, function(i, val) {
                                products += '<div class="col">';
                                products +=
                                    '<button type="button" class="btn btn-outline-dark position-relative me-lg-1 product">';
                                products += '<i class="bx bx-box align-middle"></i> ' + val
                                    .name +
                                    '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary">' +
                                    val.stock + '</span>';
                                products += '</button>';
                                products += '</div>';
                            });
                        } else {
                            products += '<label>No Product Found..</label>';
                        }
                    } else {
                        products += '<label>No Product Found..</label>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#product_list').html(products);
                    $('#cat_id').html(categories);
                }
            });
        });
        $('body').on('change', '#cat_id', function() {
            let data = {
                'cat_id': $(this).val()
            };
            let result = {};
            let products = '';
            $.ajax({
                type: "GET",
                url: "{{ url('product-category/catGroupWiseCategory') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.products != undefined) {
                        if (result.products.length > 0) {
                            $.each(result.products, function(i, val) {
                                products += '<div class="col">';
                                products +=
                                    '<button type="button" class="btn btn-outline-dark position-relative me-lg-1 product">';
                                products += '<i class="bx bx-box align-middle"></i> ' + val
                                    .name +
                                    '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary">' +
                                    val.stock + '</span>';
                                products += '</button>';
                                products += '</div>';
                            });
                        } else {
                            products += '<label>No Product Found..</label>';
                        }
                    } else {
                        products += '<label>No Product Found..</label>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#product_list').html(products);
                }
            });
        });
        $('body').on('change', '#brand_id', function() {
            let data = {
                'brand_id': $(this).val()
            };
            let result = {};
            let products = '';
            $.ajax({
                type: "GET",
                url: "{{ url('setting-product-brand/brandWiseProduct') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.products != undefined) {
                        if (result.products.length > 0) {
                            $.each(result.products, function(i, val) {
                                products += '<div class="col">';
                                products +=
                                    '<button type="button" class="btn btn-outline-dark position-relative me-lg-1 product">';
                                products += '<i class="bx bx-box align-middle"></i> ' + val
                                    .name +
                                    '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary">' +
                                    val.stock + '</span>';
                                products += '</button>';
                                products += '</div>';
                            });
                        } else {
                            products += '<label>No Product Found..</label>';
                        }
                    } else {
                        products += '<label>No Product Found..</label>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#product_list').html(products);
                }
            });
        });
        $('body').on('input', '#pro_search', function() {
            let value = $(this).val();
            let data = {};
            if ($('#brand_id').val() != null && $('#cat_id').val() != null && $('#group_id').val() != null) {
                data = {
                    value: value,
                    brand_id: $('#brand_id').val(),
                    cat_id: $('#cat_id').val(),
                    group_id: 0
                };
            } else if ($('#brand_id').val() != null && $('#cat_id').val() == null && $('#group_id').val() != null) {
                data = {
                    value: value,
                    brand_id: $('#brand_id').val(),
                    cat_id: 0,
                    group_id: $('#group_id').val()
                };
            } else if ($('#brand_id').val() != null && $('#cat_id').val() == null && $('#group_id').val() == null) {
                data = {
                    value: value,
                    brand_id: $('#brand_id').val(),
                    cat_id: 0,
                    group_id: 0
                };
            } else if ($('#brand_id').val() == null && $('#cat_id').val() != null && $('#group_id').val() != null) {
                data = {
                    value: value,
                    brand_id: 0,
                    cat_id: $('#cat_id').val(),
                    group_id: 0
                };
            } else {
                data = {
                    value: value,
                    brand_id: 0,
                    cat_id: 0,
                    group_id: 0
                };
            }
            let result = {};
            let products = '';
            if (value != '') {
                $.ajax({
                    type: "GET",
                    url: "{{ url('product-list/search') }}",
                    dataType: 'json',
                    data: data,
                    success: function(response) {
                        result = response;
                        if (result.products != undefined) {
                            if (result.products.length > 0) {
                                $.each(result.products, function(i, val) {
                                    products += '<div class="col">';
                                    products +=
                                        '<button type="button" class="btn btn-outline-dark position-relative me-lg-1 product">';
                                    products += '<i class="bx bx-box align-middle"></i> ' + val
                                        .name +
                                        '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary">' +
                                        val.stock + '</span>';
                                    products += '</button>';
                                    products += '</div>';
                                });
                            } else {
                                products += '<label>No Product Found..</label>';
                            }
                        } else {
                            products += '<label>No Product Found..</label>';
                        }
                    },
                    error: function(er) {
                        console.log(er);
                    },
                    complete: function() {
                        $('#product_list').html(products);
                    }
                });
            }
        });
        $('body').on('click', '.product', function() {
            let data = {
                invoice: $('#invoice').val(),
                pro_id: $(this).data('id')
            };
            let result = {};
            let products = '';
            let x = 1;
            $.ajax({
                type: "GET",
                url: "{{ url('sales-list/productAdd') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.salesInfo != undefined) {
                        if (result.salesInfo.length > 0) {
                            $.each(result.salesInfo, function(i) {
                                let value = result.salesInfo[i];
                                products += '<tr><td style="text-align:center;">' + x +
                                    '</td><td>';
                                products += '<span class="col-lg-8 float-left">' + result
                                    .pro_info[i].name + '</span>';
                                products +=
                                    '<span class="col-lg-4 float-left" style="text-align:right">' +
                                    value.pro_price + '</span>';
                                products += '<span class="col-lg-6 float-left mt-2">';
                                products +=
                                    '<input class="form-control pro_qty" type="number" min="1" placeholder="Qty" value="' +
                                    value.pro_qty + '"/>';
                                products += '</span>';
                                products +=
                                    '<span class="col-lg-6 float-left mt-2" style="text-align:right">' +
                                    value.total_cost + '</span>';
                                products += '</td><td>';
                                products +=
                                    '<div class="row row-cols-auto g-1 mx-auto flex-center" style="flex-wrap:wrap">';
                                products += '<button data-id="' + value.id +
                                    '" class="btn-action btn-warning pro_edit"><i class="lni lni-pencil-alt"></i></button>';
                                products += '<button data-id="' + value.id +
                                    '" class="btn-action btn-danger pro_del"><i class="lni lni-trash"></i></button>';
                                products += '</div></td></tr>';
                                x++;
                            });
                        } else {
                            products +=
                                '<tr><td class="empty_row" colspan="3">No Product Added Yet..</td></tr>';
                        }
                    } else {
                        products +=
                            '<tr><td class="empty_row" colspan="3">No Product Added Yet..</td></tr>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#total_product').html(result.totalInfo.total_product);
                    $('#total_items').html(result.totalInfo.total_items);
                    $('#total_amount').html(result.totalInfo.total_amount);
                    $('#sales_product').html(products);
                }
            });
        });
        $('body').on('click', '#clear', function() {
            let data = {
                invoice: $('#invoice').val()
            };
            $.ajax({
                type: "GET",
                url: "{{ url('sales-list/productClear') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#total_product').html(result.totalInfo.total_product);
                    $('#total_items').html(result.totalInfo.total_items);
                    $('#total_amount').html(result.totalInfo.total_amount);
                    $('#sales_product').html('');
                }
            });
        });
        $('body').on('click', '#next', function() {
            var total_cost = parseFloat($('#total_amount').html()).toFixed(2);
            $('#total_cost').val(total_cost);
        });
        $('body').on('input', '#total_paid', function() {
            var total_due = 0,
                curr_balance = $('#cus_balance').val(),
                total_cost = $('#total_cost').val();
            var paid = $(this).val();
            total_due = parseFloat(total_cost) - parseFloat(paid);
            curr_balance = parseFloat(curr_balance) + parseFloat(total_due);
            $('#total_due').val(parseFloat(total_due).toFixed(2));
            $('#curr_balance').val(parseFloat(curr_balance).toFixed(2));
        });
        $('body').on('click', '#payment', function() {

            let data = {
                invoice: $('#invoice').val(),
                cus_id: $('#customer').val(),
                total_cost: $('#total_cost').val(),
                total_paid: $('#total_paid').val(),
                inv_due: $('#total_due').val(),
                remarks: $('#remarks').val(),
                balance: $('#curr_balance').val(),
                _token: "{{ csrf_token() }}"
            }
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('sales-list.store') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    if (result.status == 1) {
                        curr_class = 'btn-success';
                        $("#message").addClass('btn-success');
                    } else if (result.status == 0) {
                        curr_class = 'btn-danger';
                        $("#message").addClass('btn-danger');
                    }
                    setTimeout(function() {
                        removeClass('message', curr_class)
                        $('#message').html("");
                    }, 1500);
                    if (result.load) {
                        setTimeout(function() {
                            location.replace(result.load);
                        }, 2000);
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#message').html(result.message);
                }
            });
        });
        $('body').on('click', '.pro_edit', function() {
            let index = $(this).index('.pro_edit');
            let pro_qty = $('.pro_qty').eq(index).val();
            let data = {
                row_id: $(this).data('id'),
                pro_qty: pro_qty
            }
            let result = {};
            let products = '';
            let x = 1;
            $.ajax({
                type: "GET",
                url: "{{ url('sales-list/productEdit') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    if (result.salesInfo != undefined) {
                        if (result.salesInfo.length > 0) {
                            $.each(result.salesInfo, function(i) {
                                let value = result.salesInfo[i];
                                products += '<tr><td style="text-align:center;">' + x +
                                    '</td><td>';
                                products += '<span class="col-lg-8 float-left">' + result
                                    .pro_info[i].name + '</span>';
                                products +=
                                    '<span class="col-lg-4 float-left" style="text-align:right">' +
                                    value.pro_price + '</span>';
                                products += '<span class="col-lg-6 float-left mt-2">';
                                products +=
                                    '<input class="form-control pro_qty" type="number" min="1" placeholder="Qty" value="' +
                                    value.pro_qty + '"/>';
                                products += '</span>';
                                products +=
                                    '<span class="col-lg-6 float-left mt-2" style="text-align:right">' +
                                    value.total_cost + '</span>';
                                products += '</td><td>';
                                products +=
                                    '<div class="row row-cols-auto g-1 mx-auto flex-center" style="flex-wrap:wrap">';
                                products += '<button data-id="' + value.id +
                                    '" class="btn-action btn-warning pro_edit"><i class="lni lni-pencil-alt"></i></button>';
                                products += '<button data-id="' + value.id +
                                    '" class="btn-action btn-danger pro_del"><i class="lni lni-trash"></i></button>';
                                products += '</div></td></tr>';
                                x++;
                            });
                        } else {
                            products +=
                                '<tr><td class="empty_row" colspan="3">No Product Added Yet..</td></tr>';
                        }
                    } else {
                        products +=
                            '<tr><td class="empty_row" colspan="3">No Product Added Yet..</td></tr>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#total_product').html(result.totalInfo.total_product);
                    $('#total_items').html(result.totalInfo.total_items);
                    $('#total_amount').html(result.totalInfo.total_amount);
                    $('#sales_product').html(products);
                }
            });
        });
        $('body').on('click', '.pro_del', function() {
            let data = {
                row_id: $(this).data('id')
            }
            let result = {};
            let products = '';
            let x = 1;
            $.ajax({
                type: "GET",
                url: "{{ url('sales-list/productDel') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    if (result.salesInfo != undefined) {
                        if (result.salesInfo.length > 0) {
                            $.each(result.salesInfo, function(i) {
                                let value = result.salesInfo[i];
                                products += '<tr><td style="text-align:center;">' + x +
                                    '</td><td>';
                                products += '<span class="col-lg-8 float-left">' + result
                                    .pro_info[i].name + '</span>';
                                products +=
                                    '<span class="col-lg-4 float-left" style="text-align:right">' +
                                    value.pro_price + '</span>';
                                products += '<span class="col-lg-6 float-left mt-2">';
                                products +=
                                    '<input class="form-control pro_qty" type="number" min="1" placeholder="Qty" value="' +
                                    value.pro_qty + '"/>';
                                products += '</span>';
                                products +=
                                    '<span class="col-lg-6 float-left mt-2" style="text-align:right">' +
                                    value.total_cost + '</span>';
                                products += '</td><td>';
                                products +=
                                    '<div class="row row-cols-auto g-1 mx-auto flex-center" style="flex-wrap:wrap">';
                                products += '<button data-id="' + value.id +
                                    '" class="btn-action btn-warning pro_edit"><i class="lni lni-pencil-alt"></i></button>';
                                products += '<button data-id="' + value.id +
                                    '" class="btn-action btn-danger pro_del"><i class="lni lni-trash"></i></button>';
                                products += '</div></td></tr>';
                                x++;
                            });
                        } else {
                            products +=
                                '<tr><td class="empty_row" colspan="3">No Product Added Yet..</td></tr>';
                        }
                    } else {
                        products +=
                            '<tr><td class="empty_row" colspan="3">No Product Added Yet..</td></tr>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#total_product').html(result.totalInfo.total_product);
                    $('#total_items').html(result.totalInfo.total_items);
                    $('#total_amount').html(result.totalInfo.total_amount);
                    $('#sales_product').html(products);
                }
            });
        });
    </script>
@endsection
