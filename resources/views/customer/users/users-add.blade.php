@extends("layouts.app")
@section("wrapper")
        <div class="page-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Users</div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add New Users</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row">
                    <div class="col-xl-7 mx-auto">
                        <div class="card border-top border-0 border-4 border-primary">
                            <div class="card-body p-5">
                                <form class="row g-3">
                                    <div class="card-title d-flex align-items-center">
                                        <div><i class="bx bxs-user me-1 font-22 text-primary"></i>
                                        </div>
                                        <h5 class="mb-0 text-primary">Basic Information</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputFirstName" class="form-label">First Name</label>
                                        <input type="text" class="form-control" id="inputFirstName">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputLastName" class="form-label">Last Name</label>
                                        <input type="text" class="form-control" id="inputLastName">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputPassword" class="form-label">Phone No.</label>
                                        <input type="number" class="form-control" id="inputPassword">
                                    </div>
                                    <div class="col-md-8">
                                        <label for="inputEmail" class="form-label">Email</label>
                                        <input type="email" class="form-control" id="inputEmail">
                                    </div>
                                    <div class="col-12">
                                        <label for="inputAddress" class="form-label">Address</label>
                                        <input type="text" class="form-control" placeholder="Address..." id="inputLastName">
                                    </div>
                                    <div class="col-12">
                                        <label for="inputAddress2" class="form-label">Address 2</label>
                                        <input type="text" class="form-control" placeholder="Address..." id="inputLastName">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="inputCity" class="form-label">City</label>
                                        <input type="text" class="form-control" id="inputCity">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputState" class="form-label">State</label>
                                        <select id="inputState" class="form-select">
                                            <option selected>Choose...</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="inputZip" class="form-label">Zip</label>
                                        <input type="number" class="form-control" id="inputZip">
                                    </div>
                                    <div class="card-title d-flex align-items-center" style="margin-top:35px;">
                                        <div><i class="bx bx-money me-1 font-22 text-primary"></i></div>
                                        <h5 class="mb-0 text-primary">Accounts Information</h5>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <label for="inputFirstName" class="form-label">Customer Group</label>
                                        <input type="text" class="form-control" id="inputFirstName">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputFirstName" class="form-label">Customer Type</label>
                                        <input type="text" class="form-control" id="inputFirstName">
                                    </div> -->
                                    <div class="col-md-4">
                                        <label for="inputFirstName" class="form-label">Advanced</label>
                                        <input type="text" class="form-control" id="inputFirstName">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputLastName" class="form-label">Due</label>
                                        <input type="text" class="form-control" id="inputLastName">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputLastName" class="form-label">Currnet Balance</label>
                                        <input type="text" class="form-control" id="inputLastName">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputLastName" class="form-label">Remarks</label>
                                        <textarea class="form-control" id="Remarks" placeholder="Remarks..." rows="3"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary float-right">
                                          <i class="bx bx-book-add" style="margin-right:6px;"></i>Add New</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
@endsection
