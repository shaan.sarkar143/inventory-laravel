<?php 
    use \App\Http\Controllers\Setting\CustomerType;
    $types = CustomerType::create();
?>
@extends("layouts.app")
@section("wrapper")
        <div class="page-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Customer</div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add New Customer</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row">
                    <div class="col-xl-7 mx-auto">
                        <div class="card border-top border-0 border-4 border-primary">
                            <div class="card-body p-5">
                                <form class="row g-3" action="{{ route('customer-list.store') }}" method="post" id="newCustomer">
                                    <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                    <div class="card-title d-flex align-items-center">
                                        <div><i class="bx bxs-user me-1 font-22 text-primary"></i>
                                        </div>
                                        <h5 class="mb-0 text-primary">Basic Information</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">First Name</label>
                                        <input name="first_name" type="text" class="form-control" >
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">Last Name</label>
                                        <input name="last_name" type="text" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Phone No.</label>
                                        <input name="phone" type="text" class="form-control" >
                                    </div>
                                    <div class="col-md-8">
                                        <label class="form-label">Email</label>
                                        <input name="email" type="email" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">City</label>
                                        <input name="city" type="text" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">State</label>
                                        <input name="state" type="text" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Zip</label>
                                        <input name="zip" type="number" class="form-control" >
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Local Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Address..." >
                                    </div>
                                    <div class="card-title d-flex align-items-center" style="margin-top:35px;">
                                        <div><i class="bx bx-money me-1 font-22 text-primary"></i></div>
                                        <h5 class="mb-0 text-primary">Accounts Information</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">Customer Type</label>
                                        <select name="type_id" id="type_id" class="form-select select_search">
                                            <option selected disabled>Choose Type</option>
                                        <?php foreach($types as $row => $v){?>
                                            <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">Customer Group</label>
                                        <input id="group_id" name="group_id" type="hidden"/>
                                        <input id="group_name" type="text" class="form-control" placeholder="Customer Group" readonly/>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Advanced</label>
                                        <input name="advanced" type="text" class="form-control"  value="0">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Due</label>
                                        <input name="due" type="text" class="form-control"  value="0">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Currnet Balance</label>
                                        <input name="balance" type="text" class="form-control"  value="0">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="form-label">Remarks</label>
                                        <textarea name="remarks" class="form-control" id="Remarks" placeholder="Remarks..." rows="3"></textarea>
                                    </div>
                                    <div class="mt-5 col-md-12">
                                        <label id="message" class="col-sm-6 col-form-label form-message float-left"></label>
                                        <button type="submit" class="btn btn-primary float-right ">
                                          <i class="bx bx-book-add" style="margin-right:6px;"></i>Add New</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
@endsection
@section("script")
<script>
    $(document).ready(function() {
        $('.select_search').select2({
            height:'resolved'
        });
        
        $('body').on('change','#type_id', function(){
            let data = {
                'cat_id': $(this).val()
            };
            let result = {};
            $.ajax({
                type: "GET",
                url: "{{ url('setting-customer-type/group_info') }}",
                dataType: 'json',
                data: data,
                success: function (response)
                {
                    result = response;
                },
                error: function (er)
                {
                    console.log(er);
                },
                complete: function ()
                {
                    $('#group_name').val(result.name);
                    $('#group_id').val(result.id);
                }
            });
        });
    });
</script>
@endsection