<?php 
    use \App\Http\Controllers\Setting\CustomerType;
    function customerType($id){
        $type = CustomerType::show($id);
        return ($type !== null) ? $type->name : 'N/A';
    }
?>
@extends("layouts.app")

@section("style")
<link href="<?= config('app.url'); ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Customer</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Customer List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
          <div class="col-md-6">
            <!-- Modal -->
            <div class="modal fade" id="cusModal" aria-hidden="true" style="overflow-x:hidden;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Customer Information Update</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="edit_cus">
                        </div>
                        <div class="modal-footer" style="display:inline-block">
                            <button form="cus_edit_form" type="submit" class="btn btn-primary float-right">Update</button>
                            <button form="cus_edit_form" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
      <div class="card col-xl-12 mx-auto">
        <div class="card-body">
          <div class="table-responsive">
            <?php $x = 1; foreach($customer as $row => $v){?>
              <div class="block-view">
                <div class="code-image">
                  <label style="height:53px;line-height:18px;padding:8px;text-align:left;">Account No.<br/>#<?= $v->id ?></label>
                  <label style="height:53px;padding:8px;top:unset;margin-top:4px;justify-content:center;display:flex;align-items:center;font-size:16px;"><?= customerType($v->type_id) ?></label>
                </div>
                <div class="product-info">
                  <div class="product-head">
                    <label><?= $v->fname.' '.$v->lname ?></label>
                    <button class="btn-action btn-danger"><i class="lni lni-close" style="margin-right:6px"></i>Remove</button>
                    <button class="btn-action btn-info"><i class="bx bx-money" style="margin-right:6px"></i>Billing</button>
                    <button data-id="<?= $v->id ?>" class="btn-action btn-warning cus_edit"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                    <button data-id="<?= $v->id ?>" class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>Details</button>
                  </div>
                  <div class="product-body">
                    <div class="col-xl-6 float-left">
                      <label>
                        <span>Phone</span>
                        <span>: <?= $v->phone ?></span>
                      </label>
                      <label>
                        <span>Email</span>
                        <span>: <?= $v->email ?></span>
                      </label>
                      <label>
                        <span>Address</span>
                        <span>: <?= $v->address.', '.$v->city.', '.$v->state.' - '.$v->zip ?></span>
                      </label>
                    </div>
                    <div class="col-xl-6 float-left">
                      <label>
                        <span style="width:50%;">Balance : <?= number_format($v->balance,2,'.','').' TK' ?></span>
                        <span style="width:50%;">Last Bill : <?= number_format($v->balance,2,'.','').' TK' ?></span>
                      </label>
                      <label style="float:right">
                        <span>Status</span>
                        <span class="status btn-danger">Due</span>
                      </label>
                      <div class="col-12 float-left">
                        <button class="btn-action btn-danger float-right">Cancel : 3</button>
                        <button class="btn-action btn-success float-right" style="margin-right:4px;">Done : 5</button>
                        <button class="btn-action btn-warning float-right" style="margin-right:4px;">Processing : 3</button>
                        <button class="btn-action btn-primary float-right" style="margin-right:4px;">Pending : 10</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php $x++; }?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();

        $('body').on('change','#type_id', function(){
            let data = {
                'cat_id': $(this).val()
            };
            let result = {};
            $.ajax({
                type: "GET",
                url: "{{ url('setting-customer-type/group_info') }}",
                dataType: 'json',
                data: data,
                success: function (response)
                {
                    result = response;
                },
                error: function (er)
                {
                    console.log(er);
                },
                complete: function ()
                {
                    $('#group_name').val(result.name);
                    $('#group_id').val(result.id);
                }
            });
        });

        $('body').on('click','.cus_edit', function(){
            let url = window.location.pathname;
            url = url + '/'+ $(this).data('id') + '/edit';
            $('#edit_cus').load(url,function(){
                $('#cusModal').modal('show');
                $('.select_search').select2({
                    dropdownParent: $("#cusModal"),
                    width:'100%'
                });
            });
        }); 
    });
</script>
@endsection
