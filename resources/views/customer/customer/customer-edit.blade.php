<?php 
    use \App\Http\Controllers\Setting\CustomerGroup;
    function customerGroup($id){
        $group = CustomerGroup::show($id);
        return ($group !== null) ? $group->name : 'N/A';
    }
?>
<div class="row">
    <form class="col-lg-12" style="padding:0 20px" action="{{ route('customer-list.update', $info->id) }}" id="cus_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="card-title d-flex align-items-center">
            <div><i class="bx bxs-user me-1 font-22 text-primary"></i>
            </div>
            <h5 class="mb-0 text-primary">Basic Information</h5>
        </div>
        <div class="row">
            <div class="col-md-6 mb-3 float-left">
                <label class="form-label">First Name</label>
                <input name="first_name" type="text" class="form-control" value="<?= $info->fname ?>" required>
            </div>
            <div class="col-md-6 mb-3 float-left">
                <label class="form-label">Last Name</label>
                <input name="last_name" type="text" class="form-control" value="<?= $info->lname ?>" >
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">Phone No.</label>
                <input name="phone" type="text" class="form-control" value="<?= $info->phone ?>" required>
            </div>
            <div class="col-md-8  mb-4 float-left">
                <label class="form-label">Email</label>
                <input name="email" type="email" class="form-control" value="<?= $info->email ?>" required>
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">City</label>
                <input name="city" type="text" class="form-control" value="<?= $info->city ?>" >
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">State</label>
                <input name="state" type="text" class="form-control" value="<?= $info->state ?>" >
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">Zip</label>
                <input name="zip" type="number" class="form-control" value="<?= $info->zip ?>" >
            </div>
            <div class="col-md-12 mb-3 float-left">
                <label class="form-label">Local Address</label>
                <input name="address" type="text" class="form-control" value="<?= $info->address ?>" placeholder="Address..." >
            </div>
        </div>
        <div class="card-title d-flex align-items-center" style="margin-top:35px;">
            <div><i class="bx bx-money me-1 font-22 text-primary"></i></div>
            <h5 class="mb-0 text-primary">Accounts Information</h5>
        </div>
        <div class="row">
            <div class="col-md-6 mb-3 float-left">
                <label class="form-label">Customer Type </label>
                <select name="type_id" id="type_id" class="form-select select_search" required>
                    <option selected disabled>Choose Type</option>
                <?php foreach($types as $row => $v){?>
                    <option value="<?= $v->id ?>" <?= ($info->type_id == $v->id) ? 'selected' : '' ?>><?= $v->name ?></option>
                <?php }?>
                </select>
            </div>
            <div class="col-md-6 mb-3 float-left">
                <label class="form-label">Customer Group</label>
                <input id="group_id" name="group_id" type="hidden" value="<?= $info->group_id ?>" required/>
                <input id="group_name" type="text" class="form-control" value="<?= customerGroup($info->group_id) ?>" placeholder="Customer Group" readonly/>
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">Advanced</label>
                <input name="advanced" type="text" class="form-control" value="<?= $info->advanced ?>"  value="0">
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">Due</label>
                <input name="due" type="text" class="form-control" value="<?= $info->due ?>"  value="0">
            </div>
            <div class="col-md-4 mb-3 float-left">
                <label class="form-label">Current Balance</label>
                <input name="balance" type="text" class="form-control" value="<?= $info->balance ?>"  value="0">
            </div>
            <div class="col-md-12">
                <label class="form-label">Remarks</label>
                <textarea name="remarks" class="form-control" id="Remarks" placeholder="Remarks..." rows="3"><?= $info->remarks ?></textarea>
            </div>
        </div>
    </form>
</div>