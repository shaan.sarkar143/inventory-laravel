@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Biller</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Biller's List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="card col-xl-12 mx-auto">
        <div class="card-body">
          <div class="table-responsive">
            <?php for($x = 1; $x <= 3; $x++){?>
              <div class="block-view">
                <div class="code-image">
                  <label style="height:53px;line-height:18px;padding:8px;text-align:left;">Account No.<br/>#20220001</label>
                  <label style="height:53px;padding:8px;top:unset;margin-top:4px;justify-content:center;display:flex;align-items:center;font-size:16px;">Reguler</label>
                </div>
                <div class="product-info">
                  <div class="product-head">
                    <label>Biller Name</label>
                    <button class="btn-action btn-danger"><i class="lni lni-close" style="margin-right:6px"></i>Remove</button>
                    <button class="btn-action btn-info"><i class="bx bx-money" style="margin-right:6px"></i>Billing</button>
                    <button class="btn-action btn-warning"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                    <button class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>Details</button>
                  </div>
                  <div class="product-body">
                    <div class="col-xl-6 float-left">
                      <label>
                        <span>Phone</span>
                        <span>: 01923347911</span>
                      </label>
                      <label>
                        <span>Email</span>
                        <span>: shaan.sarkar143@gmail.com</span>
                      </label>
                      <label>
                        <span>Address</span>
                        <span>: Kaliganj, Gazipur, Dhaka</span>
                      </label>
                    </div>
                    <div class="col-xl-6 float-left">
                      <label>
                        <span style="width:50%;">Balance : 1000</span>
                        <span style="width:50%;">Last Bill : 1000</span>
                      </label>
                      <label style="float:right">
                        <span>Status</span>
                        <span class="status btn-danger">Due</span>
                      </label>
                      <div class="col-12 float-left">
                        <button class="btn-action btn-danger float-right">Cancel : 3</button>
                        <button class="btn-action btn-success float-right" style="margin-right:4px;">Done : 5</button>
                        <button class="btn-action btn-warning float-right" style="margin-right:4px;">Processing : 3</button>
                        <button class="btn-action btn-primary float-right" style="margin-right:4px;">Pending : 10</button>
                      </div>
                    </div>
                    <!-- <div class="col-xl-2 float-right">
                      <label>
                        <span>Cost</span>
                        <span>15000.00</span>
                      </label>
                      <label>
                        <span>Paid</span>
                        <span>10000.00</span>
                      </label>
                      <label>
                        <span>Rest</span>
                        <span>5000.00</span>
                      </label>
                    </div> -->
                  </div>
                </div>
              </div>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
