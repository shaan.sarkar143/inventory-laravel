@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .table thead th:not(:nth-child(3)),
        .table tbody td:not(:nth-child(3)) {
            text-align: center
        }

        .page-breadcrumb {
            position: relative;
        }

        .page-breadcrumb #print,
        .page-breadcrumb .total {
            position: absolute;
            right: 0;
            height: 40px;
            width: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
            column-gap: 10px;
            font-size: 17px;
        }

        .page-breadcrumb .total {
            right: 120px;
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Report</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Product&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Product Stock
                            </li>
                        </ol>
                    </nav>
                </div>
                <button class="total btn-action btn-danger">100000</button>
                <button id="print" class="btn-action btn-primary">
                    <i class="bx bx-printer"></i>Print
                </button>
            </div>
            <div class="card col-xl-12 mx-auto">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:60px">Sl.</th>
                                    <th style="width:150px">Color Name</th>
                                    <th>Product Name</th>
                                    <th style="width:150px">Total Supplier</th>
                                    <th style="width:150px">Current Stock</th>
                                    <th style="width:150px">Minimum QTY</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
