@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Supplier</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Discount Products List</li>
            </ol>
          </nav>
        </div>
      </div>
      <!--end breadcrumb-->
      <div class="row">
          <div class="col-xl-12 mx-auto">
              <div class="card p-3 pt-1">
                <div class="card-body">
                  <div class="table-responsive">
                    <?php for($x = 1; $x <= 30; $x++){?>
                      <div class="block-view">
                        <div class="code-image">
                          <label>#989009</label>
                          <img src="assets/images/logo-icon.png"/>
                        </div>
                        <div class="product-info">
                          <div class="product-head">
                            <label>Product Name</label>
                            <button class="btn-action btn-dark"><i class="bx bx-barcode-reader" style="margin-right:6px"></i>Barcode</button>
                            <button class="btn-action btn-danger"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                            <button class="btn-action btn-warning"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                            <button class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>View</button>
                          </div>
                          <div class="product-body">
                            <div class="col-xl-7 float-left">
                              <label>
                                <span>Brand</span>
                                <span>: Brand Name</span>
                              </label>
                              <label>
                                <span>Group</span>
                                <span>: Group Name</span>
                              </label>
                              <label>
                                <span>Category</span>
                                <span>: Category Name</span>
                              </label>
                            </div>
                            <div class="col-xl-3 float-left">
                              <label>
                                <span>Last Purs.</span>
                                <span>: 100</span>
                              </label>
                              <label>
                                <span>Min Qty.</span>
                                <span>: 10</span>
                              </label>
                              <label>
                                <span>Stock Qty.</span>
                                <span>: 230</span>
                              </label>
                            </div>
                            <div class="col-xl-2 float-right">
                              <label>
                                <span>Cost</span>
                                <span>100.00</span>
                              </label>
                              <label>
                                <span>MRP</span>
                                <span>130.00</span>
                              </label>
                              <label>
                                <span>Profit</span>
                                <span>30.00</span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php }?>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
