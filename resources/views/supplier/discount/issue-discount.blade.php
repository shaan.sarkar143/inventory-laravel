@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.min.css">
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Supplier</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Issue Discount</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-lg-12 mx-auto">
                          <div class="card p-3 pt-1">
                              <form action="#" method="post" name="">
                                  <div class="card-body col-xl-12 float-left">
                                      <div class="col-xl-4 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Search Supplier</label>
                                          <input class="form-control mb-3" type="text" placeholder="Search Supplier" aria-label="Search Supplier">
                                      </div>
                                      <div class="col-xl-4 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Supplier Balance</label>
                                          <input class="form-control mb-3" type="text" placeholder="0.00" aria-label="Supplier Balance" readonly>
                                      </div>
                                      <div class="col-xl-4 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Total Products</label>
                                          <input class="form-control mb-3" type="text" placeholder="0" aria-label="Total Products" readonly>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Category</label>
                                          <select class="form-control float-left">
                                              <option>Category Name</option>
                                          </select>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Brand</label>
                                          <select class="form-control float-left" disabled></select>
                                      </div>
                                      <div class="col-xl-6 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Search Product</label>
                                          <input class="form-control mb-3" type="text" placeholder="Search Product with Code/Name" aria-label="Search Product">
                                      </div>
                                      <div class="col-xl-12 px-3 float-left" style="margin-top:20px;">
                                        <div class="table-responsive">
                                          <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                              <tr>
                                                <th style="width:50px">Sl.</th>
                                                <th>Code</th>
                                                <th>Product Info</th>
                                                <th>Cost / Items</th>
                                                <th>Sale Disc.</th>
                                                <th>Action</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <?php for($x = 1; $x <= 5; $x++){?>
                                                <tr>
                                                  <td style="text-align:center"><?php echo $x;?></td>
                                                  <td style="min-width:70px;">989007</td>
                                                  <td>
                                                      <span class="float-left col-xl-12">Product Name</span>
                                                      <span class="float-left col-xl-6">Brand</span>
                                                      <span class="float-right col-xl-6" style="text-align:right">Category</span>
                                                  </td>
                                                  <td style="width:180px;">
                                                      <input type="text" placeholder="0.00" class="form-control float-left input-number" style="width:180px;" readonly/>
                                                  </td>
                                                  <td style="width:180px;">
                                                      <input type="text" placeholder="0.00" class="form-control float-left input-number" style="width:180px;"/>
                                                  </td>
                                                  <td>
                                                    <div class="row row-cols-auto g-3 mx-auto flex-center float-left" style="width:100px;">
                                                        <button class="btn-action btn-primary"><i class="bx bx-file"></i></button>
                                                        <button class="btn-action btn-danger"><i class="lni lni-trash"></i></button>
                                                    </div>
                                                  </td>
                                                </tr>
                                              <?php }?>
                                                <tr>
                                                    <td colspan="3" style="text-align:right">Total</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="width:67px;">
                                                      <div class="row row-cols-auto g-3 mx-auto flex-center float-left" style="width:100px;">
                                                          <button class="btn-action btn-danger"><i class="lni lni-trash" style="margin-right:3px"></i>Remove All</button>
                                                      </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                      <div class="col-xl-12 px-3 float-left">
                                          <label for="inputPassword" class="col-sm-12 col-form-label">Remarks</label>
                                          <textarea class="form-control mb-3"></textarea>
                                      </div>
                                      <div class="col-xl-12 px-3 float-left">
                                          <div class="d-flex gap-2 mt-3 float-start">
                                              <a href="{{ url('purchase-list') }}" class="btn btn-warning"><i class="bx bx-list-ol"></i>Discount Products List</a>
                                          </div>
                                          <div class="d-flex gap-2 mt-3 float-end">
                                              <button type="submit" class="btn btn-primary"><i class="bx bx-list-plus"></i>New Purchase</button>
                                          </div>
                                          <div class="d-flex gap-2 mt-3 float-end" style="margin-right:10px">
                                              <button type="reset" class="btn btn-danger"><i class="lni lni-eraser"></i>Clear</button>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="trumbowyg/dist/trumbowyg.min.js"></script>
<script>
    $('#editor').trumbowyg();
</script>
@endsection
