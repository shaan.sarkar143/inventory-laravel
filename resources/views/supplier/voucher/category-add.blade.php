@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Category</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Add New Category</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-lg-5 mx-auto">
                          <div class="card">
                              <div class="card-body col-xl-12 float-left">
                                  <div class="col-xl-12">
                                      <label for="inputPassword" class="col-sm-12 col-form-label">Category Name</label>
                                      <input class="form-control mb-3" type="text" placeholder="Category Name" aria-label="Category Name">
                                  </div>
                                  <div class="col-xl-12">
                                      <div class="d-flex gap-2 mt-3 float-start">
                                          <a href="{{ url('category-list') }}" class="btn btn-danger"><i class="bx bx-list-ol"></i>Category List</a>
                                      </div>
                                      <div class="d-flex gap-2 mt-3 float-end">
                                          <a href="javascript:;" class="btn btn-primary"><i class="bx bx-list-plus"></i>Add</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
@endsection
