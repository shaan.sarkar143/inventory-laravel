<?php 
    use \App\Http\Controllers\Supplier\SupplierList;
    function businessName($id){
        $info = SupplierList::business($id);
        return $info->name;
    }
?>
@extends("layouts.app")

@section("style")
<link href="<?= config('app.url'); ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Supplier</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Supplier List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
          <div class="col-md-6">
            <!-- Modal -->
            <div class="modal fade" id="supModal" aria-hidden="true" style="overflow-x:hidden;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Supplier Information Update</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="edit_sup">
                        </div>
                        <div class="modal-footer" style="display:inline-block">
                            <button form="sup_edit_form" type="submit" class="btn btn-primary float-right">Update</button>
                            <button form="sup_edit_form" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
      <div class="card col-xl-12 mx-auto">
        <div class="card-body">
          <div class="table-responsive">
            <?php 
                $x = 1;
                if(count($suppliers) > 0){
                   foreach($suppliers as $row => $v){
            ?>
              <div class="block-view">
                <div class="code-image">
                  <img src="<?= asset("storage/$v->photo") ?>"/>
                </div>
                <div class="code-image" style="left:150px;">
                  <label style="height:53px;line-height:18px;padding:8px;text-align:left;">ID No.<br/>#<?= $v->id ?></label>
                  <label style="height:53px;padding:8px;top:unset;margin-top:4px;justify-content:center;display:flex;align-items:center;font-size:16px;"><?= businessName($v->id) ?></label>
                </div>
                <div class="product-info" style="padding-left:300px">
                  <div class="product-head">
                    <label><?= $v->fname .' '. $v->lname ?></label>
                    <button class="btn-action btn-danger"><i class="lni lni-close" style="margin-right:6px"></i>Erase</button>
                    <button data-id="<?= $v->id ?>" class="btn-action btn-warning sup_edit"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                    <button class="btn-action btn-dark"><i class="lni lni-files" style="margin-right:6px"></i>Documents</button>
                    <button class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>Details</button>
                  </div>
                  <div class="product-body">
                    <div class="col-xl-6 float-left">
                      <label>
                        <span>Phone</span>
                        <span>: <?= $v->phone ?></span>
                      </label>
                      <label>
                        <span>Email</span>
                        <span>: <?= $v->email ?></span>
                      </label>
                      <label>
                        <span>Address</span>
                        <span>: <?= $v->address.', '.$v->thana.', '.$v->district ?></span>
                      </label>
                    </div>
                    <div class="col-xl-6 float-left">
                      <label>
                        <span>Advanced</span>
                        <span>: <?= number_format($v->advanced,2,'.','').' TK' ?></span>
                      </label>
                      <label style="float:right">
                        <span>Due</span>
                        <span>: <?= number_format($v->due,2,'.','').' TK' ?></span>
                      </label>
                      <label style="float:right">
                        <span>Balance</span>
                        <span>: <?= number_format($v->balance,2,'.','').' TK' ?></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            <?php $x++; }}else{?>
              <h3>No Supplier Found..<a href="{{ url('new-supplier') }}" style="float:right;font-size:15px;cursor:pointer;"><u>Add New</u></a></h3>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();

        $('body').on('click','.sup_edit', function(){
            let url = window.location.pathname;
            url = url + '/'+ $(this).data('id') + '/edit';
            $('#edit_sup').load(url,function(){
                $('#supModal').modal('show');
            });
        }); 
    });
</script>
@endsection
