<div class="row">
    <form class="col-lg-12" style="padding: 0 20px" action="{{ route('supplier-list.update', $id) }}" id="sup_edit_form" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="col-lg-12 card-title d-flex align-items-center float-left">
            <div><i class="bx bxs-user me-1 font-22 text-primary"></i></div>
            <h5 class="mb-0 text-primary">Basic Information</h5>
        </div>
        <div class="row">
            <div class="col-xl-6 float-left">
                <label class="form-label">First Name</label>
                <input name="first_name" type="text" class="form-control" value="<?= $profile->fname ?>" />
            </div>
            <div class="col-xl-6 float-left">
                <label class="form-label">Last Name</label>
                <input name="last_name" type="text" class="form-control" value="<?= $profile->lname ?>" />
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-4 float-left">
                <label class="form-label">Phone No.</label>
                <input name="phone" type="number" class="form-control" value="<?= $profile->phone ?>" />
            </div>
            <div class="col-xl-8 float-left">
                <label class="form-label">Email</label>
                <input name="email" type="email" class="form-control" value="<?= $profile->email ?>" />
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-8 float-left">
                <label for="inputAddress" class="form-label">Local Address</label>
                <input name="address" type="text" class="form-control" placeholder="Address..." value="<?= $profile->address ?>" />
            </div>
            <div class="col-xl-4 float-left">
                <label for="inputZip" class="form-label">Zip / Post Code</label>
                <input name="zip" type="number" class="form-control" value="<?= $profile->zip ?>" />
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6 float-left">
                <label class="form-label">Thana</label>
                <input name="thana" type="text" class="form-control" value="<?= $profile->thana ?>" />
            </div>
            <div class="col-xl-6 float-left">
                <label class="form-label">District</label>
                <input name="district" type="text" class="form-control" value="<?= $profile->district ?>" />
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6 float-left">
                <label class="form-label">Photo</label>
                <input name="photo" type="file" class="form-control" accept="image/jpg,image/png"/>
            </div>
        </div>
        <div class="col-lg-12 card-title d-flex align-items-center float-left" style="margin-top:35px;">
            <div><i class="bx bx-money me-1 font-22 text-primary"></i></div>
            <h5 class="mb-0 text-primary">Business Information</h5>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6 float-left">
                <label class="form-label">Business Name</label>
                <input name="name" type="text" class="form-control" value="<?= $business->name ?>"/>
            </div>
            <div class="col-xl-6 float-left">
                <label class="form-label">Bank Account No.</label>
                <input name="bank_ac" type="text" class="form-control" value="<?= $business->bank_ac ?>"/>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6 float-left">
                <label class="form-label">Trade License No.</label>
                <input name="trade_no" type="text" class="form-control" value="<?= $business->trade_no ?>"/>
            </div>
            <div class="col-xl-6 float-left">
                <label class="form-label">TIN & BIN</label>
                <input name="tin_bin" type="text" class="form-control" value="<?= $business->tin_bin ?>" />
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-12 float-left">
                <label class="form-label">Attachment</label>
                <input name="attachment" type="file" name="attachment[]" class="form-control" accept="application/msword,application/pdf,image/*" multiple="multiple"/>
            </div>
        </div>
        <div class="col-lg-12 card-title d-flex align-items-center float-left" style="margin-top:35px;">
            <div><i class="bx bx-money me-1 font-22 text-primary"></i></div>
            <h5 class="mb-0 text-primary">Accounts Information</h5>
        </div>
        <div class="row mt-3">
            <div class="col-xl-4 float-left">
                <label class="form-label">Advanced</label>
                <input name="advanced" type="text" class="form-control" value="<?= $profile->advanced ?>"/>
            </div>
            <div class="col-xl-4 float-left">
                <label class="form-label">Due</label>
                <input name="due" type="text" class="form-control" value="<?= $profile->due ?>" />
            </div>
            <div class="col-xl-4 float-left">
                <label class="form-label">Currnet Balance</label>
                <input name="balance" type="text" class="form-control" value="<?= $profile->balance ?>" />
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-12 float-left">
                <label class="form-label">Remarks</label>
                <textarea name="remarks" class="form-control" id="Remarks" placeholder="Remarks..." rows="3"> <?= $profile->remarks ?></textarea>
            </div>
        </div>
    </form>
</div>