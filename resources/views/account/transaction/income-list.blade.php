<?php
use App\Http\Controllers\Account\Transaction;
use App\Http\Controllers\Account\AccountSetup;
$transactions = Transaction::transactionList(1);
function findcatdetails($cat_id)
{
    $cat_info = AccountSetup::show($cat_id);
    return $cat_info->title;
}
function personinfo($id, $from)
{
    switch ($from) {
        case 1:
            return 'Company';
            break;
        case 2:
            break;
        case 2:
            break;
        case 2:
            break;
        case 2:
            break;
        default:
            return 'N/A';
            break;
    }
}
//var_dump($transactions);
?>
@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Transaction</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Income List</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card col-xl-12 mx-auto">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:30px">Sl.</th>
                                    <th>Trans. Date</th>
                                    <th>Trans. No.</th>
                                    <th>Acc. Category</th>
                                    <th>Payer Info.</th>
                                    <th>Receiver Info.</th>
                                    <th>Amount</th>
                                    <th style="width:180px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x = 1; ?>
                                @forelse ($transactions as $transaction)
                                    <tr>
                                        <td style="width:30px">{{ $x }}</td>
                                        <td>{{ date('d-M-Y', strtotime($transaction->trans_date)) }}</td>
                                        <td>{{ $transaction->trans_no }}</td>
                                        <td>{{ findcatdetails($transaction->trans_cat) }}</td>
                                        <td>Payer Info.</td>
                                        <td>Receiver Info.</td>
                                        <td>Amount</td>
                                        <td>
                                            <div class="row row-cols-auto g-3 mx-auto flex-center">
                                                <button class="btn-action btn-primary erase"><i class="lni lni-eye"
                                                        style="margin-right:6px"></i>View</button>
                                                <button class="btn-action btn-warning cat_edit"><i
                                                        class="lni lni-pencil-alt"
                                                        style="margin-right:6px"></i>Edit</button>
                                                <button class="btn-action btn-danger erase"><i class="lni lni-trash"
                                                        style="margin-right:6px"></i>Erase</button>
                                            </div>
                                        </td>
                                    </tr>
                                    {{ $x++ }}
                                @empty
                                    <tr>
                                        <td colspan="8">No records found.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
