@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Account</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Transaction&nbsp;&nbsp;&raquo;&nbsp;&nbsp;New Transaction</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-lg-10 mx-auto">
                          <div class="card">
                              <div class="card-body col-xl-12 float-left p-4">
                                  <form class="col-lg-12" action="{{ route('transaction.store') }}" method="post" id="formId">
                                     <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                        <div class="row">
                                          <div class="col-xl-3 float-left">
                                              <label class="col-sm-12 col-form-label">Transaction Date</label>
                                              <input name="trans_date" class="form-control mb-3" type="date" aria-label="Transaction Date">
                                          </div>
                                          <div class="col-xl-3 float-left">
                                              <label class="col-sm-12 col-form-label">Transaction No.</label>
                                              <input name="trans_no" class="form-control mb-3" type="text" value="<?= date('Ymd').mt_rand(111,999) ?>" readonly aria-label="Transaction No.">
                                          </div>
                                          <div class="col-xl-3 float-left">
                                              <label class="col-sm-12 col-form-label">Transaction Type</label>
                                              <select id="acc_type" name="trans_type" class="form-control mb-3 select_search" aria-label="Account Type">
                                                  <option value="" selected disabled>Choose --</option>
                                                  <option value="2">Bill Pay - Expense</option>
                                                  <option value="1">Bill Receive - Income</option>
                                              </select>
                                          </div>
                                          <div class="col-xl-3 float-right">
                                              <label class="col-sm-12 col-form-label">Category Name</label>
                                              <select id="category" name="trans_cat" class="form-control mb-3 select_search" aria-label="Category Name" disabled></select>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-xl-3 float-left">
                                              <label class="col-sm-12 col-form-label">Payment From</label>
                                              <select id="payer_choose" name="pay_from" class="form-control mb-3 select_search" aria-label="Payment From" disabled>
                                                  <option value="" selected disabled>Choose --</option>
                                                  <option value="1">Company</option>
                                                  <option value="2">Customer</option>
                                                  <option value="3">Supplier</option>
                                                  <option value="4">Employee</option>
                                                  <option value="5">Other</option>
                                              </select>
                                          </div>
                                          <div class="col-xl-3 float-left">
                                              <label class="col-sm-12 col-form-label">Payer Info</label>
                                              <select name="pay_info" id="payer" class="form-control mb-3 select_search" aria-label="Payer Info" disabled></select>
                                          </div>
                                          <div class="col-xl-3 float-right">
                                              <label class="col-sm-12 col-form-label">Payment To</label>
                                              <select id="receiver_choose" name="pay_to" class="form-control mb-3 select_search" aria-label="Payment To" disabled>
                                                  <option value="" selected disabled>Choose --</option>
                                                  <option value="1">Company</option>
                                                  <option value="2">Customer</option>
                                                  <option value="3">Supplier</option>
                                                  <option value="4">Employee</option>
                                                  <option value="5">Other</option>
                                              </select>
                                          </div>
                                          <div class="col-xl-3 float-left">
                                              <label class="col-sm-12 col-form-label">Receiver Info</label>
                                              <select name="rec_info" id="receiver" class="form-control mb-3 select_search" aria-label="Receiver Info" disabled></select>
                                          </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-3 float-left">
                                                <label class="col-sm-12 col-form-label">Paid Amount</label>
                                                <input name="paid_amount" class="form-control mb-3" type="text" placeholder="0.00" aria-label="Paid Amount">
                                            </div>
                                            <div class="col-xl-9 float-left">
                                                <label class="col-sm-12 col-form-label">Remarks</label>
                                                <input name="remarks" class="form-control mb-3" type="text" placeholder="Remarks" aria-label="Remarks">
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <label id="message" class="col-sm-6 col-form-label mb-3 form-message"></label>
                                            <button type="submit" class="btn btn-primary float-right" style="width: 130px;">
                                            <i class="bx bx-book-add" style="margin-right:6px;"></i>Add New</button>
                                            <button type="reset" class="btn btn-danger float-right" style="width: 130px;margin-right:10px;">
                                            <i class="lni lni-eraser" style="margin-right:6px;"></i>Clear</button>
                                        </div>
                                    </form>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script>
    $('.select_search').select2({
        height:'resolved',
        width:'100%'
    });
    $('body').on('change', '#acc_type', function(){
        let data = {
			'type': $(this).val()
		};
		let result = {};
        let options = '--Choose--';
		$.ajax({
			type: "GET",
			url: "{{ url('transaction/categoryList') }}",
			dataType: 'json',
            data: data,
			success: function (response)
			{
				result = response;
                if(result != undefined && result.length > 0){
                    $('#category').prop('disabled', false);
                    $('#payer_choose').prop('disabled', false);
                    $('#receiver_choose').prop('disabled', false);
                    $.each(result, function(i, val){
                        options += '<option value="'+val.id+'">'+val.name+'</option>';
                    });
                }else{
                    $('#category').prop('disabled', true);
                    $('#payer_choose').prop('disabled', true);
                    $('#receiver_choose').prop('disabled', true);
                    options = '<option value="">No Category Found</option>';
                }
			},
			error: function (er)
			{
				// let error = v.responseText;
				console.log(er);
			},
			complete: function ()
			{
				$('#category').html(options);
			}
		});
    });
    $('body').on('change', '#payer_choose', function(){
        if($('#acc_type').val() != undefined){
            let data = {
                'payer': $(this).val(),
                'acc_type': $('#acc_type').val()
            };
            let result = {};
            let options = '--Choose--';
            $.ajax({
                type: "GET",
                url: "{{ url('transaction/payerInfo') }}",
                dataType: 'json',
                data: data,
                success: function (response)
                {
                    result = response;
                    console.log(result);
                    if(result != undefined && result.length > 0){
                        $('#payer').prop('disabled', false);
                        $.each(result, function(i, val){
                            options += '<option value="'+val.id+'">'+val.name+'</option>';
                        });
                    }else{
                        $('#payer').prop('disabled', true);
                        options = '<option value="">No Data Found</option>';
                    }
                },
                error: function (er)
                {
                    // let error = v.responseText;
                    console.log(er);
                },
                complete: function ()
                {
                    $('#payer').html(options);
                }
            });
        }else{
            e.preventDefault();
            alert('Select Transaction type first.');
        }
    });
    $('body').on('change', '#receiver_choose', function(){
        if($('#acc_type').val() != undefined){
            let data = {
                'receiver': $(this).val(),
                'acc_type': $('#acc_type').val()
            };
            let result = {};
            let options = '--Choose--';
            $.ajax({
                type: "GET",
                url: "{{ url('transaction/receiverInfo') }}",
                dataType: 'json',
                data: data,
                success: function (response)
                {
                    result = response;
                    if(result != undefined && result.length > 0){
                        $('#receiver').prop('disabled', false);
                        $.each(result, function(i, val){
                            options += '<option value="'+val.id+'">'+val.name+'</option>';
                        });
                    }else{
                        $('#receiver').prop('disabled', true);
                        options = '<option value="">No Data Found</option>';
                    }
                },
                error: function (er)
                {
                    // let error = v.responseText;
                    console.log(er);
                },
                complete: function ()
                {
                    $('#receiver').html(options);
                }
            });
        }else{
            alert('Select Transaction type first.');
        }
    });
</script>
@endsection
