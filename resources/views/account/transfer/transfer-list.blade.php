@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Category</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Category List</li>
            </ol>
          </nav>
        </div>
        <!-- <div class="ms-auto">
          <div class="btn-group">
            <button type="button" class="btn btn-primary">Settings</button>
            <button type="button" class="btn btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown">	<span class="visually-hidden">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end">	<a class="dropdown-item" href="javascript:;">Action</a>
              <a class="dropdown-item" href="javascript:;">Another action</a>
              <a class="dropdown-item" href="javascript:;">Something else here</a>
              <div class="dropdown-divider"></div>	<a class="dropdown-item" href="javascript:;">Separated link</a>
            </div>
          </div>
        </div> -->
      </div>
      <!--end breadcrumb-->
      <!-- <h6 class="mb-0 text-uppercase">DataTable Example</h6>
      <hr/> -->
      <div class="card col-xl-7 mx-auto">
        <div class="card-body">
          <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th style="width:8%">Sl.</th>
                  <th>Name</th>
                  <th style="width:40%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php for($x = 1; $x <= 100; $x++){?>
                <tr>
                  <td><?php echo $x;?></td>
                  <td>System Architect</td>
                  <td>
                    <div class="row row-cols-auto g-3 mx-auto">
                        <button class="btn-action btn-primary">Edit</button>
                        <!-- <div class="d-flex float-end">
                            <a href="javascript:;" class="btn btn-primary"><i class="bx bx-list-plus"></i>Add</a>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-info">View</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-primary">Edit</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">Delete</button>
                        </div> -->
                    </div>
                  </td>
                </tr>
              <?php }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
