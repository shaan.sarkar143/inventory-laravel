<?php 
    use \App\Http\Controllers\Account\AccountSetup;
    $category = AccountSetup::categoryList(2);
?>
@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Account</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Account Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Expense Category</li>
            </ol>
          </nav>
        </div>
      </div>                
      <div class="row">
          <div class="col-md-6">
            <!-- Modal -->
            <div class="modal fade" id="catModal" aria-hidden="true" style="overflow-x:hidden;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Expense Category Update</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="edit_cat">
                        </div>
                        <div class="modal-footer" style="display:inline-block">
                            <button form="cat_edit_form" type="submit" class="btn btn-primary float-right">Update</button>
                            <button form="cat_edit_form" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
      <div class="card col-xl-8 mx-auto">
        <div class="card-body">
          <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th style="width:50px">Sl.</th>
                  <th style="width:200px">Name</th>
                  <th>Remarks</th>
                  <th style="width:120px">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $x = 1; foreach($category as $row => $v){?>
                <tr>
                  <td><?= $x ?></td>
                  <td><?= ucfirst($v->title) ?></td>
                  <td><?= ucfirst($v->remarks) ?></td>
                  <td>
                    <div class="row row-cols-auto g-3 mx-auto flex-center">
                      <button data-id="<?= $v->id ?>" class="btn-action btn-warning cat_edit"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                      <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                    </div>
                  </td>
                </tr>
              <?php $x++; }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('body').on('click','.cat_edit', function(){
            let url = window.location.pathname;
            url = url + '/cat-update/'+ $(this).data('id');
            $('#edit_cat').load(url,function(){
                $('.select_search').select2({
                    dropdownParent: $("#catModal"),
                    width:'100%'
                });
                $('#catModal').modal('show');
            });
        }); 
    });
</script>
@endsection