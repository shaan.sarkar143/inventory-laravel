@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Account</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Account Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;New Category</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-lg-8 mx-auto">
                          <div class="card">
                              <div class="card-body col-xl-12 float-left p-4">
                                  <form class="col-lg-12" action="{{ route('account-setup.store') }}" method="post" id="proCategory" enctype="multipart/form-data">
                                     <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                        <div class="row">
                                          <div class="col float-left">
                                              <label class="col-sm-12 col-form-label">Account Type</label>
                                              <select name="acc_type" class="form-control mb-3 select_search" aria-label="Account Type">
                                                  <option value="" selected disabled>Choose --</option>
                                                  <option value="1">Income</option>
                                                  <option value="2">Expense</option>
                                              </select>
                                          </div>
                                          <div class="col float-right">
                                              <label class="col-sm-12 col-form-label">Category Name</label>
                                              <input name="cat_name" class="form-control mb-3" type="text" placeholder="Category Name" aria-label="Category Name">
                                          </div>
                                        </div>
                                        <div class="row">
                                            <div class="col float-left">
                                                <label class="col-sm-12 col-form-label">Remarks</label>
                                                <input name="remarks" class="form-control mb-3" type="text" placeholder="Remarks" aria-label="Remarks">
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <label id="message" class="col-sm-6 col-form-label mb-3 form-message"></label>
                                            <button type="submit" class="btn btn-primary float-right" style="width: 130px;">
                                            <i class="bx bx-book-add" style="margin-right:6px;"></i>Add New</button>
                                            <button type="reset" class="btn btn-danger float-right" style="width: 130px;margin-right:10px;">
                                            <i class="lni lni-eraser" style="margin-right:6px;"></i>Clear</button>
                                        </div>
                                    </form>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script>
    $('.select_search').select2({
        height:'resolved',
        width:'100%'
    });
</script>
@endsection
