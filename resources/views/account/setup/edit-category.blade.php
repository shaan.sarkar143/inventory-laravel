<?php 
    use \App\Http\Controllers\Account\AccountSetup;
    $cat = AccountSetup::show($cat_id);
?>

<div class="row">
    <form class="col-lg-12" style="padding: 0 20px" action="{{ route('account-setup.update', $cat->id) }}" id="cat_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col float-left">
                <label class="col-sm-12 col-form-label">Account Type</label>
                <select name="acc_type" class="form-control mb-3 select_search" aria-label="Account Type" required>
                    <option value="1" <?= ($cat->acc_type == 1) ? 'selected': ''?>>Income</option>
                    <option value="2" <?= ($cat->acc_type == 2) ? 'selected': ''?>>Expense</option>
                </select>
            </div>
            <div class="col float-right">
                <label class="col-sm-12 col-form-label">Category Name</label>
                <input name="cat_name" class="form-control mb-3" type="text" placeholder="Category Name" value="<?= $cat->title ?>" aria-label="Category Name" required>
            </div>
        </div>
        <div class="row">
            <div class="col float-left">
                <label class="col-sm-12 col-form-label">Remarks</label>
                <input name="remarks" class="form-control mb-3" type="text" placeholder="Remarks" value="<?= $cat->remarks ?>" aria-label="Remarks">
            </div>
        </div>
    </form>
</div>