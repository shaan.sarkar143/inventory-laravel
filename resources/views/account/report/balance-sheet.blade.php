@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .table thead th:first-child,
        .table tbody td:first-child,
        .table thead th:nth-child(2),
        .table tbody td:nth-child(2) {
            text-align: center;
        }

        .table thead th:nth-child(3),
        .table tbody td:nth-child(3),
        .table thead th:nth-child(5),
        .table tbody td:nth-child(5) {
            text-align: left;
        }

        .table thead th:nth-child(6),
        .table tbody td:nth-child(6),
        .table thead th:nth-child(7),
        .table tbody td:nth-child(7),
        .table thead th:nth-child(4),
        .table tbody td:nth-child(4) {
            text-align: right;
            padding-right: 10px;
        }

        .page-breadcrumb {
            position: relative;
        }

        .page-breadcrumb #print,
        .page-breadcrumb .total,
        .page-breadcrumb .date {
            position: absolute;
            right: 0;
            height: 40px;
            width: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
            column-gap: 10px;
            font-size: 17px;
        }

        .page-breadcrumb .total {
            right: 120px;
        }

        .page-breadcrumb .date {
            right: 240px;
            width: 200px;
            padding: 0 10px;
            border: 1px solid #ddd;
            border-radius: 3px;
            cursor: pointer;
        }

        .page-breadcrumb .date:nth-child(5) {
            position: absolute;
            right: 480px;
        }

        .page-breadcrumb span:nth-child(4) {
            position: absolute;
            right: 450px;
            font-size: 18px;
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Accounts</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Account Statements&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Balance Sheet
                            </li>
                        </ol>
                    </nav>
                </div>
                <input type="date" class="date" value="<?= date('Y-m-d') ?>">
                <span>To</span>
                <input type="date" class="date" value="<?= date('Y-m-d') ?>">
                <button class="total btn-action btn-danger">10000</button>
                <button id="print" class="btn-action btn-primary">
                    <i class="bx bx-printer"></i>Print
                </button>
            </div>
            <div class="card col-xl-12 mx-auto">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th colspan="10">
                                        Balance Sheet of &nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?= date('d-M-Y') ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="10" style="text-align: left">
                                        Opening Balance : 10000 TK
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width:60px">Sl.</th>
                                    <th style="width:150px">Date</th>
                                    <th>Income Head</th>
                                    <th style="width:150px">Amount</th>
                                    <th>Expense Head</th>
                                    <th style="width:150px">Amount</th>
                                    <th style="width:150px">Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:60px">1</td>
                                    <td style="width:150px"><?= date('d-M-Y') ?></td>
                                    <td>Sale</td>
                                    <td style="width:150px">10000</td>
                                    <td>N/A</td>
                                    <td style="width:150px">0.00</td>
                                    <td style="width:150px">20000</td>
                                </tr>
                                <tr>
                                    <td style="width:60px">2</td>
                                    <td style="width:150px"><?= date('d-M-Y') ?></td>
                                    <td>N/A</td>
                                    <td style="width:150px">0.00</td>
                                    <td>Electric Bill</td>
                                    <td style="width:150px">2000</td>
                                    <td style="width:150px">18000</td>
                                </tr>
                                <tr>
                                    <td style="width:60px">3</td>
                                    <td style="width:150px"><?= date('d-M-Y') ?></td>
                                    <td>Sale</td>
                                    <td style="width:150px">10000</td>
                                    <td>N/A</td>
                                    <td style="width:150px">0.00</td>
                                    <td style="width:150px">28000</td>
                                </tr>
                                <tr>
                                    <td colspan="6" style="text-align: right">Balance </td>
                                    <td style="text-align: right">28000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
