@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .table thead th:not(:nth-child(3)),
        .table tbody td:not(:nth-child(3)) {
            text-align: center;
        }

        .table thead th:nth-child(5),
        .table tbody td:nth-child(5),
        .table thead th:nth-child(6),
        .table tbody td:nth-child(6) {
            text-align: right;
            padding-right: 10px;
        }

        .page-breadcrumb {
            position: relative;
        }

        .page-breadcrumb #print,
        .page-breadcrumb .total,
        .page-breadcrumb .date {
            position: absolute;
            right: 0;
            height: 40px;
            width: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
            column-gap: 10px;
            font-size: 17px;
        }

        .page-breadcrumb .total {
            right: 120px;
        }

        .page-breadcrumb .date {
            right: 240px;
            width: 200px;
            padding: 0 10px;
            border: 1px solid #ddd;
            border-radius: 3px;
            cursor: pointer;
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Accounts</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Account Statements&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Daily Statement
                            </li>
                        </ol>
                    </nav>
                </div>
                <input type="date" class="date" value="<?= date('Y-m-d') ?>">
                <button class="total btn-action btn-danger">100000</button>
                <button id="print" class="btn-action btn-primary">
                    <i class="bx bx-printer"></i>Print
                </button>
            </div>
            <div class="card col-xl-12 mx-auto">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th colspan="10">
                                        Account Statement of &nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?= date('d-M-Y') ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width:60px">Sl.</th>
                                    <th style="width:150px">Trans. No.</th>
                                    <th>Trans. Info</th>
                                    <th style="width:150px">Trans. Type</th>
                                    <th style="width:150px">Debit</th>
                                    <th style="width:150px">Credit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td rowspan="2" style="width:60px">1</td>
                                    <td style="width:150px">989009</td>
                                    <td>Trans. Info</td>
                                    <td style="width:150px">Trans. Type</td>
                                    <td style="width:150px">Debit</td>
                                    <td style="width:150px">Credit</td>
                                </tr>
                                <tr>
                                    <td colspan="6" style="text-align: left">Remarks : </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
