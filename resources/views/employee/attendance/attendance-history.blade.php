@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Employee</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item">
                                    <a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Attendance History</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-xl-12 mx-auto">
                          <div class="card p-3 pt-1">
                              <div class="card-body col-xl-12 float-left">
                                  <form class="col-lg-12" action="#" method="post" name="">
                                      <div class="row">
                                          <div class="col float-left">
                                              <label for="inputPassword" class="col-sm-12 col-form-label">Employee Name</label>
                                              <select class="form-control mb-3" aria-label="Group Name">
                                                  <option>Choose Employee</option>
                                              </select>
                                          </div>
                                          <div class="col float-left">
                                              <label for="arrival_time" class="col-sm-12 col-form-label">Date From</label>
                                              <input class="form-control mb-3" type="date" aria-label="Arrival Time">
                                          </div>
                                          <div class="col float-left">
                                              <label for="arrival_time" class="col-sm-12 col-form-label">Date To</label>
                                              <input class="form-control mb-3" type="date" aria-label="Arrival Time">
                                          </div>
                                          <div class="col float-right">
                                              <label for="attendanceType" class="col-sm-12 col-form-label">Attendance Type</label>
                                              <select class="form-control mb-3" aria-label="Attendance Type">
                                                  <option selected>Present</option>
                                                  <option>Absent</option>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col float-right">
                                              <div class="d-flex gap-2 float-end">
                                                  <button type="submit" class="btn btn-primary"><i class="bx bx-list-plus"></i>Search</button>
                                              </div>
                                              <div class="d-flex gap-2 float-end" style="margin-right:10px">
                                                  <button type="reset" class="btn btn-danger"><i class="lni lni-eraser"></i>Clear</button>
                                              </div>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                              <hr class="col-xl-12 float-left"/>
                              <div class="card-body col-xl-12 float-left">
                                <div class="table-responsive col-xl-12 float-left" style="overflow:hidden;">
                                  <table class="table table-striped table-bordered" style="width:100%;overflow:hidden;">
                                    <thead>
                                      <tr>
                                        <th style="width:50px;text-align:center;">Sl.</th>
                                        <th>Employee Name</th>
                                        <th style="min-width:150px;text-align:center;">Department</th>
                                        <th style="min-width:120px;text-align:center;">Status</th>
                                        <th style="min-width:120px;text-align:center;">Arrival Time</th>
                                        <th style="width:150px;text-align:center;">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php for($x = 1; $x <= 20; $x++){?>
                                      <tr>
                                        <td style="text-align:center;"><?php echo $x;?></td>
                                        <td>Mr. X</td>
                                        <td style="text-align:center;">HR</td>
                                        <td style="text-align:center;">Present</td>
                                        <td style="text-align:center;">10:00 AM</td>
                                        <td>
                                          <div class="row row-cols-auto g-3 mx-auto flex-center">
                                              <button class="btn-action btn-primary"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                                              <button class="btn-action btn-danger"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                          </div>
                                        </td>
                                      </tr>
                                    <?php }?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
