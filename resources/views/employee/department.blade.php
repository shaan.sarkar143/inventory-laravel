@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Employee</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Department Manage</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-xl-10 mx-auto">
                          <div class="card p-3 pt-1">
                              <div class="card-body col-xl-12 float-left">
                                  <form class="col-lg-12" action="{{ route('employee-department.store') }}" method="post" id="employeeDept" enctype="multipart/form-data">
                                      <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                      <div class="row">
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">Dept. Name</label>
                                              <input name="name" class="form-control mb-3" type="text" placeholder="Department Name" aria-label="Department Name" required>
                                          </div>
                                          <div class="col-xl-8 float-left">
                                              <label class="col-sm-12 col-form-label">Dept. About</label>
                                              <input name="about" class="form-control mb-3" type="text" placeholder="Short Details about Department" aria-label="Dept. About">
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">Dept. email</label>
                                              <input name="email" class="form-control mb-3" type="email" placeholder="Department Email" aria-label="Dept. email">
                                          </div>
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">Contact No.</label>
                                              <input name="contact_no" class="form-control mb-3" type="text" placeholder="IP Phone/Contact No." aria-label="Department Name">
                                          </div>
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">Image</label>
                                              <input name="image" class="form-control mb-3" type="file" accept="image/jpeg" aria-label="Department Image">
                                          </div>
                                      </div>
                                      <div class="row mt-3">
                                          <label id="message" class="col-sm-6 col-form-label float-left form-message"></label>
                                          <div class="col-xl-6 float-right">
                                              <button type="submit" class="btn btn-primary float-right"><i class="bx bx-list-plus"></i>Add</button>
                                              <button type="reset" class="btn btn-danger float-right" style="margin-right:20px"><i class="lni lni-eraser"></i>Clear</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                              <hr class="col-xl-12 float-left"/>
                              <div class="card-body col-xl-12 float-left">
                                <div class="table-responsive col-xl-12 float-left" style="overflow:hidden;">
                                  <table id="example" class="table table-striped table-bordered" style="width:100%;overflow:hidden;">
                                    <thead>
                                      <tr>
                                        <th style="width:20px">Sl.</th>
                                        <th>Dept. Name</th>
                                        <th style="width: 450px">Dept. About</th>
                                        <th>Email</th>
                                        <th>Contact No.</th>
                                        <th style="width:120px">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 1; foreach( $dept as $row => $v ){?>
                                      <tr>
                                        <td><?= $x ?></td>
                                        <td><?= $v->name ?></td>
                                        <td style="word-break: break-all"><?= $v->about ?></td>
                                        <td><?= $v->email ?></td>
                                        <td><?= $v->contact_no ?></td>
                                        <td>
                                          <div class="row row-cols-auto g-3 mx-auto flex-center">
                                              <button class="btn-action btn-primary"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                                              <button class="btn-action btn-danger"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                          </div>
                                        </td>
                                      </tr>
                                    <?php $x++; }?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('.select_search').select2({
            height:'resolved'
        });
    });
</script>
@endsection
