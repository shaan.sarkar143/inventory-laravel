<?php 
    use \App\Http\Controllers\Setting\EmployeeType;
    use \App\Http\Controllers\Employee\DepartmentSetup;
    $types = EmployeeType::create();
    $departments = DepartmentSetup::create();
?>
@extends("layouts.app")
@section("wrapper")
        <div class="page-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Employee</div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add New Employee</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row">
                    <div class="col-xl-7 mx-auto">
                        <div class="card border-top border-0 border-4 border-primary">
                            <div class="card-body p-5">
                              <form class="row g-3" action="{{ route('employee-list.store') }}" method="post" id="newEmployee" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                    <div class="card-title d-flex align-items-center">
                                        <div><i class="bx bxs-user me-1 font-22 text-primary"></i>
                                        </div>
                                        <h5 class="mb-0 text-primary">Basic Information</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">First Name</label>
                                        <input name="first_name" type="text" class="form-control"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">Last Name</label>
                                        <input name="last_name" type="text" class="form-control"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Phone No.</label>
                                        <input name="phone" type="number" class="form-control"/>
                                    </div>
                                    <div class="col-md-8">
                                        <label class="form-label">Email</label>
                                        <input name="email" type="email" class="form-control"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Thana</label>
                                        <input name="thana" type="text" class="form-control"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">District</label>
                                        <input name="district" type="text" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Zip</label>
                                        <input name="zip" type="number" class="form-control" />
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Local Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Address..." />
                                    </div>
                                    <div class="card-title d-flex align-items-center" style="margin-top:35px;">
                                        <div><i class="bx bx-money me-1 font-22 text-primary"></i></div>
                                        <h5 class="mb-0 text-primary">Accounts Information</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Department</label>
                                        <select name="dept_id" class="form-control mb-3 select_search" aria-label="Department">
                                            <option value="" selected disabled>Choose--</option>
                                            <?php foreach($departments as $row => $v){?>
                                                <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Designation</label>
                                        <input name="designation" type="text" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Employee Type</label>
                                        <select name="type_id" class="form-control mb-3 select_search" aria-label="Employee Type">
                                            <option value="" selected disabled>Choose--</option>
                                            <?php foreach($types as $row => $v){?>
                                                <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Salary</label>
                                        <input name="salary" type="text" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Joining Date</label>
                                        <input name="joining_date" type="date" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Photo</label>
                                        <input name="photo" type="file" class="form-control" accept="image/*">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">Attachment</label>
                                        <input name="attachment" type="file" class="form-control" accept="application/pdf,application/msword"/>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="form-label">Remarks</label>
                                        <textarea name="remarks" class="form-control" placeholder="Remarks..." rows="3"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label id="message" class="col-sm-12 col-form-label mb-3 form-message"></label>
                                        <button type="reset" class="btn btn-danger float-left">
                                          <i class="lni lni-eraser" style="margin-right:6px;"></i>Clear</button>
                                        <button type="submit" class="btn btn-primary float-right">
                                          <i class="bx bx-book-add" style="margin-right:6px;"></i>Add New</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
@endsection
@section("script")
<script>
	$('.select_search').select2({
        height:'resolved'
    });
</script>
@endsection