@extends("layouts.app")

@section("style")
<link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Employee</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Employee List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="card col-xl-12 mx-auto">
        <div class="card-body">
          <div class="table-responsive">
            <?php $x = 1; if(count($employee) > 0){ foreach($employee as $row => $v){?>
              <div class="block-view">
                <div class="code-image">
                  <img src="<?= asset("storage/$v->photo") ?>"/>
                </div>
                <div class="code-image" style="left:150px;">
                  <label style="height:53px;line-height:18px;padding:8px;text-align:left;">ID No.<br/>#<?= $v->id ?></label>
                  <label style="height:53px;padding:8px;top:unset;margin-top:4px;justify-content:center;display:flex;align-items:center;font-size:16px;"><?= $v->designation ?></label>
                </div>
                <div class="product-info" style="padding-left:300px">
                  <div class="product-head">
                    <label><?= $v->name ?></label>
                    <button class="btn-action btn-danger"><i class="lni lni-close" style="margin-right:6px"></i>Remove</button>
                    <button class="btn-action btn-warning"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                    <button class="btn-action btn-primary"><i class="lni lni-eye" style="margin-right:6px"></i>Details</button>
                  </div>
                  <div class="product-body">
                    <div class="col-xl-6 float-left">
                      <label>
                        <span>Phone</span>
                        <span>: <?= $v->phone ?></span>
                      </label>
                      <label>
                        <span>Email</span>
                        <span>: <?= $v->email ?></span>
                      </label>
                      <label>
                        <span>Address</span>
                        <span>: <?= $v->address.', '.$v->thana.', '.$v->district ?></span>
                      </label>
                    </div>
                    <div class="col-xl-6 float-left">
                      <label>
                        <span>Salary</span>
                        <span>: <?= number_format($v->salary,2,'.','').' TK' ?></span>
                      </label>
                      <label style="float:right">
                        <span>Paid</span>
                        <span>: 0.00 TK</span>
                      </label>
                      <label style="float:right">
                        <span>Advanced</span>
                        <span>: 0.00 TK</span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
                    <?php $x++; }}else{?>
                      <h3>No Employee Found..<a href="{{ url('new-employee') }}" style="float:right;font-size:15px;cursor:pointer;"><u>Add New</u></a></h3>
                    <?php }?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection

@section("script")
<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection
