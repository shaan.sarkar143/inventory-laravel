@extends("layouts.app")
@section("style")
<link href="<?= config('app.url'); ?>assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
<link href="<?= config('app.url'); ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection
@section("wrapper")
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Product</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Company Setup</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="row">
         <div class="col-md-6">
            <!-- Modal -->
            <div class="modal fade" id="companyModal" aria-hidden="true" style="overflow-x:hidden;">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title">Update Company Information</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                     </div>
                     <div class="modal-body" id="edit_company">
                     </div>
                     <div class="modal-footer" style="display:inline-block">
                        <button form="companySetup" type="submit" class="btn btn-primary float-right">Update</button>
                        <button form="companySetup" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <?php if(empty($company)){?>
         <div class="col-xl-10 mx-auto" id="setup_form" style="margin-top: 20px">
            <div class="card p-3 pt-1">
               <div class="card-body col-xl-12 float-left">
                  <div id="loader" class="form-loader"></div>
                  <form class="col-lg-12" data-formhide="setup_form" action="{{ route('company.store') }}" method="post" id="comapnySetup" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                     {{-- @csrf --}}
                     <div class="row">
                        <div class="col-xl-4 float-left">
                           <label class="col-sm-12 col-form-label">Company Name</label>
                           <input class="form-control mb-3" name="name" type="text" placeholder="Company Name" aria-label="Company Name">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">Email Address</label>
                           <input class="form-control mb-3" name="email" type="email" placeholder="Email Address" aria-label="Email Address">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">Phone No.</label>
                           <input class="form-control mb-3" name="phone" type="text" placeholder="Phone No." aria-label="Phone No.">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-4 float-left">
                           <label class="col-sm-12 col-form-label">City</label>
                           <input class="form-control mb-3" name="city" type="text" placeholder="City Name" aria-label="City Name">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">State</label>
                           <input class="form-control mb-3" name="state" type="text" placeholder="State name" aria-label="State Name">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">Zip/Post Code</label>
                           <input class="form-control mb-3" name="zip" type="number" placeholder="Zip/Post Code" aria-label="Zip/Post Code">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-8 float-left">
                           <label class="col-sm-12 col-form-label">Local Address</label>
                           <input class="form-control mb-3" name="address" type="text" placeholder="Local Address" aria-label="Local Address">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">Est. Year</label>
                           <input class="form-control mb-3" name="est_year" type="number" placeholder="Est. Year" aria-label="Est. Year">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-3 float-left">
                           <label class="col-sm-12 col-form-label">Account Started</label>
                           <input class="form-control mb-3" name="started" type="date" aria-label="Account Started">
                        </div>
                        <div class="col-xl-3 float-right">
                           <label class="col-sm-12 col-form-label">Initial Invest</label>
                           <input class="form-control mb-3" name="invest" type="number" placeholder="0.00" aria-label="Initial Invest">
                        </div>
                        <div class="col-xl-3 float-right">
                           <label class="col-sm-12 col-form-label">Expense</label>
                           <input class="form-control mb-3" name="expense" type="text" placeholder="0.00" aria-label="Expense">
                        </div>
                        <div class="col-xl-3 float-right">
                           <label class="col-sm-12 col-form-label">Income</label>
                           <input class="form-control mb-3" name="income" type="text" placeholder="0.00" aria-label="Income">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-3 float-right">
                           <label class="col-sm-12 col-form-label">Balance</label>
                           <input class="form-control mb-3" name="balance" type="text" placeholder="0.00" aria-label="Balance">
                        </div>
                        <div class="col-xl-3 float-right">
                           <label class="col-sm-12 col-form-label">Receivable Amount</label>
                           <input class="form-control mb-3" name="amount_due" type="number" placeholder="0.00" aria-label="Receivable Amount">
                        </div>
                        <div class="col-xl-3 float-right">
                           <label class="col-sm-12 col-form-label">Payable Amount</label>
                           <input class="form-control mb-3" name="amount_pay" type="number" placeholder="0.00" aria-label="Payable Amount">
                        </div>
                        <div class="col-xl-3 float-left">
                           <label class="col-sm-12 col-form-label">Logo</label>
                           <input class="form-control mb-3" name="logo" type="file" accept="image/jpeg" aria-label="Logo">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">Invoice Start From</label>
                           <input class="form-control mb-3" name="invoice_start" type="text" placeholder="<?= date('Ym').'00001'?>" aria-label="Invoice Start From">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label">Product Code Start From</label>
                           <input class="form-control mb-3" name="code_start" type="text" placeholder="<?= date('Ym').'00001'?>" aria-label="Product Code Start From">
                        </div>
                        <div class="col-xl-4 float-right">
                           <label class="col-sm-12 col-form-label"></label>
                           <button type="submit" class="btn btn-primary float-right" style="margin-top:10px;">Setup</button>
                        </div>
                     </div>
                     <div class="col-xl-12 float-left">
                        <label id="message" class="col-sm-12 col-form-label mt-3 form-message"></label>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <?php }else { $info = $company[0];?>
         <div class="col-xl-10 mx-auto" style="margin-top: 20px">
            <div class="card">
               <div class="card-body p-5">
                  <div class="col-xl-4 float-left">
                     <div class="d-flex flex-column align-items-center text-center">
                        <img src="<?= asset("storage/$info->logo") ?>" alt="Admin" class="p-2" style="width:50%;border:none;">
                        <div class="mt-3">
                           <h4><?= $info->name ?></h4>
                        </div>
                     </div>
                     <hr class="my-4" />
                     <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                           <h6 class="mb-0">Invest</h6>
                           <span class="text-secondary"><?= number_format($info->invest, 2, '.', '') ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                           <h6 class="mb-0">Expense</h6>
                           <span class="text-secondary"><?= number_format($info->expense, 2, '.', '') ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                           <h6 class="mb-0">Income</h6>
                           <span class="text-secondary"><?= number_format($info->income, 2, '.', '') ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                           <h6 class="mb-0">Balance</h6>
                           <span class="text-secondary"><?= number_format($info->balance, 2, '.', '') ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                           <h6 class="mb-0">Receivable</h6>
                           <span class="text-secondary"><?= number_format($info->amount_due, 2, '.', '') ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                           <h6 class="mb-0">Payable</h6>
                           <span class="text-secondary"><?= number_format($info->amount_pay, 2, '.', '') ?></span>
                        </li>
                     </ul>
                  </div>
                  <div class="col-md-7 float-right">
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Est. Year</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->est_year ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Email Address</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->email ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Phone No.</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->phone ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">City</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->city ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">State</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->state ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Zip/Post Code</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->zip ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Local Address</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->address ?>" readonly/>
                        </div>
                     </div>
                     <div class="row mb-3">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Invoice Start</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->invoice_start ?>" readonly/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-3">
                           <h6 class="mb-0 pt-2">Product Code Start</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                           <input type="text" class="form-control" value="<?= $info->code_start ?>" readonly/>
                        </div>
                     </div>                 
                     <div class="col-sm-3 float-right">
                        <div class="d-flex gap-2 float-end mt-3">
                           <button data-id="<?= $info->id ?>" type="button" id="update_info" class="btn btn-warning" style="margin-top: 73px;">
                              <i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit Information</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php }?>
      </div>
   </div>
</div>
@endsection
@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script>
   $(document).ready(function() {
       $('#example').DataTable();
   });
   $('body').on('click','#update_info', function(){
      let url = window.location.pathname;
      url = url + '/'+ $(this).data('id') + '/edit';
      $('#edit_company').load(url,function(){
            $('#companyModal').modal('show');
      });
   }); 
</script>
@endsection