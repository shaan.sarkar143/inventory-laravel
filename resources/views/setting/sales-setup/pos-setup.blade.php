@extends('layouts.app')
@section('style')
    <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
    <style scoped>
        * {
            position: relative;
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            height: auto;
        }

        img {
            width: auto;
            height: auto;
        }

        a {
            text-decoration: none;
        }

        ul li {
            list-style: none;
        }

        .inv_container {
            height: auto;
            width: 100%;
            float: left;
            overflow: hidden;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .inv_container .invoice {
            width: 314px;
            height: auto;
            float: left;
            position: relative;
            margin: auto;
            background-color: #eeeeee63;
        }

        .inv_container .invoice .header {
            height: auto;
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: space-between;
            padding: 37px 10px 0px;
        }

        .inv_container .invoice .header h2 {
            position: absolute;
            top: 0;
            left: 0;
            font-size: 12px;
            line-height: 17px;
            padding: 3px 10px;
            width: 100%;
            font-weight: 400;
        }

        .inv_container .invoice .header img {
            max-height: 100px;
            max-width: 100px;
        }

        .inv_container .invoice .header ul li {
            font-size: 13px;
            line-height: 17px;
            width: 100%;
        }

        .inv_container .invoice .customer,
        .inv_container .invoice .items,
        .inv_container .invoice .footer {
            padding: 5px 10px;
            height: auto;
            width: 100%;
            float: left;
        }

        .inv_container .invoice .footer {
            border-bottom: 1px solid #444;
        }

        .inv_container .invoice .customer h2,
        .inv_container .invoice .items h2 {
            font-size: 14px;
            line-height: 20px;
            padding: 3px 0px;
            width: 100%;
            font-weight: 400;
            float: left;
            text-align: center;
        }

        .inv_container .invoice .customer h2 span,
        .inv_container .invoice .items h2 span {
            width: auto;
            padding: 0 10px;
            background-color: #f8f8f8;
            z-index: 1;
        }


        .inv_container .invoice .customer h2::after,
        .inv_container .invoice .items h2::after {
            content: "";
            width: 100%;
            height: 0;
            border-bottom: 1px dashed #666;
            position: absolute;
            left: 0;
            top: 13px;
        }


        .inv_container .invoice .customer ul,
        .inv_container .invoice .items ul,
        .inv_container .invoice .footer ul {
            float: left;
            width: 100%;
            height: auto;
            padding: 0;
            padding-bottom: 10px;
            margin: 0;
        }

        .inv_container .invoice .customer ul li,
        .inv_container .invoice .items ul li,
        .inv_container .invoice .footer ul li {
            float: left;
            width: 48.5%;
            height: auto;
            font-size: 12px;
            line-height: 17px;
            border-bottom: 1px dotted #000;
            margin-bottom: 10px;
        }

        .inv_container .invoice .items ul {
            padding-bottom: 0;
        }

        .inv_container .invoice .customer ul li:nth-child(even) {
            float: right;
        }

        .inv_container .invoice .items ul li:first-child {
            width: 39%;
        }

        .inv_container .invoice .items ul li:not(.inv_container .invoice .items ul li:first-child) {
            width: 22%;
            margin-left: 1%;
            text-align: right;
        }

        .inv_container .invoice .items ul li:nth-child(2) {
            width: 14% !important;
            text-align: center !important;
        }

        .inv_container .invoice .items ul:last-child,
        .inv_container .invoice .footer ul:first-child {
            width: 60%;
            float: right;
            margin-top: 10px;
        }

        .inv_container .invoice .items ul:last-child li,
        .inv_container .invoice .footer ul:first-child li {
            width: 100% !important;
            float: right;
            padding: 0 0 0 10px;
        }

        .inv_container .invoice .items ul:last-child li span:first-child,
        .inv_container .invoice .footer ul:first-child li span:first-child {
            float: left;
        }

        .inv_container .invoice .items ul:last-child li span:last-child,
        .inv_container .invoice .footer ul:first-child li span:last-child {
            float: right;
        }

        .inv_container .invoice .footer ul li:last-child {
            border: 0;
            margin: 0;
            margin-bottom: -8px;
        }

        .inv_container .invoice textarea,
        .inv_container .invoice input {
            width: 100%;
            outline: none;
            padding: 4px 6px;
            float: left;
        }

        .inv_container .invoice input {
            margin-top: 2px;
        }

        .inv_container .invoice label {
            width: 100%;
            height: auto;
            font-size: 12px;
            line-height: 17px;
            padding: 10px 10px 0 0;
            text-align: left;
            float: left;
            margin: 5px 0;
        }

        .inv_container .invoice label:nth-child(6) {
            text-align: right;
            margin: 0;
        }
    </style>
@endsection
@section('wrapper')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Settings</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Sales Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;POS Setup
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-lg-5 mx-auto">
                    <div class="card">
                        <div class="card-body col-xl-12 float-left">
                            <div class="col-xl-12 float-left">
                                <div class="inv_container">
                                    <form action="{{ route('setting-pos-setup.update', 1) }}" id="pos_setup">
                                        {{ method_field('PUT') }}
                                        @csrf
                                        <div class="invoice">
                                            <div class="header">
                                                <h2>Mushak - 989009<br />11-Jun-2022</h2>
                                                <img src="<?= config('app.url') ?>assets/images/logo-img.png"
                                                    class="msg-avatar" alt="user avatar">
                                                <ul>
                                                    <li>Company Name</li>
                                                    <li>Address Line 1</li>
                                                    <li>Address Line 2</li>
                                                </ul>
                                            </div>
                                            <div class="customer">
                                                <h2><span>Retail Invoice</span></h2>
                                                <ul>
                                                    <li>Customer_name</li>
                                                    <li>Cashier_name</li>
                                                    <li>Phone_number</li>
                                                    <li>INV #202208170001</li>
                                                </ul>
                                            </div>
                                            <label>Message for Customer</label>
                                            <textarea name="text1" cols="30" rows="2" placeholder="Message for customer..." required>{{ $pos->text1 }}</textarea>
                                            <div class="items">
                                                <h2><span>Item's Info</span></h2>
                                                <ul>
                                                    <li># Item's Name</li>
                                                    <li>Qty.</li>
                                                    <li>Price/Qty</li>
                                                    <li>Amount</li>
                                                </ul>
                                                <ul>
                                                    <li>----</li>
                                                    <li>----</li>
                                                    <li>----</li>
                                                    <li>----</li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <span>Total</span>
                                                        <span>----</span>
                                                    </li>
                                                    <li>
                                                        <span>Vat</span>
                                                        <span>----</span>
                                                    </li>
                                                    <li>
                                                        <span>Discount</span>
                                                        <span>----</span>
                                                    </li>
                                                    <li>
                                                        <span>Grand Total</span>
                                                        <span>----</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <label>-------</label>
                                            <div class="footer">
                                                <ul>
                                                    <li>
                                                        <span>Paid Amount</span>
                                                        <span>----</span>
                                                    </li>
                                                    <li>
                                                        <span>Change Amount</span>
                                                        <span>----</span>
                                                    </li>
                                                    <li>
                                                        <span>Due Amount</span>
                                                        <span>----</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <label>Thank you message </label>
                                            <textarea name="text2" cols="30" rows="3" placeholder="Thank you message for customer..." required>{{ $pos->text2 }}</textarea>
                                            <label>Email Address </label>
                                            <input name="email" type="email" placeholder="Email Address"
                                                value="{{ $pos->email }}" required />
                                            <label>Web Address </label>
                                            <input name="web" type="text" placeholder="Web Address"
                                                value="{{ $pos->web == null ? 'N/A' : $pos->web }}" />
                                            <label>Phone No. <br />(Use comma (,) for multiple contact no.)</label>
                                            <input name="phone" type="text" value="{{ $pos->phone }}"
                                                placeholder="Use comma (,) for multiple contact no." required />
                                            <label>
                                                Powered By -
                                                <br />
                                                Developed By - Techgeen
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <label id="message" class="col-sm-6 col-form-label form-message"></label>
                            <div class="col-xl-12">
                                <div class="d-flex gap-2 mt-3 float-end">
                                    <button class="btn btn-warning" type="submit" form="pos_setup">
                                        <i class="bx bx-pencil"></i>Update
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
@endsection

@section('script')
    <script src="assets/plugins/input-tags/js/tagsinput.js"></script>
@endsection
