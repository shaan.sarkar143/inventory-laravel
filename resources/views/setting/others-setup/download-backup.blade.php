@extends("layouts.app")
  @section("wrapper")
  <!--start page wrapper -->
  <div class="page-wrapper">
    <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Settings</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Other Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Downloads</li>
            </ol>
          </nav>
        </div>
      </div>
      <!--end breadcrumb-->
      <div class="row">
          <div class="col-xl-9 mx-auto">
              <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                <div class="col">
                    <div class="card radius-10 bg-info">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-dark">Database</p>
                                    <h4 class="my-1 text-dark">3MB</h4>
                                </div>
                                <div class="widgets-icons bg-white text-dark ms-auto">
                                    <i class="bx bx-download"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 bg-info">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-dark">Products</p>
                                    <h4 class="my-1 text-dark">3000</h4>
                                </div>
                                <div class="widgets-icons bg-white text-dark ms-auto">
                                    <i class="bx bx-download"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 bg-info">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-dark">Purchase</p>
                                    <h4 class="my-1 text-dark">3000</h4>
                                </div>
                                <div class="widgets-icons bg-white text-dark ms-auto">
                                    <i class="bx bx-download"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card radius-10 bg-info">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-dark">Sales</p>
                                    <h4 class="my-1 text-dark">3000</h4>
                                </div>
                                <div class="widgets-icons bg-white text-dark ms-auto">
                                    <i class="bx bx-download"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page wrapper -->
  @endsection
	@section("script")
  @parent
	<script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
	<script src="assets/js/widgets.js"></script>
	@endsection
