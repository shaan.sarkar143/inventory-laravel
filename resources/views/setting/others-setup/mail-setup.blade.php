<?php
$used_for = ['All', 'Employee', 'Customer', 'Supplier'];
?>
@extends('layouts.app')
@section('style')
    <link href="<?= config('app.url') ?>assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
    <link href="<?= config('app.url') ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection
@section('wrapper')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Settings</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Other&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Mail Setup
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- Modal -->
                    <div class="modal fade" id="mailModal" aria-hidden="true" style="overflow-x:hidden;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Mail information Update</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body" id="edit_mail">
                                </div>
                                <div class="modal-footer" style="display:inline-block">
                                    <button form="mail_edit_form" type="submit"
                                        class="btn btn-primary float-right">Update</button>
                                    <button form="mail_edit_form" type="reset" class="btn btn-danger float-right"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-10 mx-auto">
                    <div class="card p-3 pt-1">
                        <div class="card-body col-xl-12 float-left">
                            <form class="col-lg-12" action="{{ route('setting-mail-setup.store') }}" method="post"
                                id="employeeDept">
                                <input type="hidden" name="_token" value="<?= csrf_token() ?>" />
                                <div class="row">
                                    <div class="col-xl-4 float-left">
                                        <label class="col-sm-12 col-form-label">Used For</label>
                                        <select name="used_for" class="form-control mb-3 select_search"
                                            aria-label="Used For" required>
                                            <option value="0" selected>All</option>
                                            <option value="1">Employee</option>
                                            <option value="2">Customer</option>
                                            <option value="3">Supplier</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-4 float-left">
                                        <label class="col-sm-12 col-form-label">Mail From</label>
                                        <input name="mail_from" class="form-control mb-3" type="text"
                                            placeholder="Mail From - Send mail from" aria-label="Mail From" required>
                                    </div>
                                    <div class="col-xl-4 float-left">
                                        <label class="col-sm-12 col-form-label">Reply To</label>
                                        <input name="reply_to" class="form-control mb-3" type="text"
                                            placeholder="Reply To - Reply mail address" aria-label="Reply To" required>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <label id="message" class="col-sm-6 col-form-label form-message"></label>
                                    <div class="col-xl-6 float-left">
                                        <button type="submit" class="btn btn-primary float-right"><i
                                                class="bx bx-list-plus"></i>Add</button>
                                        <button type="reset" class="btn btn-danger float-right"
                                            style="margin-right:20px"><i class="lni lni-eraser"></i>Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <hr class="col-xl-12 float-left" />
                        <div class="card-body col-xl-12 float-left">
                            <div class="table-responsive col-xl-12 float-left" style="overflow:hidden;">
                                <table id="example" class="table table-striped table-bordered"
                                    style="width:100%;overflow:hidden;">
                                    <thead>
                                        <tr>
                                            <th style="width:8%">Sl.</th>
                                            <th>Used For</th>
                                            <th>Mail From</th>
                                            <th>Reply To</th>
                                            <th style="width:120px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $x = 1; foreach( $mails as $row => $v ){?>
                                        <tr>
                                            <td><?= $x ?></td>
                                            <td><?= $used_for[$v->used_for] ?></td>
                                            <td><?= $v->mail_from ?></td>
                                            <td><?= $v->reply_to ?></td>
                                            <td>
                                                <div class="row row-cols-auto g-3 mx-auto flex-center">
                                                    <button data-id="<?= $v->id ?>"
                                                        class="btn-action btn-warning mail_edit"><i
                                                            class="lni lni-pencil-alt"
                                                            style="margin-right:6px"></i>Edit</button>
                                                    <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i
                                                            class="lni lni-trash"
                                                            style="margin-right:6px"></i>Erase</button>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $x++; }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
@endsection

@section('script')
    <script src="<?= config('app.url') ?>assets/plugins/input-tags/js/tagsinput.js"></script>
    <script src="<?= config('app.url') ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
            $('.select_search').select2({
                height: 'resolved'
            });

            $('body').on('click', '.mail_edit', function() {
                let url = window.location.pathname;
                url = url + '/' + $(this).data('id') + '/edit';
                $('#edit_mail').load(url, function() {
                    $('.select_search').select2({
                        dropdownParent: $("#mailModal"),
                        width: '100%'
                    });
                    $('#mailModal').modal('show');
                });
            });
        });
    </script>
@endsection
