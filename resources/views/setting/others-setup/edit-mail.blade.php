<div class="row">
    <form class="col-lg-12" style="padding: 0 20px;" action="{{ route('setting-mail-setup.update', $mail->id) }}" id="mail_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-6 float-left">
                <label class="col-sm-12 col-form-label">Used For</label>
                <select name="used_for" class="form-control mb-3 select_search" aria-label="Used For" required>
                    <option value="0" <?= ($mail->used_for == 0) ? 'selected': ''?>>All</option>
                    <option value="1" <?= ($mail->used_for == 1) ? 'selected': ''?>>Employee</option>
                    <option value="2" <?= ($mail->used_for == 2) ? 'selected': ''?>>Customer</option>
                    <option value="3" <?= ($mail->used_for == 3) ? 'selected': ''?>>Supplier</option>
                </select>
            </div>
            <div class="col-xl-6 float-left">
                <label class="col-sm-12 col-form-label">Mail From</label>
                <input name="mail_from" class="form-control mb-3" type="text" placeholder="Mail From - Send mail from" value="<?= $mail->mail_from ?>" aria-label="Mail From" required>
            </div>
            <div class="col-xl-6 float-left">
                <label class="col-sm-12 col-form-label">Reply To</label>
                <input name="reply_to" class="form-control mb-3" type="text" placeholder="Reply To - Reply mail address" value="<?= $mail->mail_from ?>" aria-label="Reply To" required>
            </div>
        </div>
    </form>
</div>