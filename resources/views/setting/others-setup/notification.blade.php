@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.min.css">
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Settings</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Other Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Notification</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-lg-5 mx-auto">
                          <div class="card p-3 pt-1">
                              <form action="{{ url('setting-other-notification') }}" method="get" name="">
                                  <div class="card-body col-xl-12 float-left">
                                      <div class="col-xl-12 px-3 float-left" style="margin-top:10px;">
                                          <div class="form-check form-switch">
                                              <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                              <label class="form-check-label" for="flexSwitchCheckChecked">Checked switch checkbox input</label>
                                          </div>
                                          <div class="form-check-danger form-check form-switch">
                                              <input class="form-check-input" type="checkbox" id="flexSwitchCheckCheckedDanger">
                                              <label class="form-check-label" for="flexSwitchCheckCheckedDanger">Checked switch checkbox input</label>
                                          </div>
                                          <div class="form-check form-switch">
                                              <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                              <label class="form-check-label" for="flexSwitchCheckChecked">Checked switch checkbox input</label>
                                          </div>
                                          <div class="form-check-danger form-check form-switch">
                                              <input class="form-check-input" type="checkbox" id="flexSwitchCheckCheckedDanger" checked>
                                              <label class="form-check-label" for="flexSwitchCheckCheckedDanger">Checked switch checkbox input</label>
                                          </div>
                                      </div>
                                      <div class="col-xl-12 px-3 float-left">
                                          <div class="d-flex gap-2 mt-3 float-end">
                                              <button type="submit" class="btn btn-warning"><i class="bx bx-list-plus"></i>Update</button>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="trumbowyg/dist/trumbowyg.min.js"></script>
<script>
    $('#editor').trumbowyg();
</script>
@endsection
