<?php 
    $day = array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
?>
@extends("layouts.app")
  @section("style")
  <link href="<?= config('app.url'); ?>assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link href="<?= config('app.url'); ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Settings</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Employee Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;HRM&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Time table</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <!-- Modal -->
                        <div class="modal fade" id="hrmModal" aria-hidden="true" role="dialog" style="overflow:hidden;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">HRM: Time Table Update</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body" id="edit_hrm">
                                    </div>
                                    <div class="modal-footer" style="display:inline-block">
                                        <button form="hrm_edit_form" type="submit" class="btn btn-primary float-right">Update</button>
                                        <button form="hrm_edit_form" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-xl-10 mx-auto">
                          <div class="card p-3 pt-1">
                              <div class="card-body col-xl-12 float-left">
                                  <form class="col-lg-12" action="{{ route('setting-employee-hrm.store') }}" method="post" id="employeeDept">
                                      <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                      <div class="row">
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">Day Name</label>
                                              <select name="day_no" class="form-control mb-3 select_search" aria-label="Day Name" required>
                                                  <option value="" selected disabled>Choose Day</option>
                                              <?php $x = 1; foreach($day as $v){?>
                                                  <option value="<?= $x ?>"><?= $v ?></option>
                                              <?php $x++; }?>
                                              </select>
                                          </div>
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">Start Time</label>
                                              <input name="start_time" class="form-control mb-3" type="time" placeholder="Start Time" aria-label="Start Time" required>
                                          </div>
                                          <div class="col-xl-4 float-left">
                                              <label class="col-sm-12 col-form-label">End Time</label>
                                              <input name="end_time" class="form-control mb-3" type="time" placeholder="End Time" aria-label="End Time" required>
                                          </div>
                                      </div>
                                      <div class="row mt-3">
                                          <label id="message" class="col-sm-6 col-form-label form-message"></label>
                                          <div class="col-xl-6 float-left">
                                              <button type="submit" class="btn btn-primary float-right"><i class="bx bx-list-plus"></i>Add</button>
                                              <button type="reset" class="btn btn-danger float-right" style="margin-right:20px"><i class="lni lni-eraser"></i>Clear</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                              <hr class="col-xl-12 float-left"/>
                              <div class="card-body col-xl-12 float-left">
                                <div class="table-responsive col-xl-12 float-left" style="overflow:hidden;">
                                  <table id="example" class="table table-striped table-bordered" style="width:100%;overflow:hidden;">
                                    <thead>
                                      <tr>
                                        <th style="width:8%">Sl.</th>
                                        <th>Day Name</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th style="width:120px">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 1; foreach( $time_table as $row => $v ){?>
                                      <tr>
                                        <td><?= $x ?></td>
                                        <td><?= $day[$v->day_no - 1] ?></td>
                                        <td><?= (!empty($v->start_time) && $v->start_time !== NULL)?date("h:i A", strtotime($v->start_time)):'--:-- --' ?></td>
                                        <td><?= (!empty($v->end_time) && $v->end_time !== NULL)?date("h:i A", strtotime($v->end_time)):'--:-- --' ?></td>
                                        <td>
                                          <div class="row row-cols-auto g-3 mx-auto flex-center">
                                              <button data-id="<?= $v->id ?>" class="btn-action btn-warning hrm_edit"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                                              <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                          </div>
                                        </td>
                                      </tr>
                                    <?php $x++; }?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();

        $('.select_search').select2({
            height:'resolved'
        });
        
        $('body').on('click','.hrm_edit', function(){
            let url = window.location.pathname;
            url = url + '/'+ $(this).data('id') + '/edit';
            $('#edit_hrm').load(url,function(){
                $('.select_search').select2({
                    dropdownParent: $("#hrmModal"),
                    width:'100%'
                });
                $('#hrmModal').modal('show');
            });
        }); 
    });
</script>
@endsection
