<div class="row">
    <form class="col-lg-12" style="padding:0 20px;" action="{{ route('setting-employee-type.update', $type->id) }}" id="type_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-4 float-left">
                <label for="inputPassword" class="col-sm-12 col-form-label">Type Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Type Name" value="<?= $type->name ?>" aria-label="Type Name" required>
            </div>
            <div class="col-xl-8 float-right">
                <label for="inputPassword" class="col-sm-12 col-form-label">Remarks</label>
                <input name="details" class="form-control mb-3" type="text" placeholder="Remarks" value="<?= $type->details ?>" aria-label="Remarks">
            </div>
        </div>
    </form>
</div>