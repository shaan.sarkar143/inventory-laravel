<?php 
    $day = array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
?>
<div class="row">
    <form class="col-lg-12" style="padding:0 20px;" action="{{ route('setting-employee-hrm.update', $hrm->id) }}" id="hrm_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-6 float-left">
                <label class="col-sm-12 col-form-label">Day Name</label>
                <select name="day_no" class="form-control mb-3 select_search" aria-label="Day Name" required>
                    <option value="" selected disabled>Choose Day</option>
                <?php $x = 1; foreach($day as $v){?>
                    <option value="<?= $x ?>" <?= $hrm->day_no == $x ? 'selected': ''; ?> ><?= $v ?></option>
                <?php $x++; }?>
                </select>
            </div>
            <div class="col-xl-6 float-left">
                <label class="col-sm-12 col-form-label">Start Time</label>
                <input name="start_time" class="form-control mb-3" type="time" value="<?= $hrm->start_time ?>" placeholder="Start Time" aria-label="Start Time" required>
            </div>
            <div class="col-xl-6 float-left">
                <label class="col-sm-12 col-form-label">End Time</label>
                <input name="end_time" class="form-control mb-3" type="time" value="<?= $hrm->end_time ?>" placeholder="End Time" aria-label="End Time" required>
            </div>
        </div>
    </form>
</div>