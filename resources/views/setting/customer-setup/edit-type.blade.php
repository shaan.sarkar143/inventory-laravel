<div class="row">
    <form class="col-lg-12" style="padding:0 20px" action="{{ route('setting-customer-type.update', $type->id) }}" id="type_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-md-6 float-left">
                <label for="Group Name" class="col-sm-12 col-form-label">Group Name</label>
                <select name="group_id" class="form-control mb-3 group_select" aria-label="Group Name">
                    <option value="" selected disabled>Choose Group</option>
                <?php foreach($groups as $row => $v){?>
                    <option value="<?= $v->id ?>" <?= ($type->group_id == $v->id) ? 'selected': ''; ?>><?= $v->name ?></option>
                <?php }?>
                </select>
            </div>
            <div class="col-md-6 float-left">
                <label for="Type Name" class="col-sm-12 col-form-label">Type Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Type Name" value="<?= $v->name ?>" aria-label="Type Name">
            </div>
            <div class="col-xl-12 float-right">
                <label for="Remarks" class="col-sm-12 col-form-label">Remarks</label>
                <input name="details" class="form-control mb-3" type="text" placeholder="Remarks" value="<?= $v->details ?>" aria-label="Remarks">
            </div>
        </div>
    </form>
</div>

