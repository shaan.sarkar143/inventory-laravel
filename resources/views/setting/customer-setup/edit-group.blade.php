<div class="row">
    <form class="col-lg-12" style="padding: 0 20px" action="{{ route('setting-customer-group.update', $group->id) }}" id="group_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-4 float-left">
                <label for="Group Name" class="col-sm-12 col-form-label">Group Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Group Name" value="<?= $group->name ?>" aria-label="Group Name" required>
            </div>
            <div class="col-xl-8 float-right">
                <label for="Remarks" class="col-sm-12 col-form-label">Remarks</label>
                <input name="details" class="form-control mb-3" type="text" placeholder="Remarks" value="<?= $group->details ?>" aria-label="Remarks">
            </div>
        </div>
    </form>
</div>