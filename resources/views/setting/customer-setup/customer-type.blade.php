<?php 
    use \App\Http\Controllers\Setting\CustomerGroup;
    $groups = CustomerGroup::create();
    function group_name($id){
        $group = CustomerGroup::show($id);
        return (!empty($group)) ? $group->name : 'N/A';
    }
?>
@extends("layouts.app")
  @section("style")
  <link href="<?= config('app.url'); ?>assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link href="<?= config('app.url'); ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Settings</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Customer Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Customer Type</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <!-- Modal -->
                        <div class="modal fade" id="typeModal" aria-hidden="true" style="overflow-x:hidden;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Customer Type Information Update</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body" id="edit_type">
                                    </div>
                                    <div class="modal-footer" style="display:inline-block">
                                        <button form="type_edit_form" type="submit" class="btn btn-primary float-right">Update</button>
                                        <button form="type_edit_form" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-xl-10 mx-auto">
                          <div class="card p-3 pt-1">
                              <div class="card-body col-xl-12 float-left">
                                  <div class="row">
                                      <form class="col-lg-12" action="{{ route('setting-customer-type.store') }}" method="post" id="customerType">
                                          <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                          <div class="row">
                                              <div class="col float-left">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label">Group Name</label>
                                                  <select name="group_id" class="form-control mb-3 group_select" aria-label="Group Name">
                                                      <option value="" selected disabled>Choose Group</option>
                                                  <?php foreach($groups as $row => $v){?>
                                                      <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                                  <?php }?>
                                                  </select>
                                              </div>
                                              <div class="col float-left">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label">Type Name</label>
                                                  <input name="name" class="form-control mb-3" type="text" placeholder="Type Name" aria-label="Type Name">
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xl-7 float-right">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label">Remarks</label>
                                                  <input name="details" class="form-control mb-3" type="text" placeholder="Remarks" aria-label="Remarks">
                                              </div>
                                              <div class="col-xl-5 float-right">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label"></label>
                                                  <div class="d-flex gap-2 mt-3 float-end">
                                                      <button type="submit" class="btn btn-primary"><i class="bx bx-list-plus"></i>Add</button>
                                                  </div>
                                                  <div class="d-flex gap-2 mt-3 float-end" style="margin-right:10px">
                                                      <button type="reset" class="btn btn-danger"><i class="lni lni-eraser"></i>Clear</button>
                                                  </div>
                                              </div>
                                          </div>
                                          <label id="message" class="col-sm-6 col-form-label mt-3 form-message"></label>
                                      </form>
                                  </div>
                              </div>
                              <hr class="col-xl-12 float-left"/>
                              <div class="card-body col-xl-12 float-left">
                                <div class="table-responsive col-xl-12 float-left" style="overflow:hidden;">
                                  <table id="example" class="table table-striped table-bordered" style="width:100%;overflow:hidden;">
                                    <thead>
                                      <tr>
                                        <th style="width:8%">Sl.</th>
                                        <th>Type Name</th>
                                        <th>Group Name</th>
                                        <th>Remarks</th>
                                        <th style="width:120px">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 1; foreach($cus_types as $row => $v){?>
                                      <tr>
                                        <td><?= $x ?></td>
                                        <td><?= ucfirst($v->name) ?></td>
                                        <td><?= ucfirst(group_name($v->group_id)) ?></td>
                                        <td><?= ucfirst($v->details) ?></td>
                                        <td>
                                          <div class="row row-cols-auto g-3 mx-auto flex-center">
                                              <button data-id="<?= $v->id ?>" class="btn-action btn-warning type_edit"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                                              <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                          </div>
                                        </td>
                                      </tr>
                                    <?php $x++;}?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();

        $('.group_select').select2({
            height:'resolved'
        });

        $('body').on('click','.type_edit', function(){
            let url = window.location.pathname;
            url = url + '/'+ $(this).data('id') + '/edit';
            $('#edit_type').load(url,function(){
                $('.group_select').select2({
                    dropdownParent: $("#typeModal"),
                    width:'100%'
                });
                $('#typeModal').modal('show');
            });
        }); 
    });
</script>
@endsection
