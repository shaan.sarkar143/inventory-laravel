<div class="col-xl-12 mx-auto" id="setup_form">
    <div id="loader" class="form-loader"></div>
    <form class="col-lg-12 p-3" data-formhide="setup_form" action="{{ route('company.update', $info->id) }}" id="companySetup" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-4 float-left">
                <label class="col-sm-12 col-form-label">Company Name</label>
                <input class="form-control mb-3" name="name" type="text" placeholder="Company Name" value="<?= $info->name ?>" aria-label="Company Name">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Email Address</label>
                <input class="form-control mb-3" name="email" type="email" placeholder="Email Address" value="<?= $info->email ?>" aria-label="Email Address">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Phone No.</label>
                <input class="form-control mb-3" name="phone" type="text" placeholder="Phone No." value="<?= $info->phone ?>" aria-label="Phone No.">
            </div>
            <div class="col-xl-4 float-left">
                <label class="col-sm-12 col-form-label">City</label>
                <input class="form-control mb-3" name="city" type="text" placeholder="City Name" value="<?= $info->city ?>" aria-label="City Name">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">State</label>
                <input class="form-control mb-3" name="state" type="text" placeholder="State name" value="<?= $info->state ?>" aria-label="State Name">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Zip/Post Code</label>
                <input class="form-control mb-3" name="zip" type="number" placeholder="Zip/Post Code" value="<?= $info->zip ?>" aria-label="Zip/Post Code">
            </div>
            <div class="col-xl-8 float-left">
                <label class="col-sm-12 col-form-label">Local Address</label>
                <input class="form-control mb-3" name="address" type="text" placeholder="Local Address" value="<?= $info->address ?>" aria-label="Local Address">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Est. Year</label>
                <input class="form-control mb-3" name="est_year" type="number" placeholder="Est. Year" value="<?= $info->est_year ?>" aria-label="Est. Year">
            </div>
            <div class="col-xl-4 float-left">
                <label class="col-sm-12 col-form-label">Account Started</label>
                <input class="form-control mb-3" name="started" type="date" value="<?= $info->started ?>" aria-label="Account Started">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Initial Invest</label>
                <input class="form-control mb-3" name="invest" type="number" placeholder="0.00" value="<?= number_format($info->invest, 2, '.', '') ?>" aria-label="Initial Invest">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Expense</label>
                <input class="form-control mb-3" name="expense" type="text" placeholder="0.00" value="<?= number_format($info->expense, 2, '.', '') ?>" aria-label="Expense">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Income</label>
                <input class="form-control mb-3" name="income" type="text" placeholder="0.00" value="<?= number_format($info->income, 2, '.', '') ?>" aria-label="Income">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Balance</label>
                <input class="form-control mb-3" name="balance" type="text" placeholder="0.00" value="<?= number_format($info->balance, 2, '.', '') ?>" aria-label="Balance">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Receivable Amount</label>
                <input class="form-control mb-3" name="amount_due" type="number" placeholder="0.00" value="<?= number_format($info->amount_due, 2, '.', '') ?>" aria-label="Receivable Amount">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Payable Amount</label>
                <input class="form-control mb-3" name="amount_pay" type="number" placeholder="0.00" value="<?= number_format($info->amount_pay, 2, '.', '') ?>" aria-label="Payable Amount">
            </div>
            <div class="col-xl-4 float-left">
                <label class="col-sm-12 col-form-label">Logo</label>
                <input class="form-control mb-3" name="logo" type="file" accept="image/jpeg" aria-label="Logo">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Invoice Start From</label>
                <input class="form-control mb-3" name="invoice_start" type="text" placeholder="<?= date('Ym').'00001'?>" value="<?= $info->invoice_start ?>" aria-label="Invoice Start From">
            </div>
            <div class="col-xl-4 float-right">
                <label class="col-sm-12 col-form-label">Product Code Start From</label>
                <input class="form-control mb-3" name="code_start" type="text" placeholder="<?= date('Ym').'00001'?>" value="<?= $info->code_start ?>" aria-label="Product Code Start From">
            </div>
            <div class="col-xl-8 float-left">
                <label id="message" class="col-sm-12 col-form-label mt-3 form-message"></label>
            </div>
        </div>
    </form>
</div>