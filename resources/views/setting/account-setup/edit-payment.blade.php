<div class="row">
    <form class="col-lg-12" action="{{ route('setting-account-payment.update', $payment->id) }}" id="payment_edit_form" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-6 float-left">
                <label for="Method Name" class="col-sm-12 col-form-label">Method Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Method Name" value="<?= $payment->name ?>" aria-label="Method Name" required>
            </div>
            <div class="col-xl-6 float-right">
                <label for="Used For" class="col-sm-12 col-form-label">Used For</label>
                <select name="used_for" class="form-control mb-3" aria-label="Used For" required>
                    <option value="1" <?= $payment->used_for == 1 ? 'selected' : '' ?>>Income</option>
                    <option value="2" <?= $payment->used_for == 2 ? 'selected' : '' ?>>Expense</option>
                    <option value="0" <?= $payment->used_for == 0 ? 'selected' : '' ?>>Both</option>
                </select>
            </div>
            <div class="col-xl-6 float-left">
                <label for="Image" class="col-sm-12 col-form-label">Image</label>
                <input name="image" class="form-control mb-3" type="file" accept="image/jpeg" aria-label="Image">
            </div>
        </div>
    </form>
</div>