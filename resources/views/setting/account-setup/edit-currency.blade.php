<div class="row">
    <form class="col-lg-12" style="padding:0 20px;" action="{{ route('setting-account-currency.update', $currency->id) }}" id="currency_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-6 float-left">
                <label for="Currency Name" class="col-sm-12 col-form-label">Currency Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Currency Name" value="<?= $currency->name ?>" aria-label="Currency Name" required>
            </div>
            <div class="col-xl-6 float-right">
                <label for="BDT Amount" class="col-sm-12 col-form-label">BDT Amount</label>
                <input name="amount" class="form-control mb-3" type="number" placeholder="0.00" step='0.01' value="<?= $currency->amount ?>" aria-label="BDT Amount" required>
            </div>
        </div>
    </form>
</div>