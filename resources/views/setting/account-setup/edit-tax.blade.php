<div class="row"> 
    <form class="col-lg-12" style="padding: 0 20px" action="{{ route('setting-account-tax.update', $tax->id) }}" id="tax_edit_form" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-6 float-left">
                <label for="TAX Title" class="col-sm-12 col-form-label">TAX Title</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="TAX Title" value="<?= $tax->name ?>" aria-label="TAX Title" required>
            </div>
            <div class="col-xl-6 float-right">
                <label for="Used For" class="col-sm-12 col-form-label">Used For</label>
                <select name="used_for" class="form-control mb-3" aria-label="Used For" required>
                    <option value="1" <?= $tax->used_for == 1 ? 'selected' : '' ?>>Income</option>
                    <option value="2" <?= $tax->used_for == 2 ? 'selected' : '' ?>>Expense</option>
                    <option value="0" <?= $tax->used_for == 0 ? 'selected' : '' ?>>Both</option>
                </select>
            </div>
            <div class="col-xl-6 float-left">
                <label for="TAX Image" class="col-sm-12 col-form-label">Image</label>
                <input name="image" class="form-control mb-3" type="file" accept="image/jpeg" aria-label="TAX Image">
            </div>
        </div>
    </form>
</div>