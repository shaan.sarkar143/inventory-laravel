<?php 
function used_for($num){
    switch($num){
        case 1: return 'Income'; break;
        case 2: return 'Expense'; break;
        case 0:
        default: return 'Both'; break;
    }
}
?>
@extends("layouts.app")
  @section("style")
  <link href="<?= config('app.url'); ?>assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link href="<?= config('app.url'); ?>assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Settings</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Account Setup&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Payment Method</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <!-- Modal -->
                        <div class="modal fade" id="paymentModal" aria-hidden="true" style="overflow-x:hidden;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Brand Update</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body" id="edit_payment">
                                    </div>
                                    <div class="modal-footer" style="display:inline-block">
                                        <button form="payment_edit_form" type="submit" class="btn btn-primary float-right">Update</button>
                                        <button form="payment_edit_form" type="reset" class="btn btn-danger float-right" data-bs-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-xl-10 mx-auto">
                          <div class="card p-3 pt-1">
                              <div class="card-body col-xl-12 float-left">
                                  <div class="row">
                                      <form class="col-lg-12" action="{{ route('setting-account-payment.store') }}" method="post" id="accountPayment" enctype="multipart/form-data">
                                          <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                          <div class="row">
                                              <div class="col float-left">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label">Method Name</label>
                                                  <input name="name" class="form-control mb-3" type="text" placeholder="Method Name" aria-label="Method Name">
                                              </div>
                                              <div class="col float-right">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label">Used For</label>
                                                  <select name="used_for" class="form-control mb-3" aria-label="Group Name">
                                                      <option value="1">Income</option>
                                                      <option value="2">Expense</option>
                                                      <option value="0" selected>Both</option>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col float-left">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label">Image</label>
                                                  <input name="image" class="form-control mb-3" type="file" accept="image/jpeg" aria-label="Category Image">
                                              </div>
                                              <div class="col float-right">
                                                  <label for="inputPassword" class="col-sm-12 col-form-label"></label>
                                                  <div class="d-flex gap-2 mt-3 float-end">
                                                      <button type="submit" class="btn btn-primary"><i class="bx bx-list-plus"></i>Add</button>
                                                  </div>
                                                  <div class="d-flex gap-2 mt-3 float-end" style="margin-right:10px">
                                                      <button type="reset" class="btn btn-danger"><i class="lni lni-eraser"></i>Clear</button>
                                                  </div>
                                              </div>
                                          </div>
                                          <label id="message" class="col-sm-6 col-form-label mt-3 form-message float-left"></label>
                                      </form>
                                  </div>
                              </div>
                              <hr class="col-xl-12 float-left"/>
                              <div class="card-body col-xl-12 float-left">
                                <div class="table-responsive col-xl-12 float-left" style="overflow:hidden;">
                                  <table id="example" class="table table-striped table-bordered" style="width:100%;overflow:hidden;">
                                    <thead>
                                      <tr>
                                        <th style="width:40px">Sl.</th>
                                        <th style="width:60px">Image</th>
                                        <th>Method Name</th>
                                        <th>Used For</th>
                                        <th style="width:120px">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 1; foreach($payment as $row => $v){?>
                                      <tr>
                                        <td><?= $x ?></td>
                                        <td>
                                            @if($v->image !== null)
                                                <img style="height: 50px;max-width:70px;" src="<?= asset("storage/$v->image") ?>"/>
                                            @else
                                                <img style="height: 50px;max-width:70px;" src="assets/images/logo-icon.png"/>
                                            @endif
                                        </td>
                                        <td><?= $v->name ?></td>
                                        <td><?= used_for($v->used_for) ?></td>
                                        <td>
                                          <div class="row row-cols-auto g-3 mx-auto flex-center">
                                              <button data-id="<?= $v->id ?>" class="btn-action btn-warning payment_edit"><i class="lni lni-pencil-alt" style="margin-right:6px"></i>Edit</button>
                                              <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                          </div>
                                        </td>
                                      </tr>
                                    <?php $x++; }?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="<?= config('app.url'); ?>assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="<?= config('app.url'); ?>assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        
        $('body').on('click','.payment_edit', function(){
            let url = window.location.pathname;
            url = url + '/'+ $(this).data('id') + '/edit';
            $('#edit_payment').load(url,function(){
                $('#paymentModal').modal('show');
            });
        }); 
    });
</script>
@endsection
