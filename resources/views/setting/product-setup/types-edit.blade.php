<div class="row">
    <form class="col-lg-12" style="padding: 0 20px" action="{{ route('setting-product-type.update', $type->id) }}" id="type_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-5 float-right">
                <label class="col-sm-12 col-form-label">Type's Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Type's Name" value="<?= $type->name ?>" aria-label="Type's Name" required>
            </div>
            <div class="col-xl-7 float-left">
                <label class="col-sm-12 col-form-label">Remarks</label>
                <input name="remarks" class="form-control mb-3" type="text" placeholder="Remarks" value="<?= $type->remarks ?>" aria-label="Remarks">
            </div>
        </div>
    </form>
</div>