<div class="row">
    <form class="col-lg-12" style="padding:0 20px" action="{{ route('setting-product-unit.update', $unit->id) }}" id="unit_edit_form">
        {{ method_field('PUT') }}
        @csrf
        <div class="col-xl-6 float-left">
            <label for="inputPassword" class="col-sm-12 col-form-label">Unit Name</label>
            <input name="name" class="form-control mb-3" type="text" placeholder="Unit Name" value="<?= $unit->name ?>" aria-label="Unit Name">
        </div>
    </form>
</div>