<div class="row">
    <form class="col-lg-12" style="padding:0px 20px" action="{{ route('setting-product-group.update', $group->id) }}" id="gorup_edit_form" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="col-xl-12 float-left">
            <label for="groupName" class="col-sm-12 col-form-label">Group Name</label>
            <input name="group_name" class="form-control mb-3" type="text" placeholder="Group Name" value="<?= $group->name ?>" aria-label="groupName">
            <label id="message" class="col-sm-12 col-form-label mt-3 form-message"></label>
        </div>
    </form>
</div>