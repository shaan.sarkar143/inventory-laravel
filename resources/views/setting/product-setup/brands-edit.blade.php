<div class="row">
    <form class="col-lg-12" style="padding:0px 20px" action="{{ route('setting-product-brand.update', $brands->id) }}" id="brand_edit_form" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-xl-12 float-right">
                <label for="inputPassword" class="col-sm-12 col-form-label">Brand's Name</label>
                <input name="name" class="form-control mb-3" type="text" placeholder="Brand's Name" value="<?= $brands->name ?>" aria-label="Brand's Name" required/>
            </div>
            <div class="col-xl-12 float-left">
                <label for="inputPassword" class="col-sm-12 col-form-label">Image</label>
                <input name="image" class="form-control mb-3" type="file" accept="image/jpeg" aria-label="Brand's Image"/>
            </div>
        </div>
    </form>
</div>