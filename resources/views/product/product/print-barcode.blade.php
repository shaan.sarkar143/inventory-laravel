<?php
use App\Http\Controllers\Setting\GroupSetup;
use App\Http\Controllers\Product\CategorySetup;
use App\Http\Controllers\Setting\BrandSetup;
use App\Http\Controllers\Supplier\SupplierList;
$groups = GroupSetup::create();
$brands = BrandSetup::create();
$suppliers = SupplierList::create();
function group_name($id)
{
    $group = GroupSetup::show($id);
    return $group[0]->name;
}
require 'vendor/autoload.php';
$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
?>
@extends('layouts.app')
@section('style')
    <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection
@section('wrapper')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Product</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Print Barcode</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-10 mx-auto">
                    <div class="card p-3 pt-1">
                        <div class="card-body col-xl-12 float-left">
                            <div class="row">
                                <div class="col-xl-12 float-right">
                                    <input id="com_info" class="form-control mb-3 print_range" type="text"
                                        placeholder="Company Information" aria-label="Company Information" />
                                </div>
                                <div class="col-xl-3 float-left">
                                    <div class="col-xl-12 float-left">
                                        <label class="col-sm-12 col-form-label">Sticker Size</label>
                                        <select id="sticker_size" class="form-control mb-3 select_search"
                                            aria-label="Sticker Size">
                                            <option value="0" selected disabled>Choose Width</option>
                                            <option value="20">20mm</option>
                                            <option value="24">24mm</option>
                                            <option value="36">36mm</option>
                                            <option value="58" selected>58mm</option>
                                            <option value="100">100mm</option>
                                            <option value="115">100mm</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-12 float-left">
                                        <label class="col-sm-12 col-form-label">Print Amount</label>
                                        <input id="print_num" class="form-control mb-3 print_range" type="text"
                                            placeholder="Total Print" aria-label="Total Print" />
                                    </div>
                                    <div class="col-xl-12 float-left">
                                        <label class="col-sm-12 col-form-label">Barcode Format</label>
                                        <select id="barcode_format" class="form-control mb-3 select_search"
                                            aria-label="Sticker Size">
                                            <option value="0" selected disabled>Choose Format</option>
                                            <option value="39">Code 39</option>
                                            <option value="128" selected>Code 128</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-3 px-3 float-left" style="margin-top:10px;">
                                    <div class="form-check form-switch">
                                        <input name="check_option[]" value="1" class="form-check-input"
                                            type="checkbox" />
                                        <label class="form-check-label" for="flexSwitchCheckChecked">Product Name</label>
                                    </div>
                                    <div class="form-check form-switch">
                                        <input name="check_option[]" value="7" class="form-check-input"
                                            type="checkbox" />
                                        <label class="form-check-label" for="flexSwitchCheckChecked">MRP</label>
                                    </div>
                                    <div class="form-check-danger form-check form-switch">
                                        <input name="check_option[]" value="2" class="form-check-input"
                                            type="checkbox" />
                                        <label class="form-check-label" for="flexSwitchCheckCheckedDanger">MFD &
                                            EXP</label>
                                    </div>
                                    <div class="form-check-danger form-check form-switch">
                                        <input name="check_option[]" value="3" class="form-check-input" type="checkbox"
                                            checked />
                                        <label class="form-check-label" for="flexSwitchCheckCheckedDanger">Barcode</label>
                                    </div>
                                    <div class="form-check-danger form-check form-switch">
                                        <input name="check_option[]" value="4" class="form-check-input"
                                            type="checkbox" />
                                        <label class="form-check-label" for="flexSwitchCheckCheckedDanger">BSTI
                                            Information</label>
                                    </div>
                                    <div class="form-check form-switch">
                                        <input name="check_option[]" value="5" class="form-check-input"
                                            type="checkbox" />
                                        <label class="form-check-label" for="flexSwitchCheckChecked">Company Logo</label>
                                    </div>
                                    <div class="col-xl-12 float-left form-check form-switch">
                                        <input name="check_option[]" value="6" class="form-check-input"
                                            type="checkbox" />
                                        <label class="form-check-label" for="flexSwitchCheckCheckedDanger">Company
                                            Information</label>
                                    </div>
                                </div>
                                <div class="col-xl-6 px-3 float-left" style="margin-top:10px;">
                                    <div class="form-check form-switch" style="padding-left: 4px;">
                                        <i class="lni lni-checkmark" style="color:#F00"></i>
                                        <label class="form-check-label text-danger">MFD & EXP
                                            : Manufactural & Expired Date</label>
                                    </div>
                                    <div class="form-check form-switch" style="padding-left: 4px;">
                                        <i class="lni lni-checkmark" style="color:#F00"></i>
                                        <label class="form-check-label text-danger">Barcode :
                                            According to Product</label>
                                    </div>
                                    <div class="form-check form-switch" style="padding-left: 4px;">
                                        <i class="lni lni-checkmark" style="color:#F00"></i>
                                        <label class="form-check-label text-danger">BSTI
                                            Information : BSTI Approved or Not and BDS No. If BSTI Approved.</label>
                                    </div>
                                    <div class="form-check-danger form-check form-switch" style="padding-left: 4px;">
                                        <i class="lni lni-checkmark" style="color:#F00"></i>
                                        <label class="form-check-label text-danger">Company Information : Company Name,
                                            Address,Contact Number.</label>
                                    </div>
                                    <div class="form-check-danger form-check form-switch" style="padding-left: 4px;">
                                        <i class="lni lni-checkmark" style="color:#F00"></i>
                                        <label class="form-check-label text-danger">Code Format : Code 39 & code
                                            128</label>
                                    </div>
                                    <div class="col-xl-12 float-left" style="border-top: 1px solid #DDD">
                                        <button id="clear_setup" class="btn-action btn-danger float-right role_setup_btn"
                                            type="button">Clear
                                            Role</button>
                                        <button id="role_setup" class="btn-action btn-primary float-right role_setup_btn"
                                            type="button">Role
                                            Set</button>
                                    </div>
                                    <div class="col-xl-12 float-left mt-3" id="role_msg">
                                        <label class="col-xl-12 form-check-label text-success p-2"></label>
                                    </div>
                                    <style scoped>
                                        .role_setup_btn {
                                            height: 38px;
                                            width: 120px;
                                            margin-top: 10px;
                                        }

                                        .role_setup_btn:first-child {
                                            margin-left: 20px;
                                        }

                                        #role_msg {
                                            display: none;
                                        }

                                        #role_msg label {
                                            text-align: right;
                                            font-size: 16px;
                                        }
                                    </style>
                                </div>
                            </div>
                            <div class="row mt-3 pt-3" style="border-top: 1px solid #DDD">
                                <div class="col-xl-4 float-left">
                                    <label class="col-sm-12 col-form-label">Select Supplier</label>
                                    <select id="supp_id" name="supplier" class="form-control mb-3 select_search"
                                        aria-label="Select Supplier" disabled>
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($suppliers as $row => $v){?>
                                        <option value="<?= $v->id ?>">
                                            <?= ucfirst(strtolower($v->fname)) . ' ' . ucfirst(strtolower($v->lname)) . ' # ' . $v->phone ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-4 float-left">
                                    <label class="col-sm-12 col-form-label">Product Group</label>
                                    <select name="group_id" id="group_id" class="form-control mb-3 select_search"
                                        aria-label="Product Group" disabled>
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($groups as $row => $v){?>
                                        <option value="<?= $v->id ?>"><?= ucfirst($v->name) ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-4 float-left">
                                    <label class="col-sm-12 col-form-label">Product Category</label>
                                    <select name="cat_id" id="cat_id" class="form-control mb-3 select_search"
                                        aria-label="Product Category" disabled></select>
                                </div>
                                <div class="col-xl-4 float-left">
                                    <label class="col-sm-12 col-form-label">Brand</label>
                                    <select name="brand_id" id="brand_id" class="form-control mb-3 select_search"
                                        aria-label="Brands" disabled>
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($brands as $row => $v){?>
                                        <option value="<?= $v->id ?>"><?= ucfirst($v->name) ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-4 float-left">
                                    <label class="col-sm-12 col-form-label">Select Product</label>
                                    <select id="product" class="form-control mb-3 select_search"
                                        aria-label="Product Category" disabled></select>
                                </div>
                                <div class="col-xl-4 float-left">
                                    <label class="col-sm-12 col-form-label">Color Code</label>
                                    <select id="color_code" class="form-control mb-3 select_search"
                                        aria-label="Color Code" disabled></select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="card-body col-xl-12 float-left" style="margin-bottom:30px;">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr class="bg-dark">
                                                <th colspan="8" style="color:#FFF;text-align:center;">Summary : Total
                                                    Products <span id="total_product">0</span></th>
                                            </tr>
                                            <tr>
                                                <th style="width:30px;text-align:center;">##</th>
                                                <th>Product Name</th>
                                                <th style="width:150px">MRP</th>
                                                <th style="width:130px">MFD</th>
                                                <th style="width:130px">EXP</th>
                                                <th style="width:130px">BSTI</th>
                                                <th style="width:130px">BSTI Code</th>
                                                <th style="width:40px">###</th>
                                            </tr>
                                        </thead>
                                        <tbody id="sales_product"></tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8" style="text-align: right">
                                                    <button class="btn-action btn-primary" id="generate"
                                                        style="height:30px;width:120px;">Generate</button>
                                                    <button class="btn-action btn-danger"
                                                        style="height:30px;width:120px;">Remove All</button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr class="col-xl-12 float-left" />
                        <div class="card-body col-xl-12 float-left">
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
@endsection

@section('script')
    <script src="assets/plugins/input-tags/js/tagsinput.js"></script>
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script>
        let barcode_print_role = {};
        const print_check = $("input[name='check_option[]']");
        let product_list = [];
        $('body').on('click', '#role_setup', () => {
            barcode_print_role = {};
            product_list = [];
            let sticker_size = ($('#sticker_size').val() != '' && $('#sticker_size').val() != undefined) ? $(
                '#sticker_size').val() : 0;
            let print_num = ($('#print_num').val() != '' && $('#print_num').val() != undefined) ? $('#print_num')
                .val() : 0;
            let barcode_format = ($('#barcode_format').val() != '' && $('#barcode_format').val() != undefined) ? $(
                '#barcode_format').val() : 0;
            let check_options = [];
            if (sticker_size != 0 || print_num != 0 || barcode_format != 0) {
                barcode_print_role = {
                    'sticker_size': sticker_size,
                    'barcode_format': barcode_format,
                    'print_num': print_num
                };
            }
            print_check.each(function() {
                if (this.checked === true) {
                    check_options.push(this.value);
                }
            });
            const countChecked = $.map(check_options, function(n, i) {
                return i;
            }).length;
            if (countChecked > 0) {
                barcode_print_role.check_options = check_options;
            }
            const count = $.map(barcode_print_role, function(n, i) {
                return i;
            }).length;
            console.log(barcode_print_role, product_list);
            if (count > 0) {
                $('#supp_id').prop('disabled', false);
                $('#role_msg').css({
                    display: 'block'
                });
                $('#role_msg').find('label').html('Role setup done');
            } else {
                $('#supp_id').prop('disabled', true);
                $('#role_msg').css({
                    display: 'none'
                });
                $('#role_msg').find('label').html('');
            }
        });
        $('body').on('click', '#clear_setup', () => {
            $('#sticker_size').val('0').trigger('change');
            $('#barcode_format').val('0').trigger('change');
            $('#print_num').val('');
            print_check.each(function() {
                this.checked = false;
            });
            barcode_print_role = {};
            $('#role_msg').css({
                display: 'none'
            });
            $('#role_msg').find('label').html('');
        });
        $('.select_search').select2({
            height: 'resolved',
            width: '100%'
        });
        $('body').on('change', '#supp_id', function() {
            $('#group_id').prop('disabled', false);
            $('#brand_id').prop('disabled', false);
        });
        $('body').on('change', '#group_id', function() {
            let data = {
                'group_id': $(this).val()
            };
            let result = {};
            let categories = '<option value="" selected disabled>Choose--</option>';
            let products = '';
            $.ajax({
                type: "GET",
                url: "{{ url('product-category/groupWiseCategory') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.categories != undefined) {
                        if (result.categories.length > 0) {
                            $('#cat_id').prop('disabled', false);
                            $.each(result.categories, function(i, val) {
                                categories += '<option value="' + val.id + '">' + val.name +
                                    '</option>';
                            });
                        } else {
                            $('#cat_id').prop('disabled', true);
                            categories += '';
                        }
                    } else {
                        $('#cat_id').prop('disabled', true);
                        categories += '';
                    }
                    if (result.products != undefined) {
                        if (result.products.length > 0) {
                            $('#product').prop('disabled', false);
                            products +=
                                '<option value="" selected disabled>-- Choose --</option>';
                            $.each(result.products, function(i, val) {
                                products += '<option value="' + val.id + '">' + val.name +
                                    '</option>';
                            });
                        } else {
                            $('#product').prop('disabled', true);
                            products += '<option selected>No Product Found..</option>';
                        }
                    } else {
                        $('#product').prop('disabled', true);
                        products = '<option selected>No Product Found..</option>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#product').html(products);
                    $('#cat_id').html(categories);
                }
            });
        });
        $('body').on('change', '#cat_id', function() {
            let data = {
                'cat_id': $(this).val()
            };
            let result = {};
            let products = '';
            $.ajax({
                type: "GET",
                url: "{{ url('product-category/catGroupWiseCategory') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.products != undefined) {
                        if (result.products.length > 0) {
                            $('#product').prop('disabled', false);
                            products +=
                                '<option value="" selected disabled>-- Choose --</option>';
                            $.each(result.products, function(i, val) {
                                products += '<option value="' + val.id + '">' + val.name +
                                    '</option>';
                            });
                        } else {
                            $('#product').prop('disabled', true);
                            products += '<option selected>No Product Found..</option>';
                        }
                    } else {
                        $('#product').prop('disabled', true);
                        products += '<option selected>No Product Found..</option>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#product').html(products);
                }
            });
        });
        $('body').on('change', '#brand_id', function() {
            let data = {
                'brand_id': $(this).val()
            };
            let result = {};
            let products = '';
            $.ajax({
                type: "GET",
                url: "{{ url('setting-product-brand/brandWiseProduct') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result.products != undefined) {
                        if (result.products.length > 0) {
                            $('#product').prop('disabled', false);
                            products +=
                                '<option value="" selected disabled>-- Choose --</option>';
                            $.each(result.products, function(i, val) {
                                products += '<option value="' + val.id + '">' + val.name +
                                    '</option>';
                            });
                        } else {
                            $('#product').prop('disabled', true);
                            products += '<option selected>No Product Found..</option>';
                        }
                    } else {
                        $('#product').prop('disabled', true);
                        products += '<option selected>No Product Found..</option>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#product').html(products);
                }
            });
        });
        $('body').on('change', '#product', function() {
            let value = $(this).val();
            let color_code = {};
            $.ajax({
                type: "GET",
                url: "{{ url('product-list/color') }}" + '/' + value,
                dataType: 'json',
                success: function(response) {
                    result = response;
                    console.log(result);
                    if (result != undefined) {
                        if (result.length > 0) {
                            $('#color_code').prop('disabled', false);
                            color_code +=
                                '<option value="" selected disabled>-- Choose --</option>';
                            $.each(result, function(i, val) {
                                color_code += '<option value="' + val.id + '">' + val.color +
                                    '</option>';
                            });
                        } else {
                            $('#color_code').prop('disabled', true);
                            color_code += '<option selected>No Color Found..</option>';
                        }
                    } else {
                        $('#color_code').prop('disabled', true);
                        color_code += '<option selected>No Color Found..</option>';
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#color_code').html(color_code);
                }
            });
        });
        $('body').on('change', '#color_code', function() {
            let value = $(this).val();
            let data = {};
            let column = '';
            let temp = 0;
            let product_list_table = [];
            data = {
                'sup_id': $('#supp_id').val(),
                'color_id': $(this).val(),
                'pro_id': $('#product').val()
            };
            $.ajax({
                type: "GET",
                url: "{{ url('product-list/info') }}" + '/' + $('#supp_id').val() + '/' + $(this).val() +
                    '/' + $('#product').val(),
                dataType: 'json',
                cache: false,
                success: function(response) {
                    result = response;
                    console.log(result);
                    const pro_id = result.pro_id;
                    let len = product_list.length;
                    if (result.pro_id != undefined) {
                        len += 1;
                        column += '<tr><td rowspan="2">' + len + '</td>';
                        column += '<td>' + result.name + '</td>';
                        column += '<td>' + result.mrp + '</td>';
                        column += '<td>' + result.mfd + '</td>';
                        column += '<td>' + result.exp + '</td>';
                        column += '<td>' + result.bsti + '</td>';
                        column += '<td>' + result.bsti_code + '</td>';
                        column +=
                            '<td rowspan="2"><button data-id="' + pro_id +
                            '" class="btn-action btn-danger"><i class="lni lni-close"></i></button></td></tr>';
                        column += '<tr><td colspan="6"><img id="bar_code"/></td></tr>';
                        if (product_list.length > 0) {
                            var index = product_list.findIndex(obj => obj.pro_id === pro_id);
                            if (index > -1) {
                                temp = 1;
                                product_list.push({
                                    pro_id: result.pro_id,
                                    sup_id: result.sup_id,
                                    color_id: result.color_id
                                });
                                product_list_table.push(column);
                            } else {
                                alert('Already in list. Try with another one');
                            }
                        } else {
                            temp = 1;
                            product_list.push({
                                pro_id: result.pro_id,
                                sup_id: result.sup_id,
                                color_id: result.color_id
                            });
                            product_list_table.push(column);
                        }
                    } else {
                        alert("With this color, product isn't purchased yet. Try again. ");
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    if (temp == 1) {
                        $('#sales_product').append(product_list_table);
                        $('#bar_code').prop('src',
                            "data:image/png;base64,<?= base64_encode($generator->getBarcode('+result.bar_code+', $generator::TYPE_CODE_128)) ?>"
                        );
                        console.log(barcode_print_role);
                    }
                }
            });
        });
        $('body').on('click', '#generate', function() {
            let print_data = {};
            print_data.role = barcode_print_role;
            print_data.product = product_list;
            let url = 'barcode/' + JSON.stringify(print_data);
            window.open(url, '_blank');
        });
    </script>
@endsection
