<?php
use App\Http\Controllers\Product\PurchaseList;
use App\Http\Controllers\Product\CategorySetup;
use App\Http\Controllers\Setting\BrandSetup;
use App\Http\Controllers\Setting\UnitSetup;
function unitName($id)
{
    $unit = UnitSetup::show($id);
    return $unit->name;
}

function product_stock($id)
{
    $stock = PurchaseList::stock($id);
    return $stock;
}
?>
@extends('layouts.app')
@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style scoped>
        h3 a {
            float: right;
            font-size: 15px;
            cursor: pointer;
        }
    </style>
@endsection
@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Product</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Product List</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-md-6">
                    <!-- Modal -->
                    <div class="modal fade" id="productViewModal" aria-hidden="true" style="overflow-x:hidden;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Product View</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body" id="product_view">
                                </div>
                                <div class="modal-footer" style="display:inline-block">
                                    <button form="category_edit" type="reset" class="btn btn-danger float-right"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mx-auto">
                    <div class="card p-3">
                        <label id="message" class="col-sm-12 col-form-label form-message"></label>
                        <div class="search col-xl-12 float-left px-3">
                            <select name="" id="">
                                <option value="30" selected>30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <input type="text" placeholder="Search...">
                            <div class="search_con">
                                <input type="date">
                                <span>To</span>
                                <input type="date">
                                <button class="btn-action btn-primary">
                                    <i class="lni lni-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @forelse ($products as $row => $v)
                                    <?php $cat_id = $v->cat_id;
                                    $brand_id = $v->brand_id;
                                    $group = CategorySetup::groupName($cat_id);
                                    $brand = BrandSetup::show($brand_id);
                                    $category = CategorySetup::show($cat_id);
                                    $colorArray = isset($v->colors) ? $v->colors : null;
                                    $colorString = $colorArray !== null ? implode(',', $colorArray) : 'No color found';
                                    ?>
                                    <div class="block-view">
                                        <div class="code-image">
                                            <label>#<?= $v->code ?></label>
                                            <img src="<?= asset("storage/$v->img_1") ?>"/>
                                        </div>
                                        <div class="product-info">
                                            <div class="product-head">
                                                <label><?= $v->name ?></label>
                                                <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i
                                                        class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                                <button data-id="<?= $v->id ?>" class="btn-action btn-warning edit"><i
                                                        class="lni lni-pencil-alt"
                                                        style="margin-right:6px"></i>Edit</button>
                                                <button data-id="<?= $v->id ?>" class="btn-action btn-primary view"><i
                                                        class="lni lni-eye" style="margin-right:6px"></i>View</button>
                                            </div>
                                            <div class="product-body">
                                                <div class="col-xl-6 float-left">
                                                    <label>
                                                        <span>Brand</span>
                                                        <span>:
                                                            <?= $brand !== null ? ucfirst($brand->name) : 'N/A' ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Group</span>
                                                        <span>: <?= $group !== null ? ucfirst($group) : 'N/A' ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Category</span>
                                                        <span>:
                                                            <?= $category !== null ? ucfirst($category->name) : 'N/A' ?></span>
                                                    </label>
                                                </div>
                                                <div class="col-xl-6 float-left">
                                                    <p>
                                                        <span>Color Code</span>
                                                        <span>: <?= $colorString . '.' ?></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <h3>No Product Found..<a href="{{ url('new-product') }}"><u>Add New</u></a></h3>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();

            $('body').on('click', '.view', function() {
                let url = 'product/view-product/' + $(this).data('id');
                $('#product_view').load(url, function() {
                    $('#productViewModal').modal('show');
                });
            });
        });
    </script>
@endsection
