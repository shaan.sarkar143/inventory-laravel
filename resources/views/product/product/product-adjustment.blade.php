<?php
use App\Http\Controllers\Product\ProductList;
use App\Http\Controllers\Product\PurchaseList;
use App\Http\Controllers\Product\CategorySetup;
use App\Http\Controllers\Setting\BrandSetup;
use App\Http\Controllers\Setting\UnitSetup;
function unitName($id)
{
    $unit = UnitSetup::show($id);
    return $unit->name;
}

function product_stock($id)
{
    $stock = PurchaseList::stock($id);
    return $stock;
}
$products = PurchaseList::productAdjustment();
$total = 100;
$half_init = $total / 30;
$half_val = intval($total / 30);
if ($half_init == $half_val) {
    $half = $half_val;
} else {
    $half = $half_val + 1;
}
$page_btn = [];
for ($p = 1; $p <= $half; $p++) {
    array_push($page_btn, $p);
}
$page_active = 1;
?>
@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .block-view .product-info .product-body label span:first-child {
            width: 120px !important;
            float: left;
        }

        h3 a {
            float: right;
            font-size: 15px;
            cursor: pointer;
        }

        .page_content {
            width: 100%;
            height: 100%;
            float: left;
            display: flex;
            flex-wrap: nowrap;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn {
            height: 30px;
            width: 30px;
            margin: 0 1px;
            background-color: #0073b6;
            color: #FFF;
            border-radius: 3px;
            border: none;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn i {
            font-size: 23px;
        }

        .page_content .page_active {
            background-color: #034165
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Product</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Product Adjustment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-md-6">
                    <!-- Modal -->
                    <div class="modal fade" id="exampleLargeModal" aria-hidden="true" style="overflow:hidden;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Adjust Info</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="col-lg-12 p-3" action="#" method="post" name="">
                                        <input type="hidden" id="pro_id" name="pro_id" />
                                        <input type="hidden" id="purs_id" name="purs_id" />
                                        <div class="row">
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Add Qty.</label>
                                                <input id="purs_qty" class="form-control mb-3" type="text"
                                                    placeholder="Add Qty" aria-label="Add Qty">
                                            </div>
                                            <div class="col-xl-6 float-right">
                                                <label class="col-sm-12 col-form-label">Current Purchase</label>
                                                <input id="curr_stock" class="form-control mb-3" type="text"
                                                    placeholder="Current Purchase" aria-label="Current Purchase" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer" style="display:inline-block">
                                    <label id="message" class="col-sm-6 col-form-label form-message float-left"></label>
                                    <button id="payment" type="button"
                                        class="btn btn-success float-right">Adjust</button>
                                    <button type="button" class="btn btn-danger float-right"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mx-auto">
                    <div class="card p-3">
                        <div class="search col-xl-12 float-left px-3">
                            <select name="" id="">
                                <option value="30" selected>30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <input type="text" placeholder="Search...">
                            <div class="search_con">
                                <input type="date">
                                <span>To</span>
                                <input type="date">
                                <button class="btn-action btn-primary">
                                    <i class="lni lni-search"></i>
                                </button>
                            </div>
                        </div>
                        {{-- <label id="message" class="col-sm-12 col-form-label form-message"></label> --}}
                        <div class="card-body">
                            <div class="table-responsive">
                                @forelse ($products as $v)
                                    <?php
                                    $pro_info = ProductList::show($v->pro_id);
                                    $cat_id = $pro_info->cat_id;
                                    $brand_id = $pro_info->brand_id;
                                    $group = CategorySetup::groupName($cat_id);
                                    $brand = BrandSetup::show($brand_id);
                                    $category = CategorySetup::show($cat_id);
                                    ?>
                                    <div class="block-view">
                                        <div class="code-image">
                                            <label>#{{ $pro_info->code }}</label>
                                            <img src="{{ asset("storage/$pro_info->img_1") }}" />
                                        </div>
                                        <div class="product-info">
                                            <div class="product-head">
                                                <label>{{ $v->name }}</label>
                                                <button data-id="{{ $v->id }}" data-pro="{{ $v->pro_id }}"
                                                    data-purs="{{ $v->purs_qty }}" data-sales="{{ $v->purs_qty }}"
                                                    data-from="product" data-bs-toggle="modal"
                                                    data-bs-target="#exampleLargeModal"
                                                    class="btn-action btn-warning adjust">
                                                    <i class="bx bx-arrow-to-right"
                                                        style="margin-right:6px"></i>Adjust</button>
                                                <button data-id="{{ $v->id }}" data-from="product"
                                                    class="btn-action btn-primary"><i class="lni lni-eye"
                                                        style="margin-right:6px"></i>View</button>
                                            </div>
                                            <div class="product-body">
                                                <div class="col-xl-7 float-left">
                                                    <label>
                                                        <span>Brand</span>
                                                        <span>: {{ ucfirst($brand->name) }}</span>
                                                    </label>
                                                    <label>
                                                        <span>Group</span>
                                                        <span>: {{ ucfirst($group) }}</span>
                                                    </label>
                                                    <label>
                                                        <span>Category</span>
                                                        <span>: {{ ucfirst($category->name) }}</span>
                                                    </label>
                                                </div>
                                                <div class="col-xl-3 float-left">
                                                    <label>
                                                        <span>Last Purs.</span>
                                                        <span>:
                                                            {{ $v->purs_qty . ' ' . strtoupper(unitName($v->purs_unit)) }}</span>
                                                    </label>
                                                    <label>
                                                        <span>Alert Qty.</span>
                                                        <span>:
                                                            {{ $v->alert_qty . ' ' . strtoupper(unitName($v->purs_unit)) }}</span>
                                                    </label>
                                                    <label>
                                                        <span>Stock Qty.</span>
                                                        <span>:
                                                            {{ $v->purs_qty . ' ' . strtoupper(unitName($v->purs_unit)) }}</span>
                                                    </label>
                                                </div>
                                                <div class="col-xl-2 float-right">
                                                    <label>
                                                        <span>Cost</span>
                                                        <span>{{ $cost = number_format($v->purchase_cost, 2, '.', '') }}</span>
                                                    </label>
                                                    <label>
                                                        <span>MRP</span>
                                                        <span>{{ $price = number_format($v->max_price, 2, '.', '') }}</span>
                                                    </label>
                                                    <label>
                                                        <span>Profit</span>
                                                        <span>{{ number_format($price - $cost, 2, '.', '') }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="col-xl-12">
                                        <label>No records found.</label>
                                    </div>
                                @endforelse
                                <table class="table col-xl-12 float-left">
                                    <tr>
                                        <td>
                                            <div class="page_content">
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-left" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-left" aria-hidden="true"></i>
                                                </button>
                                                <?php if (count($page_btn) <= 10) {
                                                        foreach ($page_btn as $btn) { ?>
                                                <button
                                                    class="page_btn <?= $page_active == $btn ? 'page_active' : '' ?>"><?= $btn ?></button>
                                                <?php }
                                                    } else {
                                                        for ($x = 0; $x < 10; $x++) { 
                                                ?>
                                                <button
                                                    class="page_btn <?= $page_active == $page_btn[$x] ? 'page_active' : '' ?>"><?= $page_btn[$x] ?></button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                </button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-right" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
