<?php 
    use \App\Http\Controllers\Product\CategorySetup;
    use \App\Http\Controllers\Product\ProductList;
    use \App\Http\Controllers\Setting\BrandSetup;
    use \App\Http\Controllers\Setting\ProductType;
    use \App\Http\Controllers\Setting\UnitSetup;
    use \App\Http\Controllers\Setting\TaxSetup;
    $category = CategorySetup::create();
    $brands = BrandSetup::create();
    $types = ProductType::create();
    $units = UnitSetup::create();
    $taxes = TaxSetup::create();
    $pro_code = ProductList::newCode();
?>
@extends("layouts.app")
  @section("style")
  <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
  <link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.min.css">
  @endsection
  @section("wrapper")
          <div class="page-wrapper">
              <div class="page-content">
                  <!--breadcrumb-->
                  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                      <div class="breadcrumb-title pe-3">Product</div>
                      <div class="ps-3">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                                  </li>
                                  <li class="breadcrumb-item active" aria-current="page">Add New Product</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
                  <!--end breadcrumb-->
                  <div class="row">
                      <div class="col-lg-12 mx-auto">
                          <div class="card p-3 pt-1">
                              <form class="row g-3" action="{{ route('product-list.store') }}" method="post" id="newCustomer" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>
                                  <div class="card-body col-xl-12 float-left">
                                      <div class="col-xl-6 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product Name</label>
                                          <input name="name" class="form-control mb-3" type="text" placeholder="Product Name" aria-label="Product Name">
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product Type</label>
                                          <select name="type" class="form-control mb-3 select_search" aria-label="Product Type">
                                              <option value="" selected disabled>Choose--</option>
                                              <?php foreach($types as $row => $v){?>
                                                  <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product Code</label>
                                          <input name="code" class="form-control mb-3" type="text" value="<?= $pro_code ?>" placeholder="Product Code" aria-label="Product Code" readonly>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Barcode Semiology</label>
                                          <input name="semiology" class="form-control mb-3" type="text" placeholder="Barcode Semilogy" aria-label="Barcode Semilogy">
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product Category</label>
                                          <select name="cat_id" id="cat_id" class="form-control mb-3 select_search" aria-label="Product Category">
                                              <option value="" selected disabled>Choose--</option>
                                              <?php foreach($category as $row => $v){?>
                                                  <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product Group</label>
                                          <input id="group_name" class="form-control mb-3" type="text" placeholder="Product Group" aria-label="Product Group" readonly>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product Brand</label>
                                          <select name="brand_id" class="form-control mb-3 select_search" aria-label="Product Brand">
                                              <option value="" selected disabled>Choose--</option>
                                              <?php foreach($brands as $row => $v){?>
                                                  <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-xl-6 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Color Name(Use comma ',' for adding multiple color name)</label>
                                          <input name="colors" class="form-control mb-3" type="text" placeholder="Color Name, ex. Black, Blue.." aria-label="Color Name">
                                      </div>
                                      <?php /*
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Purchase Qty.</label>
                                          <input name="purchase_qty" class="form-control mb-3" type="text" placeholder="Product Qty." aria-label="Product Qty.">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Purchase Unit</label>
                                          <select name="purchase_unit" class="form-control mb-3 select_search" aria-label="Product Category">
                                              <option value="" selected disabled>Choose--</option>
                                              <?php foreach($units as $row => $v){?>
                                                  <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Purchase Cost</label>
                                          <input name="purchase_cost" class="form-control mb-3" type="text" placeholder="Purchase Cost" aria-label="Purchase Cost">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Sale Qty.</label>
                                          <input name="sale_qty" class="form-control mb-3" type="text" placeholder="Sale Qty." aria-label="Sale Qty.">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Sale Unit</label>
                                          <select name="sale_unit" class="form-control mb-3 select_search" aria-label="Product Category">
                                              <option value="" selected disabled>Choose--</option>
                                              <?php foreach($units as $row => $v){?>
                                                  <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Min Price</label>
                                          <input name="min_price" class="form-control mb-3" type="text" placeholder="Min Price" aria-label="Min Price">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Max Price</label>
                                          <input name="max_price" class="form-control mb-3" type="text" placeholder="Max Price" aria-label="Max Price">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Alert Quantity</label>
                                          <input name="alert_qty" class="form-control mb-3" type="text" placeholder="Alert Qty." aria-label="Alert Qty.">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">TAX Type</label>
                                          <select name="tax_type" class="form-control mb-3 select_search" aria-label="Product Category">
                                              <option value="" selected disabled>Choose--</option>
                                              <?php foreach($taxes as $row => $v){?>
                                                  <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">TAX Amount</label>
                                          <input name="tax_amount" class="form-control mb-3" type="text" placeholder="TAX Amount" aria-label="TAX Amount">
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">BSTI Approved</label>
                                          <select name="bsti_approve" id="bsti_approve" class="form-control mb-3 select_search" aria-label="Product Category">
                                              <option value="" selected disabled>Choose--</option>
                                              <option value="1">Approved</option>
                                              <option value="0">Not Approved</option>
                                          </select>
                                      </div>
                                      <div class="col-xl-2 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">BDS No.</label>
                                          <input name="bds_no" id="bds_no" class="form-control mb-3" type="text" placeholder="BDS No." aria-label="BDS No." disabled>
                                      </div>
                                      */?>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Product View</label>
                                          <select name="view_in" class="form-control mb-3">
                                              <option value="1">Featured Product</option>
                                              <option value="2">Latest Product</option>
                                              <option value="3">Overseas Product</option>
                                          </select>
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Image 1</label>
                                          <input name="img_1" class="form-control mb-3" type="file" aria-label="Category Name">
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Image 2</label>
                                          <input name="img_2" class="form-control mb-3" type="file" aria-label="Category Name">
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Image 3</label>
                                          <input name="img_3" class="form-control mb-3" type="file" aria-label="Category Name">
                                      </div>
                                      <div class="col-xl-3 px-3 float-left">
                                          <label class="col-sm-12 col-form-label">Image 4</label>
                                          <input name="img_4" class="form-control mb-3" type="file" aria-label="Category Name">
                                      </div>
                                      <div class="col-xl-12 px-3 float-left">
                                          <textarea name="remarks" id="editor" class="form-control"></textarea>
                                      </div>
                                      <?php /*
                                      <div class="col-xl-12 px-3 float-left" style="margin-top:10px;">
                                          <div class="form-check form-switch">
                                              <input name="check_option[]" class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                              <label class="form-check-label" for="flexSwitchCheckChecked">Checked switch checkbox input</label>
                                          </div>
                                          <div class="form-check-danger form-check form-switch">
                                              <input name="check_option[]" class="form-check-input" type="checkbox" id="flexSwitchCheckCheckedDanger">
                                              <label class="form-check-label" for="flexSwitchCheckCheckedDanger">Checked switch checkbox input</label>
                                          </div>
                                          <div class="form-check form-switch">
                                              <input name="check_option[]" class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                              <label class="form-check-label" for="flexSwitchCheckChecked">Checked switch checkbox input</label>
                                          </div>
                                          <div class="form-check-danger form-check form-switch">
                                              <input name="check_option[]" class="form-check-input" type="checkbox" id="flexSwitchCheckCheckedDanger" checked>
                                              <label class="form-check-label" for="flexSwitchCheckCheckedDanger">Checked switch checkbox input</label>
                                          </div>
                                      </div>
                                      */?>
                                      <label id="message" class="col-sm-12 col-form-label form-message float-left"></label>
                                      <div class="col-xl-12 px-3 float-left">
                                          <div class="d-flex gap-2 mt-3 float-start">
                                              <a href="{{ url('product-list') }}" class="btn btn-primary"><i class="bx bx-list-ol"></i>Product List</a>
                                          </div>
                                          <div class="d-flex gap-2 mt-3 float-end">
                                              <button type="submit" class="btn btn-success"><i class="bx bx-list-plus"></i>Add</button>
                                          </div>
                                          <div class="d-flex gap-2 mt-3 float-end" style="margin-right:10px">
                                              <button type="reset" class="btn btn-danger"><i class="lni lni-eraser"></i>Clear</button>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
                  <!--end row-->
              </div>
          </div>
  @endsection

@section("script")
<script src="assets/plugins/input-tags/js/tagsinput.js"></script>
<script src="trumbowyg/dist/trumbowyg.min.js"></script>
<script>
    $('#editor').trumbowyg();
	$('.select_search').select2({
        height:'resolved'
    });
    $('body').on('change', '#cat_id', function ()
	{
		let data = {
			'cat_id': $(this).val()
		};
		let result = {};
		$.ajax({
			type: "GET",
			url: "{{ url('product-category/groups') }}",
			dataType: 'json',
            data: data,
			success: function (response)
			{
				result = response;
			},
			error: function (er)
			{
				// let error = v.responseText;
				console.log(er);
			},
			complete: function ()
			{
				$('#group_name').val(result.name);
			}
		});
	});
</script>
@endsection
