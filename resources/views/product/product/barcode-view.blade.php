<?php
use App\Http\Controllers\Product\ProductList;
require 'vendor/autoload.php';
$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
function pro_info($sup_id, $color_id, $pro_id)
{
    $pro_info = ProductList::info($sup_id, $color_id, $pro_id);
    return $pro_info;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .container {
            width: 120mm;
            height: auto;
            overflow: hidden;
            float: left;
            display: grid;
            grid-template-columns: auto auto;
        }

        .block {
            height: auto;
            overflow: hidden;
            margin: 5px;
            padding: 5px;
            position: relative;
        }

        .block img {
            width: 55mm;
            height: auto;
            float: left;
            margin: 4px 0;
        }

        .block label {
            width: 100%;
            float: left;
            font-size: 16px;
        }

        .block label:first-child {
            font-size: 18px;
        }

        .block span {
            position: absolute;
            left: 0;
            top: 0;
            z-index: -10;
            width: 100%;
            height: 78px;
        }

        .block span img {
            height: 100% !important;
            width: auto !important;
            float: right !important;
        }

        @media print {
            * {
                display: inline;
            }

            title,
            script,
            style {
                display: none;
            }

            .container {
                padding: 10px;
                break-inside: avoid;
                page-break-inside: avoid;
            }

            .block {
                break-inside: avoid;
                page-break-inside: avoid;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <?php
        $content = json_decode($pro_id);
        $design = $content->role;
        $width = 'width:' . $design->sticker_size . 'mm';
        $product = $content->product;
        ?>
        @foreach ($product as $pro)
            <?php for($x = 0; $x < $design->print_num; $x++){
                    $sup_id = $pro->sup_id;
                    $pro_id = $pro->pro_id;
                    $color_id = $pro->color_id;
                    $info = pro_info($sup_id, $color_id, $pro_id);
            ?>
            <div class="block" style="<?= $width ?>">
                <label><?= $info->name ?></label>
                <span>
                    <img src="<?= asset('storage/fkwTxFElCOJWKWg2mrcxO1X3QY48EK2WJgH7UOyo.jpg') ?>" alt="">
                </span>
                <label>MRP : <?= number_format($info->mrp, 2, '.', '') ?></label>
                <label>MFD : <?= date('d-m-Y', strtotime($info->mfd)) ?></label>
                <label>EXP : <?= date('d-m-Y', strtotime($info->exp)) ?></label>
                <?= '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($info->bar_code, $generator::TYPE_CODE_128)) . '">' ?>
            </div>
            <?php }?>
        @endforeach
    </div>
</body>

</html>
