<?php
$total = 100;
$half_init = $total / 30;
$half_val = intval($total / 30);
if ($half_init == $half_val) {
    $half = $half_val;
} else {
    $half = $half_val + 1;
}
$page_btn = [];
for ($p = 1; $p <= $half; $p++) {
    array_push($page_btn, $p);
}
$page_active = 1;
?>
@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .block-view .product-info .product-body label span:first-child {
            width: 120px !important;
            float: left;
        }

        h3 a {
            float: right;
            font-size: 15px;
            cursor: pointer;
        }

        .page_content {
            width: 100%;
            height: 100%;
            float: left;
            display: flex;
            flex-wrap: nowrap;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn {
            height: 30px;
            width: 30px;
            margin: 0 1px;
            background-color: #0073b6;
            color: #FFF;
            border-radius: 3px;
            border: none;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn i {
            font-size: 23px;
        }

        .page_content .page_active {
            background-color: #034165
        }

        .table tr th:nth-child(4),
        .table tr td:nth-child(4),
        .table tr th:nth-child(5),
        .table tr td:nth-child(5),
        .table tr th:nth-child(6),
        .table tr td:nth-child(6),
        .table tr th:nth-child(7),
        .table tr td:nth-child(7) {
            text-align: center;
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Product</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Product Stock</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-12 mx-auto">
                    <div class="card p-3">
                        <div class="search col-xl-12 float-left px-3">
                            <select name="" id="">
                                <option value="30" selected>30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <input type="text" placeholder="Search...">
                            <div class="search_con">
                                <input type="date">
                                <span>To</span>
                                <input type="date">
                                <button class="btn-action btn-primary">
                                    <i class="lni lni-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:50px">Sl.</th>
                                            <th style="width:150px">Product Color</th>
                                            <th>Product Info</th>
                                            <th style="width:100px">Min Qty.</th>
                                            <th style="width:100px">Last Purs.</th>
                                            <th style="width:100px">Last Sale</th>
                                            <th style="width:100px">Curr. Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($x = 1; $x <= 30; $x++){?>
                                        <tr>
                                            <td style="text-align:center"><?php echo $x; ?></td>
                                            <td style="width:150px;">Color_name</td>
                                            <td>
                                                <span class="float-left col-xl-12">#989009 : Product Name</span>
                                                <span class="float-left col-xl-5 span-bga">Brand</span>
                                                <span class="float-right col-xl-6 span-bgb"
                                                    style="text-align:right">Category</span>
                                            </td>
                                            <td>10</td>
                                            <td>100</td>
                                            <td>20</td>
                                            <td>90</td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                                <table class="table col-xl-12 float-left">
                                    <tr>
                                        <td>
                                            <div class="page_content">
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-left" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-left" aria-hidden="true"></i>
                                                </button>
                                                <?php if (count($page_btn) <= 10) {
                                                        foreach ($page_btn as $btn) { ?>
                                                <button
                                                    class="page_btn <?= $page_active == $btn ? 'page_active' : '' ?>"><?= $btn ?></button>
                                                <?php }
                                                    } else {
                                                        for ($x = 0; $x < 10; $x++) { 
                                                ?>
                                                <button
                                                    class="page_btn <?= $page_active == $page_btn[$x] ? 'page_active' : '' ?>"><?= $page_btn[$x] ?></button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                </button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-right" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
