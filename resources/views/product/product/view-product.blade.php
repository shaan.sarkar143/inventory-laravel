<?php 
    use \App\Http\Controllers\Product\CategorySetup;
    use \App\Http\Controllers\Product\ProductList;
    use \App\Http\Controllers\Setting\BrandSetup;
    use \App\Http\Controllers\Setting\ProductType;
    $product = ProductList::show($pro_id);
    $group_name = CategorySetup::groupName($product->cat_id);
    $types = ProductType::show($product->type);
    $category = CategorySetup::show($product->cat_id);
    $brands = BrandSetup::show($product->brand_id);
    $view_in = 'N/A';
    if($product->view_in == 1){
        $view_in = 'Featured Product';
    } else if($product->view_in == 2){
        $view_in = 'Latest Product';
    } else if($product->view_in == 3){
        $view_in = 'Overseas Product';
    }
    $colorString = ProductList::colors($pro_id);
?>
<div class="row">
    <div class="col-lg-12 mx-auto">
        <div class="row p-2">
            <h3 class="col-xl-12 float-left"><?= $product->name ?></h3>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Code</span>
                <span class="col-xl-8 float-left">: <?= $product->code ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Type</span>
                <span class="col-xl-8 float-left">: <?= $types->name ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Barcode Symbology</span>
                <span class="col-xl-8 float-left">: <?= $product->semiology ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Category</span>
                <span class="col-xl-8 float-left">: <?= ($category !== null) ? $category->name : 'N/A' ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Group</span>
                <span class="col-xl-8 float-left">: <?= $group_name ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Brand</span>
                <span class="col-xl-8 float-left">: <?= $brands->name ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">Color Code</span>
                <span class="col-xl-8 float-left">: <?= trim($colorString) ?></span>
            </label>
            <label class="col-xl-12 float-left" style="font-size:16px">
                <span class="col-xl-4 float-left">View Type</span>
                <span class="col-xl-8 float-left">: <?= $view_in ?></span>
            </label>
            <div class="col-xl-12 float-left mt-3">
                <div class="col-xl-6 float-left">
                    <img class="col-xl-12 float-left" src="<?= asset("storage/$product->img_1") ?>"/>
                </div>
                <div class="col-xl-6 float-left">
                    <img class="col-xl-12 float-left" src="<?= asset("storage/$product->img_1") ?>"/>
                </div>
                <div class="col-xl-6 float-left">
                    <img class="col-xl-12 float-left" src="<?= asset("storage/$product->img_1") ?>"/>
                </div>
                <div class="col-xl-6 float-left">
                    <img class="col-xl-12 float-left" src="<?= asset("storage/$product->img_1") ?>"/>
                </div>
            </div>
            <p class="col-xl-12 float-left mt-3"><?= $product->remarks ?></p>
        </div>
    </div>
</div>
