<?php 
    use \App\Http\Controllers\Setting\GroupSetup;
    use \App\Http\Controllers\Product\CategorySetup;
    $groups = GroupSetup::create();
    function group_name($id){
        $group = GroupSetup::show($id);
        return $group[0]->name;
    }
    
    $cat_info = CategorySetup::show($cat_id);
    $group_id = $cat_info->group_id; 
?>
<div class="row">
    <form class="col-lg-12" style="padding: 0 20px" action="{{ route('product-category.update', $cat_id) }}" id="category_edit" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-lg-6 float-left">
                <label for="inputPassword" class="col-sm-12 col-form-label">Group Name</label>
                <select name="group_id" class="form-control mb-3 group_select" aria-label="Group Name" required>
                    <option value="" selected disabled>Choose Group</option>
                <?php foreach($groups as $row => $v){?>
                    <option value="<?= $v->id ?>" <?= ($group_id == $v->id) ? 'selected':'' ?>><?= $v->name ?></option>
                <?php }?>
                </select>
            </div>
            <div class="col-lg-6 float-right">
                <label for="inputPassword" class="col-sm-12 col-form-label">Category Name</label>
                <input form="category_edit" name="cat_name" class="form-control mb-3" type="text" placeholder="Category Name" value="<?= $cat_info->name ?>" aria-label="Category Name" required>
            </div>
            <div class="col-lg-6 float-left">
                <label for="inputPassword" class="col-sm-12 col-form-label">Image</label>
                <input name="image" class="form-control mb-3" type="file" accept="image/jpeg" aria-label="Category Image">
            </div>
        </div>
    </form>
</div>
@section("script")
<script>
    // $(document).ready(function() {
        $('.group_select').select2({
            height:'resolved'
        });
    // });
</script>
@endsection