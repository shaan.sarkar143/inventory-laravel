<?php
$total = 100;
$half_init = $total / 30;
$half_val = intval($total / 30);
if ($half_init == $half_val) {
    $half = $half_val;
} else {
    $half = $half_val + 1;
}
$page_btn = [];
for ($p = 1; $p <= $half; $p++) {
    array_push($page_btn, $p);
}
$page_active = 1;
?>
@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .block-view .product-info .product-body label span:first-child {
            width: 120px !important;
            float: left;
        }

        h3 a {
            float: right;
            font-size: 15px;
            cursor: pointer;
        }

        .page_content {
            width: 100%;
            height: 100%;
            float: left;
            display: flex;
            flex-wrap: nowrap;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn {
            height: 30px;
            width: 30px;
            margin: 0 1px;
            background-color: #0073b6;
            color: #FFF;
            border-radius: 3px;
            border: none;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn i {
            font-size: 23px;
        }

        .page_content .page_active {
            background-color: #034165
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Purchase</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Purchase Return</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!-- <h6 class="mb-0 text-uppercase">DataTable Example</h6>
                                                                          <hr/> -->
            <div class="row">
                <div class="col-xl-12 mx-auto">
                    <div class="card p-3">
                        <div class="search col-xl-12 float-left px-3">
                            <select name="" id="">
                                <option value="30" selected>30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <input type="text" placeholder="Search...">
                            <div class="search_con">
                                <input type="date">
                                <span>To</span>
                                <input type="date">
                                <button class="btn-action btn-primary">
                                    <i class="lni lni-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <?php for($x = 1; $x <= 3; $x++){?>
                                <div class="block-view">
                                    <div class="code-image">
                                        <label
                                            style="height:53px;line-height:18px;padding:8px;text-align:left;">Invoice<br />#989009</label>
                                        <label
                                            style="height:53px;line-height:18px;padding:8px;top:unset;margin-top:4px;text-align:left;">Date<br />30-Dec-2021</label>
                                    </div>
                                    <div class="product-info">
                                        <div class="product-head">
                                            <label>Supplier Name</label>
                                            <button class="btn-action btn-danger"><i class="bx bx-redo"
                                                    style="margin-right:6px"></i>Return</button>
                                            <button class="btn-action btn-primary"><i class="lni lni-eye"
                                                    style="margin-right:6px"></i>View</button>
                                        </div>
                                        <div class="product-body">
                                            <div class="col-xl-6 float-left">
                                                <label>
                                                    <span>Phone</span>
                                                    <span>: 01923347911</span>
                                                </label>
                                                <label>
                                                    <span>Balance</span>
                                                    <span>: 5000.00</span>
                                                </label>
                                                <label>
                                                    <span>Status</span>
                                                    <span class="status btn-danger">Due</span>
                                                </label>
                                            </div>
                                            <div class="col-xl-4 float-left">
                                                <label>
                                                    <span>Products</span>
                                                    <span>: 10</span>
                                                </label>
                                                <label>
                                                    <span>Items</span>
                                                    <span>: 100</span>
                                                </label>
                                                <button class="btn-action btn-info float-left" style="padding:2px 10px;">
                                                    <i class="bx bx-list-ul" style="margin-right:6px"></i>Product List
                                                </button>
                                            </div>
                                            <div class="col-xl-2 float-right">
                                                <label>
                                                    <span>Cost</span>
                                                    <span>15000.00</span>
                                                </label>
                                                <label>
                                                    <span>Paid</span>
                                                    <span>10000.00</span>
                                                </label>
                                                <label>
                                                    <span>Rest</span>
                                                    <span>5000.00</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <table class="table col-xl-12 float-left">
                                    <tr>
                                        <td>
                                            <div class="page_content">
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-left" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-left" aria-hidden="true"></i>
                                                </button>
                                                <?php if (count($page_btn) <= 10) {
                                                        foreach ($page_btn as $btn) { ?>
                                                <button
                                                    class="page_btn <?= $page_active == $btn ? 'page_active' : '' ?>"><?= $btn ?></button>
                                                <?php }
                                                    } else {
                                                        for ($x = 0; $x < 10; $x++) { 
                                                ?>
                                                <button
                                                    class="page_btn <?= $page_active == $page_btn[$x] ? 'page_active' : '' ?>"><?= $page_btn[$x] ?></button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                </button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-right" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
