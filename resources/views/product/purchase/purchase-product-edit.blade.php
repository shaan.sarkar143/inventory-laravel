<?php
use App\Http\Controllers\Product\CategorySetup;
use App\Http\Controllers\Setting\BrandSetup;
use App\Http\Controllers\Supplier\SupplierList;
use App\Http\Controllers\Setting\UnitSetup;
use App\Http\Controllers\Setting\TaxSetup;
$category = CategorySetup::create();
$brands = BrandSetup::create();
$units = UnitSetup::create();
$taxes = TaxSetup::create();
$suppliers = SupplierList::create();
$taxes = TaxSetup::create();
$bsti_info = explode('@', $product->bsti_info);
$purs_tax = explode('@', $product->purs_tax);
$sale_tax = explode('@', $product->sale_tax);
?>
<form class="col-lg-12 p-3" action="{{ url('purchase-list/productAdd') }}" method="post" id="purchase_product"
    autocomplete="off">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>" />
    <input type="hidden" name="pro_id" id="pro_id" value="{{ $product->pro_id }}" />
    <input type="hidden" name="invoice" id="invoice" value="{{ $product->invoice }}" />
    <input type="hidden" name="supplier" id="supplier" value="{{ $product->sup_id }}" />
    <div class="row">
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Color Code</label>
            <select name="color_id" id="color_code" class="form-control mb-3 modal_search" aria-label="Color Code">
                <option value="" selected disabled>Choose--</option>
                @forelse ($colors as $v)
                    <option value="{{ $v->color }}" {{ $product->color_id == $v->id ? 'selected' : '' }}>
                        {{ $v->color }}
                    </option>
                @empty
                    <option value="0">No color</option>
                @endforelse
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Purchase Qty.</label>
            <input name="purchase_qty" class="form-control mb-3" type="text" placeholder="Product Qty."
                aria-label="Product Qty." value="{{ $product->purs_qty }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Purchase Unit</label>
            <select name="purchase_unit" class="form-control mb-3 modal_search" aria-label="Product Category">
                <option value="" selected disabled>Choose--</option>
                <?php foreach($units as $row => $v){?>
                <option value="<?= $v->id ?>" {{ $product->purs_unit == $v->id ? 'selected' : '' }}><?= $v->name ?>
                </option>
                <?php }?>
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Purchase Cost/Unit</label>
            <input name="purchase_cost" class="form-control mb-3" type="text" placeholder="Purchase Cost"
                aria-label="Purchase Cost" value="{{ $product->purs_cost }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Purchase TAX Type</label>
            <select name="purse_tax_type" class="form-control mb-3 modal_search" aria-label="Purchase TAX Type">
                <option value="" selected disabled>Choose--</option>
                <?php foreach($taxes as $row => $v){?>
                <option value="<?= $v->id ?>" {{ $purs_tax[0] == $v->id ? 'selected' : '' }}><?= $v->name ?></option>
                <?php }?>
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Purchase TAX Amount</label>
            <input name="purse_tax_amount" class="form-control mb-3" type="text" placeholder="Purchase TAX Amount"
                aria-label="Purchase TAX Amount" value="{{ $purs_tax[1] }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Alert Quantity</label>
            <input name="alert_qty" class="form-control mb-3" type="text" placeholder="Alert Qty."
                aria-label="Alert Qty." value="{{ $product->alert_qty }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Sale Qty.</label>
            <input name="sale_qty" class="form-control mb-3" type="text" placeholder="Sale Qty."
                aria-label="Sale Qty." value="{{ $product->sale_qty }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Sale Unit</label>
            <select name="sale_unit" class="form-control mb-3 modal_search" aria-label="Product Category">
                <option value="" selected disabled>Choose--</option>
                <?php foreach($units as $row => $v){?>
                <option value="<?= $v->id ?>"><?= $v->name ?></option>
                <?php }?>
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Wholesale Price</label>
            <input name="sale_min" class="form-control mb-3" type="text" placeholder="Min Price"
                aria-label="Min Price" value="{{ $product->sale_min }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Retail Price</label>
            <input name="sale_max" class="form-control mb-3" type="text" placeholder="Max Price"
                aria-label="Max Price" value="{{ $product->sale_max }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Sale TAX Type</label>
            <select name="tax_type" class="form-control mb-3 modal_search" aria-label="Product Category">
                <option value="" selected disabled>Choose--</option>
                <?php foreach($taxes as $row => $v){?>
                <option value="<?= $v->id ?>" {{ $sale_tax[0] == $v->id ? 'selected' : '' }}><?= $v->name ?></option>
                <?php }?>
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Sale TAX Amount</label>
            <input name="tax_amount" class="form-control mb-3" type="text" placeholder="TAX Amount"
                aria-label="TAX Amount" value="{{ $sale_tax[1] }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">BSTI Approved</label>
            <select name="bsti_approve" id="bsti_approve" class="form-control mb-3 modal_search"
                aria-label="Product Category">
                <option value="" selected disabled>Choose--</option>
                <option value="1" {{ $bsti_info[0] == '1' ? 'selected' : '' }}>Approved</option>
                <option value="0" {{ $bsti_info[0] == '0' ? 'selected' : '' }}>Not Approved</option>
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">BDS No.</label>
            <input name="bds_no" id="bds_no" class="form-control mb-3" type="text" placeholder="BDS No."
                aria-label="BDS No." disabled value="{{ $bsti_info[1] }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Sale Discount</label>
            <select name="discount_type" class="form-control mb-3 modal_search" aria-label="Sale Discount">
                <option value="0" {{ $product->discount_type == '0' ? 'selected' : '' }}>No Discount</option>
                <option value="1" {{ $product->discount_type == '1' ? 'selected' : '' }}>Cash Reduce</option>
                <option value="2" {{ $product->discount_type == '2' ? 'selected' : '' }}>Percentage</option>
            </select>
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">Discount Amount</label>
            <input name="sale_disc" class="form-control mb-3" type="number" value="0"
                placeholder="Discount Amount" aria-label="Discount Amount" value="{{ $product->sale_disc }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">MFD.</label>
            <input name="mfd" class="form-control mb-3" type="date" value="{{ $product->mfd }}" />
        </div>
        <div class="col-xl-4 px-3 float-left">
            <label class="col-sm-12 col-form-label">EXP.</label>
            <input name="exp" class="form-control mb-3" type="date" value="{{ $product->exp }}" />
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('select:not(.normal)').each(function() {
            $(this).select2({
                dropdownParent: $(this).parent(),
                height: 'resolved',
                width: '100%'
            });
        });
    });
</script>
