<?php
use App\Http\Controllers\Supplier\SupplierList;
use App\Http\Controllers\Product\PurchaseList;
function supplier_info($id)
{
    return SupplierList::show($id);
}
function product_info($invoice, $supplier)
{
    return PurchaseList::productInfo($invoice, $supplier);
}
$total = 100;
$half_init = $total / 30;
$half_val = intval($total / 30);
if ($half_init == $half_val) {
    $half = $half_val;
} else {
    $half = $half_val + 1;
}
$page_btn = [];
for ($p = 1; $p <= $half; $p++) {
    array_push($page_btn, $p);
}
$page_active = 1;
?>
@extends('layouts.app')

@section('style')
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    <style>
        .block-view .product-info .product-body label span:first-child {
            width: 120px !important;
            float: left;
        }

        h3 a {
            float: right;
            font-size: 15px;
            cursor: pointer;
        }

        .page_content {
            width: 100%;
            height: 100%;
            float: left;
            display: flex;
            flex-wrap: nowrap;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn {
            height: 30px;
            width: 30px;
            margin: 0 1px;
            background-color: #0073b6;
            color: #FFF;
            border-radius: 3px;
            border: none;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .page_content .page_btn i {
            font-size: 23px;
        }

        .page_content .page_active {
            background-color: #034165
        }
    </style>
@endsection

@section('wrapper')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Purchase</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Purchase List</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mx-auto">
                    <div class="card p-3">
                        <label id="message" class="col-sm-12 col-form-label form-message"></label>
                        <div class="search col-xl-12 float-left px-3">
                            <select name="" id="">
                                <option value="30" selected>30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <input type="text" placeholder="Search...">
                            <div class="search_con">
                                <input type="date">
                                <span>To</span>
                                <input type="date">
                                <button class="btn-action btn-primary">
                                    <i class="lni lni-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @forelse ($purchase as $row => $v)
                                    <?php
                                    $supplier = supplier_info($v->supplier);
                                    if ($v->rest_amount < 0) {
                                        $status = 'class="status btn-danger"';
                                    } else {
                                        $status = '';
                                    }
                                    $product_info = product_info($v->invoice, $v->supplier);
                                    $product_info = $product_info['total'];
                                    ?>
                                    <div class="block-view">
                                        <div class="code-image">
                                            <label
                                                style="height:53px;line-height:18px;padding:8px;text-align:left;">Invoice<br />#<?= $v->invoice ?></label>
                                            <label
                                                style="height:53px;line-height:18px;padding:8px;top:unset;margin-top:4px;text-align:left;">Date<br /><?= date('d-M-Y', strtotime($v->date)) ?></label>
                                        </div>
                                        <div class="product-info">
                                            <div class="product-head">
                                                <label><?= $supplier->name ?></label>
                                                <button data-id="<?= $v->id ?>" class="btn-action btn-danger erase"><i
                                                        class="lni lni-trash" style="margin-right:6px"></i>Erase</button>
                                                <button class="btn-action btn-warning"><i class="lni lni-pencil-alt"
                                                        style="margin-right:6px"></i>Edit</button>
                                                <button class="btn-action btn-primary"><i class="lni lni-eye"
                                                        style="margin-right:6px"></i>View</button>
                                            </div>
                                            <div class="product-body">
                                                <div class="col-xl-6 float-left">
                                                    <label>
                                                        <span>Phone</span>
                                                        <span>: <?= $supplier->phone ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Email</span>
                                                        <span>: <?= $supplier->email ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Balance</span>
                                                        <span>: <?= $supplier->balance ?></span>
                                                    </label>
                                                </div>
                                                <div class="col-xl-4 float-left">
                                                    <label>
                                                        <span>Total Products</span>
                                                        <span>: <?= $product_info['totalProduct'] ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Total Items</span>
                                                        <span>: <?= $product_info['totalItems'] ?></span>
                                                    </label>
                                                    <button class="btn-action btn-info float-left"
                                                        style="padding:2px 10px;">
                                                        <i class="bx bx-list-ul" style="margin-right:6px"></i>Product List
                                                    </button>
                                                </div>
                                                <div class="col-xl-2 float-right">
                                                    <label>
                                                        <span>Total Cost</span>
                                                        <span><?= number_format((float) $v->total_cost, 2, '.', '') ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Total Paid</span>
                                                        <span><?= number_format((float) $v->total_paid, 2, '.', '') ?></span>
                                                    </label>
                                                    <label>
                                                        <span>Invoice Due</span>
                                                        <span
                                                            <?= $status ?>><?= $v->rest_amount < 0 ? number_format((float) $v->rest_amount, 2, '.', '') : '0.00' ?></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <h3>No Purchase record Found..<a href="{{ url('new-purchase') }}">
                                            <u>Add New</u></a>
                                    </h3>
                                @endforelse
                                <table class="table col-xl-12 float-left">
                                    <tr>
                                        <td>
                                            <div class="page_content">
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-left" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-left" aria-hidden="true"></i>
                                                </button>
                                                <?php if (count($page_btn) <= 10) {
                                                        foreach ($page_btn as $btn) { ?>
                                                <button
                                                    class="page_btn <?= $page_active == $btn ? 'page_active' : '' ?>"><?= $btn ?></button>
                                                <?php }
                                                    } else {
                                                        for ($x = 0; $x < 10; $x++) { 
                                                ?>
                                                <button
                                                    class="page_btn <?= $page_active == $page_btn[$x] ? 'page_active' : '' ?>"><?= $page_btn[$x] ?></button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                </button>
                                                <?php } ?>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevron-right" aria-hidden="true"></i>
                                                </button>
                                                <button class="page_btn">
                                                    <i class="bx bx-chevrons-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end page wrapper -->
@endsection

@section('script')
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
        $('body').on('click', '.erase', function() {
            let data = {
                "id": $(this).data('id'),
                "_token": '{{ csrf_token() }}'
            };
            let result = {};
            let con = confirm("Are you sure ?");
            if (con == true) {
                $.ajax({
                    type: "DELETE",
                    url: "purchase-list/" + $(this).data('id'),
                    dataType: 'json',
                    data: data,
                    success: function(response) {
                        result = response;
                        console.log(result);
                        $('html, body').animate({
                            scrollTop: $("#message").offset().top
                        }, 200);
                        if (result.status == 1) {
                            curr_class = 'btn-success';
                            $("#message").addClass('btn-success');
                        } else if (result.status == 0) {
                            curr_class = 'btn-danger';
                            $("#message").addClass('btn-danger');
                        }
                        setTimeout(function() {
                            removeClass('message', curr_class)
                            $('#message').html("");
                        }, 1500);
                        if (result.load) {
                            setTimeout(function() {
                                location.replace(result.load);
                            }, 2000);
                        }
                    },
                    error: function(er) {
                        console.log(er);
                    },
                    complete: function() {
                        $('#message').html(result.message);
                    }
                });
            }
        });
    </script>
@endsection
