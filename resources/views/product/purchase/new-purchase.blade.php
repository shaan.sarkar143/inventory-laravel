<?php
use App\Http\Controllers\Product\CategorySetup;
use App\Http\Controllers\Setting\BrandSetup;
use App\Http\Controllers\Supplier\SupplierList;
use App\Http\Controllers\Setting\UnitSetup;
use App\Http\Controllers\Setting\TaxSetup;
use App\Http\Controllers\Product\PurchaseList;
$category = CategorySetup::create();
$brands = BrandSetup::create();
$units = UnitSetup::create();
$taxes = TaxSetup::create();
$suppliers = SupplierList::create();
$taxes = TaxSetup::create();
$invoices = PurchaseList::invoiceForPurchase();
?>
@extends('layouts.app')
@section('style')
    <link href="assets/plugins/input-tags/css/tagsinput.css" rel="stylesheet" />
    <link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.min.css">
    <style>
        * ul li {
            list-style: none !important;
        }

        * ul {
            margin: 0 !important;
            padding: 0 !important;
        }

        * ul li span {
            width: 49%;
            overflow: hidden;
        }

        * ul li span:first-child {
            float: left;
            text-align: left
        }

        * ul li span:last-child {
            float: right;
            text-align: right;
            word-break: break-all;
        }

        .add_button {
            width: 35px;
            height: 30px;
            position: absolute;
            right: 17px;
            top: 0px;
        }

        .custome_modal {
            display: none;
            width: 100%;
            height: 100%;
            background-color: #AAA3;
            z-index: 99999;
            position: fixed;
            left: 0;
            top: 0;
        }

        .custome_modal .modal_content {
            width: 96%;
            max-width: 800px;
            height: 90%;
            background-color: #FFF;
            margin: 5% auto;
            overflow: hidden;
        }

        .custome_modal .modal_content .modal_head {
            width: 100%;
            height: 60px;
            box-shadow: 2px 2px 10px #ddd, -2px -2px 10px #666;
        }

        .custome_modal .modal_content .modal_head h5 {
            height: 100%;
            width: auto;
            float: left;
            line-height: 60px;
            padding-left: 20px;
        }

        .custome_modal .modal_content .modal_head button {
            float: right;
            height: 100%;
            width: 60px;
            border: 0;
            padding: 0;
            background: transparent;
            font-size: 20px;
            color: #fe3838;
        }
    </style>
@endsection
@section('wrapper')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Purchase</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New Purchase</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="modal fade" id="invoiceForm" tabindex="-1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-sm" style="width:400px;max-width:unset;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Invoice No.</h5>
                        </div>
                        <div class="modal-body">
                            <input id="inv_number" class="form-control mb-3" type="number"
                                placeholder="Enter purchase invoice no." />
                            <label id="add_message" class="col-sm-12 col-form-label form-message float-left"></label>
                        </div>
                        <div class="modal-footer">
                            <button id="addModal_close" type="button" class="btn btn-danger"
                                data-bs-dismiss="modal">Close</button>
                            <button id="invoice_setup" type="button" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="custome_modal">
                <div class="modal_content">
                    <div class="modal_head">
                        <h5 class="modal-title">Purchase product update</h5>
                        <button type="button">
                            <i class="lni lni-close"></i>
                        </button>
                    </div>
                    <div class="modal_body"></div>
                    <div class="modal_footer"></div>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    <div class="card p-3 pt-1">
                        <form class="row g-3" action="{{ route('purchase-list.store') }}" method="post" id="newPurchase"
                            autocomplete="off">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>" />
                            <div class="card-body col-xl-12 float-left">
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Purchase Date</label>
                                    <input name="date" class="form-control mb-3" type="date"
                                        value="<?= date('Y-m-d') ?>" aria-label="Purchase Date">
                                </div>
                                <div class="col-xl-3 px-3 float-left" style="position: relative">
                                    <label class="col-sm-12 col-form-label">Invoice No.</label>
                                    <button type="button" data-bs-toggle="modal" data-bs-target="#invoiceForm"
                                        class="btn-action btn-primary add_button"><i class="lni lni-plus"
                                            style="font-weight:600"></i></button>
                                    <select id="invoice_no" name="invoice" class="form-control mb-3 select_search"
                                        aria-label="Invoice No.">
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($invoices as $row => $v){?>
                                        <option value="<?= $v->invoice ?>"><?= $v->invoice ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Select Supplier</label>
                                    <select id="supp_id" name="supplier" class="form-control mb-3 select_search"
                                        aria-label="Select Supplier">
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($suppliers as $row => $v){?>
                                        <option value="<?= $v->id ?>">
                                            <?= ucfirst(strtolower($v->fname)) . ' ' . ucfirst(strtolower($v->lname)) . ' # ' . $v->phone ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Supplier Balance</label>
                                    <input id="supp_bal" class="form-control mb-3" type="text" placeholder="0.00"
                                        aria-label="Supplier Balance" readonly>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Category</label>
                                    <select id="cat_id" class="form-control mb-3 select_search"
                                        aria-label="Product Category" disabled>
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($category as $row => $v){?>
                                        <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Brand</label>
                                    <select id="brand_id" class="form-control mb-3 select_search"
                                        aria-label="Product Brand" disabled>
                                        <option value="" selected disabled>Choose--</option>
                                        <?php foreach($brands as $row => $v){?>
                                        <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xl-6 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Choose Product</label>
                                    {{-- <input class="form-control mb-3" type="text" placeholder="Search Product with Code/Name" aria-label="Search Product"> --}}
                                    <select id="products" class="form-control mb-3 select_search"
                                        aria-label="Choose Product" disabled></select>
                                </div>
                                <div class="col-xl-12 px-3 float-left" style="margin-top:20px;">
                                    <div class="table-responsive">
                                        <table id="product_list" class="table table-striped table-bordered"
                                            style="width:100%">
                                            <thead>
                                                <tr style="background-color: #ddd">
                                                    <th style="width:50px">Sl.</th>
                                                    <th style="width:130px;">Color</th>
                                                    <th>Product Info</th>
                                                    <th style="width:200px;">Purchase Info</th>
                                                    <th style="width:200px;">Sale Info</th>
                                                    <th style="width:200px;">Other Info</th>
                                                    <th style="width:130px;text-align:right;">Amount</th>
                                                    <th style="width:100px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="6" style="text-align:right">Total</td>
                                                    <td id="totalAmount" style="text-align:right"></td>
                                                    <td style="width:100px;">
                                                        <div class="row row-cols-auto g-3 mx-auto flex-center float-left"
                                                            style="width:100px;">
                                                            <button id="erase_all" type="button"
                                                                class="btn-action btn-danger"><i class="lni lni-trash"
                                                                    style="margin-right:3px"></i>Remove All</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Grand Total</label>
                                    <input id="totalCost" name="totalCost" class="form-control mb-3" type="text"
                                        placeholder="0.00" aria-label="Grand Total" readonly>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Total Paid</label>
                                    <input id="total_paid" name="total_paid" class="form-control mb-3" type="text"
                                        placeholder="0.00" aria-label="Total Paid">
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Due / Advanced</label>
                                    <input id="due_adv" name="due_adv" class="form-control mb-3" type="text"
                                        placeholder="0.00" aria-label="Due / Advanced" readonly>
                                </div>
                                <div class="col-xl-3 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Current Balance</label>
                                    <input id="curr_bal" name="curr_bal" class="form-control mb-3" type="text"
                                        placeholder="0.00" aria-label="Sup. Balance" readonly>
                                </div>
                                <div class="col-xl-12 px-3 float-left">
                                    <label class="col-sm-12 col-form-label">Remarks</label>
                                    <textarea name="remarks" class="form-control mb-3"></textarea>
                                </div>
                                <label id="message" class="col-sm-12 col-form-label form-message float-left"></label>
                                <div class="col-xl-12 px-3 float-left">
                                    <div class="d-flex gap-2 mt-3 float-start">
                                        <a href="{{ url('purchase-list') }}" class="btn btn-warning"><i
                                                class="bx bx-list-ol"></i>Purchase List</a>
                                    </div>
                                    <div class="d-flex gap-2 mt-3 float-end">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="bx bx-list-plus"></i>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- Modal -->
                    <div class="modal fade" id="productModal" aria-hidden="true" style="overflow-x:hidden;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Product Info : <span id="pro_name"></span></h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="col-lg-12 p-3" action="{{ url('purchase-list/productAdd') }}"
                                        method="post" id="purchase_product" autocomplete="off">
                                        <input type="hidden" name="_token" value="<?= csrf_token() ?>" />
                                        <input type="hidden" name="pro_id" id="pro_id" />
                                        <input type="hidden" name="invoice" id="invoice" />
                                        <input type="hidden" name="supplier" id="supplier" />
                                        <div class="row">
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Color Code</label>
                                                <select name="color_id" id="color_code"
                                                    class="form-control mb-3 modal_search"
                                                    aria-label="Color Code"></select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Purchase Qty.</label>
                                                <input name="purchase_qty" class="form-control mb-3" type="text"
                                                    placeholder="Product Qty." aria-label="Product Qty.">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Purchase Unit</label>
                                                <select name="purchase_unit" class="form-control mb-3 modal_search"
                                                    aria-label="Product Category">
                                                    <option value="" selected disabled>Choose--</option>
                                                    <?php foreach($units as $row => $v){?>
                                                    <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Purchase Cost/Unit</label>
                                                <input name="purchase_cost" class="form-control mb-3" type="text"
                                                    placeholder="Purchase Cost" aria-label="Purchase Cost">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Purchase TAX Type</label>
                                                <select name="purse_tax_type" class="form-control mb-3 modal_search"
                                                    aria-label="Purchase TAX Type">
                                                    <option value="" selected disabled>Choose--</option>
                                                    <?php foreach($taxes as $row => $v){?>
                                                    <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Purchase TAX Amount</label>
                                                <input name="purse_tax_amount" class="form-control mb-3" type="text"
                                                    placeholder="Purchase TAX Amount" aria-label="Purchase TAX Amount">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Alert Quantity</label>
                                                <input name="alert_qty" class="form-control mb-3" type="text"
                                                    placeholder="Alert Qty." aria-label="Alert Qty.">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Sale Qty.</label>
                                                <input name="sale_qty" class="form-control mb-3" type="text"
                                                    placeholder="Sale Qty." aria-label="Sale Qty.">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Sale Unit</label>
                                                <select name="sale_unit" class="form-control mb-3 modal_search"
                                                    aria-label="Product Category">
                                                    <option value="" selected disabled>Choose--</option>
                                                    <?php foreach($units as $row => $v){?>
                                                    <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Wholesale Price</label>
                                                <input name="sale_min" class="form-control mb-3" type="text"
                                                    placeholder="Min Price" aria-label="Min Price">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Retail Price</label>
                                                <input name="sale_max" class="form-control mb-3" type="text"
                                                    placeholder="Max Price" aria-label="Max Price">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Sale TAX Type</label>
                                                <select name="tax_type" class="form-control mb-3 modal_search"
                                                    aria-label="Product Category">
                                                    <option value="" selected disabled>Choose--</option>
                                                    <?php foreach($taxes as $row => $v){?>
                                                    <option value="<?= $v->id ?>"><?= $v->name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Sale TAX Amount</label>
                                                <input name="tax_amount" class="form-control mb-3" type="text"
                                                    placeholder="TAX Amount" aria-label="TAX Amount">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">BSTI Approved</label>
                                                <select name="bsti_approve" id="bsti_approve"
                                                    class="form-control mb-3 modal_search" aria-label="Product Category">
                                                    <option value="" selected disabled>Choose--</option>
                                                    <option value="1">Approved</option>
                                                    <option value="0">Not Approved</option>
                                                </select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">BDS No.</label>
                                                <input name="bds_no" id="bds_no" class="form-control mb-3"
                                                    type="text" placeholder="BDS No." aria-label="BDS No." disabled>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Sale Discount</label>
                                                <select name="discount_type" class="form-control mb-3 modal_search"
                                                    aria-label="Sale Discount">
                                                    <option value="0" selected>No Discount</option>
                                                    <option value="1">Cash Reduce</option>
                                                    <option value="2">Percentage</option>
                                                </select>
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">Discount Amount</label>
                                                <input name="sale_disc" class="form-control mb-3" type="number"
                                                    value="0" placeholder="Discount Amount"
                                                    aria-label="Discount Amount">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">MFD.</label>
                                                <input name="mfd" class="form-control mb-3" type="date"
                                                    placeholder="Discount Amount" aria-label="Discount Amount">
                                            </div>
                                            <div class="col-xl-4 px-3 float-left">
                                                <label class="col-sm-12 col-form-label">EXP.</label>
                                                <input name="exp" class="form-control mb-3" type="date"
                                                    placeholder="Discount Amount" aria-label="Discount Amount">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer" style="display:inline-block">
                                    <button form="purchase_product" type="submit"
                                        class="btn btn-primary float-right">Add</button>
                                    <button form="purchase_product" type="reset" class="btn btn-danger float-right"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- Modal -->
                    <div class="modal fade" id="productEditModal" aria-hidden="true" tabindex="-1"
                        style="display: none;overflow-x:hidden;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Purchase product update</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body" id="edit_group">
                                </div>
                                <div class="modal-footer" style="display:inline-block">
                                    <button form="group_edit_form" type="submit"
                                        class="btn btn-primary float-right">Update</button>
                                    <button form="group_edit_form" type="reset" class="btn btn-danger float-right"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
@endsection

@section('script')
    <script src="assets/plugins/input-tags/js/tagsinput.js"></script>
    <script src="trumbowyg/dist/trumbowyg.min.js"></script>
    <script>
        $(document).ready(function() {
            $('select:not(.normal)').each(function() {
                $(this).select2({
                    dropdownParent: $(this).parent(),
                    height: 'resolved',
                    width: '100%'
                });
            });
        });

        $('body').on('click', '#invoice_setup', function() {
            let result = {};
            let options = '<option value="" selected disabled>Choose--</option>';
            let data = {
                'invoice': $('#inv_number').val()
            };
            $.ajax({
                type: "GET",
                url: "{{ url('purchase-list/invoiceAdd') }}",
                dataType: 'json',
                data: data,
                cache: false,
                success: function(response) {
                    result = response;
                    $.each(result.invoices, function(i, value) {
                        options += '<option value="' + value.invoice + '">' + value.invoice +
                            '</option>';
                    });
                },
                error: function(er) {
                    let error = JSON.parse(v.responseText);
                    console.log(error);
                },
                complete: function() {
                    $('#add_message').html(result.message);
                    $('#invoice_no').html(options);
                    setTimeout(() => {
                        $('#inv_number').val('');
                        $('#add_message').html('');
                        $('#addModal_close').trigger('click');
                    }, 1000);
                }
            });
        });

        $('body').on('change', '#supp_id', function() {
            let url = 'supplier-list/' + $(this).val(); //show resource
            let result = {};
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function(response) {
                    result = response;
                    $('#cat_id').prop('disabled', false);
                    $('#brand_id').prop('disabled', false);
                },
                error: function(er) {
                    console.log(er);
                    $('#cat_id').prop('disabled', true);
                    $('#brand_id').prop('disabled', true);
                },
                complete: function() {
                    $('#supp_bal').val(parseFloat(result.balance).toFixed(2));
                    $('#curr_bal').val(parseFloat(result.balance).toFixed(2));
                }
            });
        });
        $('body').on('change', '#cat_id', function() {
            let data = {};
            if ($('#brand_id').val() != null) {
                data = {
                    'cat_id': $(this).val(),
                    'brand': $('#brand_id').val()
                };
            } else {
                data = {
                    'cat_id': $(this).val(),
                    'brand': 0
                };
            }
            let result = {};
            let items = '<option value="" selected disabled>Choose--</option>';
            $.ajax({
                type: "GET",
                url: "{{ url('product-list/catWiseProduct') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    $.each(result, function(i, val) {
                        items += '<option value="' + val.id + '">' + val.name + '</option>';
                    });
                    if (result.length > 0) {
                        $('#products').prop('disabled', false);
                    } else {
                        $('#products').prop('disabled', true);
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#products').html(items);
                }
            });
        });
        $('body').on('change', '#brand_id', function() {
            let data = {};
            if ($('#cat_id').val() != null) {
                data = {
                    'brand': $(this).val(),
                    'cat_id': $('#cat_id').val()
                };
            } else {
                data = {
                    'brand': $(this).val(),
                    'cat_id': 0
                };
            }
            let result = {};
            let items = '<option value="" selected disabled>Choose--</option>';
            $.ajax({
                type: "GET",
                url: "{{ url('product-list/catWiseProduct') }}",
                dataType: 'json',
                data: data,
                success: function(response) {
                    result = response;
                    $.each(result, function(i, val) {
                        items += '<option value="' + val.id + '">' + val.name + '</option>';
                    });
                    if (result.length > 0) {
                        $('#products').prop('disabled', false);
                    } else {
                        $('#products').prop('disabled', true);
                    }
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#products').html(items);
                }
            });
        });
        $('body').on('input', '#invoice_no', function() {
            $('#invoice').val($(this).val());
        });
        $('body').on('change', '#products', function() {
            $('#pro_id').val($(this).val());
            $('#pro_name').html($('#products option:selected').text());
            $('#supplier').val($('#supp_id').val());
            let invoice = $('#invoice_no').val();
            var result = '',
                items = '';
            if (invoice != '') {
                let data = {
                    'id': $(this).val()
                };
                $.ajax({
                    type: "GET",
                    url: "{{ url('purchase-list/productColor') }}",
                    dataType: 'json',
                    data: data,
                    cache: false,
                    success: function(response) {
                        result = response;
                        $.each(result, function(i, val) {
                            items += '<option value="' + val.id + '">' + val.color +
                                '</option>';
                        });
                        $('#color_code').html(items);
                        $('#invoice').val();
                    },
                    error: function(er) {
                        console.log(er);
                    },
                    complete: function() {
                        $('#productModal').modal('show');
                    }
                });
            } else {
                alert('Add invoice number first.');
                $(this).val('');
            }
        });
        $('body').on('change', '#bsti_approve', function() {
            if ($(this).val() == 1) {
                $('#bds_no').prop('disabled', false);
            } else {
                $('#bds_no').prop('disabled', true);
            }
        });
        $('body').on('click', '.pro_edit', function() {
            let url = 'purchase-list/edit-product/';
            url = url + $(this).data('row');
            $('#edit_group').load(url, function() {
                $('#productEditModal').modal('show');
            });

        });
        $('body').on('click', '.pro_erase', function() {
            var row_id = $(this).data('row');
            let data = {
                'row_id': row_id
            };
            console.log(data);
            $.ajax({
                type: "GET",
                url: "{{ url('purchase-list/productRemove') }}",
                dataType: 'json',
                data: data,
                cache: false,
                success: function(response) {
                    result = response.total;
                    console.log(result);
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#totalAmount').html(parseFloat(result.grand_total).toFixed(2));
                    $('#totalCost').val(parseFloat(result.grand_total).toFixed(2));
                    $('#row_id_' + row_id).remove();
                }
            });
        });
        $('body').on('click', '#erase_all', function() {
            var row_id = $(this).data('row');
            let data = {
                'invoice': $('#invoice_no').val(),
                'supplier': $('#supp_id').val(),
            };
            console.log(data);
            $.ajax({
                type: "GET",
                url: "{{ url('purchase-list/productRemoveAll') }}",
                dataType: 'json',
                data: data,
                cache: false,
                success: function(response) {
                    result = response.total;
                    console.log(result);
                },
                error: function(er) {
                    console.log(er);
                },
                complete: function() {
                    $('#totalQty').html(result.totalQty);
                    $('#totalCost').val(result.totalCost);
                    $('#totalDisc').html(result.totalDisc);
                    $("#product_list > tbody").html("");
                    $('#total_paid').val(0);
                    $('#due_adv').val(0);
                    $('#curr_bal').val(parseFloat($('#supp_bal').val()).toFixed(2));
                }
            });
        });
        $('body').on('input', '#total_paid', function() {
            var total_paid = $(this).val();
            var totalCost = $('#totalCost').val();
            var supp_bal = $('#supp_bal').val();
            var due_adv = 0;
            var curr_bal = 0;
            due_adv = parseFloat(total_paid) - parseFloat(totalCost);
            curr_bal = parseFloat(supp_bal) + parseFloat(due_adv);
            $('#due_adv').val(parseFloat(due_adv).toFixed(2));
            $('#curr_bal').val(parseFloat(curr_bal).toFixed(2));
        });
    </script>
@endsection
