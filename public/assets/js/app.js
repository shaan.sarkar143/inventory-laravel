$(function ()
{
	"use strict";
	/* perfect scrol bar */
	// new PerfectScrollbar('.header-message-list');
	// new PerfectScrollbar('.header-notifications-list');
	// search bar
	$(".mobile-search-icon").on("click", function ()
	{
		$(".search-bar").addClass("full-search-bar");
		$(".page-wrapper").addClass("search-overlay");
	});
	$(".search-close").on("click", function ()
	{
		$(".search-bar").removeClass("full-search-bar");
		$(".page-wrapper").removeClass("search-overlay");
	});
	$(".mobile-toggle-menu").on("click", function ()
	{
		$(".wrapper").addClass("toggled");
	});
	// toggle menu button
	$(".toggle-icon").click(function ()
	{
		if ($(".wrapper").hasClass("toggled")) {
			// unpin sidebar when hovered
			$(".wrapper").removeClass("toggled");
			$(".sidebar-wrapper").unbind("hover");
		} else {
			$(".wrapper").addClass("toggled");
			$(".sidebar-wrapper").hover(function ()
			{
				$(".wrapper").addClass("sidebar-hovered");
			}, function ()
			{
				$(".wrapper").removeClass("sidebar-hovered");
			})
		}
	});
	/* Back To Top */
	$(document).ready(function ()
	{
		$(window).on("scroll", function ()
		{
			if ($(this).scrollTop() > 300) {
				$('.back-to-top').fadeIn();
			} else {
				$('.back-to-top').fadeOut();
			}
		});
		$('.back-to-top').on("click", function ()
		{
			$("html, body").animate({
				scrollTop: 0
			}, 600);
			return false;
		});
	});
	// === sidebar menu activation js
	$(function ()
	{
		for (var i = window.location, o = $(".metismenu li a").filter(function ()
		{
			return this.href == i;
		}).addClass("").parent().addClass("mm-active"); ;) {
			if (!o.is("li")) break;
			o = o.parent("").addClass("mm-show").parent("").addClass("mm-active");
		}
	});
	// metismenu
	$(function ()
	{
		$('#menu').metisMenu();
	});
	// chat toggle
	$(".chat-toggle-btn").on("click", function ()
	{
		$(".chat-wrapper").toggleClass("chat-toggled");
	});
	$(".chat-toggle-btn-mobile").on("click", function ()
	{
		$(".chat-wrapper").removeClass("chat-toggled");
	});
	// email toggle
	$(".email-toggle-btn").on("click", function ()
	{
		$(".email-wrapper").toggleClass("email-toggled");
	});
	$(".email-toggle-btn-mobile").on("click", function ()
	{
		$(".email-wrapper").removeClass("email-toggled");
	});
	// compose mail
	$(".compose-mail-btn").on("click", function ()
	{
		$(".compose-mail-popup").show();
	});
	$(".compose-mail-close").on("click", function ()
	{
		$(".compose-mail-popup").hide();
	});
	/*switcher*/
	$(".switcher-btn").on("click", function ()
	{
		$(".switcher-wrapper").toggleClass("switcher-toggled");
	});
	$(".close-switcher").on("click", function ()
	{
		$(".switcher-wrapper").removeClass("switcher-toggled");
	});
	$("#lightmode").on("click", function ()
	{
		$('html').attr('class', 'light-theme');
	});
	$("#darkmode").on("click", function ()
	{
		$('html').attr('class', 'dark-theme');
	});
	$("#semidark").on("click", function ()
	{
		$('html').attr('class', 'semi-dark');
	});
	$("#minimaltheme").on("click", function ()
	{
		$('html').attr('class', 'minimal-theme');
	});
	$("#headercolor1").on("click", function ()
	{
		$("html").addClass("color-header headercolor1");
		$("html").removeClass("headercolor2 headercolor3 headercolor4 headercolor5 headercolor6 headercolor7 headercolor8");
	});
	$("#headercolor2").on("click", function ()
	{
		$("html").addClass("color-header headercolor2");
		$("html").removeClass("headercolor1 headercolor3 headercolor4 headercolor5 headercolor6 headercolor7 headercolor8");
	});
	$("#headercolor3").on("click", function ()
	{
		$("html").addClass("color-header headercolor3");
		$("html").removeClass("headercolor1 headercolor2 headercolor4 headercolor5 headercolor6 headercolor7 headercolor8");
	});
	$("#headercolor4").on("click", function ()
	{
		$("html").addClass("color-header headercolor4");
		$("html").removeClass("headercolor1 headercolor2 headercolor3 headercolor5 headercolor6 headercolor7 headercolor8");
	});
	$("#headercolor5").on("click", function ()
	{
		$("html").addClass("color-header headercolor5");
		$("html").removeClass("headercolor1 headercolor2 headercolor4 headercolor3 headercolor6 headercolor7 headercolor8");
	});
	$("#headercolor6").on("click", function ()
	{
		$("html").addClass("color-header headercolor6");
		$("html").removeClass("headercolor1 headercolor2 headercolor4 headercolor5 headercolor3 headercolor7 headercolor8");
	});
	$("#headercolor7").on("click", function ()
	{
		$("html").addClass("color-header headercolor7");
		$("html").removeClass("headercolor1 headercolor2 headercolor4 headercolor5 headercolor6 headercolor3 headercolor8");
	});
	$("#headercolor8").on("click", function ()
	{
		$("html").addClass("color-header headercolor8");
		$("html").removeClass("headercolor1 headercolor2 headercolor4 headercolor5 headercolor6 headercolor7 headercolor3");
	});



	// sidebar colors 


	$('#sidebarcolor1').click(theme1);
	$('#sidebarcolor2').click(theme2);
	$('#sidebarcolor3').click(theme3);
	$('#sidebarcolor4').click(theme4);
	$('#sidebarcolor5').click(theme5);
	$('#sidebarcolor6').click(theme6);
	$('#sidebarcolor7').click(theme7);
	$('#sidebarcolor8').click(theme8);

	function theme1()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor1');
	}

	function theme2()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor2');
	}

	function theme3()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor3');
	}

	function theme4()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor4');
	}

	function theme5()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor5');
	}

	function theme6()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor6');
	}

	function theme7()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor7');
	}

	function theme8()
	{
		$('html').attr('class', 'color-sidebar sidebarcolor8');
	}

	function removeClass(id, class_name)
	{
		$('#' + id).removeClass(class_name);
	}

	$('body').on('submit', 'form', function (e)
	{
		e.preventDefault();
		var formId = $(this).attr('id');
		var loader = '#loader';
		let url = $(this).attr('action'), type = 'POST';
		var form_data = new FormData(document.getElementById(formId));
		var result = '', curr_class = '', formhide = '0';
		if (typeof $(this).data('formhide') !== 'undefined') {
			formhide = $(this).data('formhide');
		}
		if ($(this).attr('method')) {
			type = $(this).attr('method');
		}
		$.ajax({
			type: type,
			enctype: 'multipart/form-data',
			url: url,
			data: form_data,
			contentType: false,
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			cache: false,
			processData: false,
			beforeSend: function (v)
			{
				$('#message').html('Processing...');
			},
			success: function (response)
			{
				// console.log(response);
				result = response;
				if (result.status == 1) {
					curr_class = 'btn-success';
					$("#message").addClass('btn-success');
					$('#' + formId).trigger("reset");
				} else if (result.status == 0) {
					curr_class = 'btn-danger';
					$("#message").addClass('btn-danger');
				}

				setTimeout(function ()
				{
					removeClass('message', curr_class)
					$('#message').html("");
				}, 1000);

				if (result.modal) {
					$('#' + result.modal).modal('hide');
				}

				if (formhide !== '0') {
					setTimeout(function ()
					{
						$('#' + formhide).hide(1000);
					}, 1300);
				}

				if (result.load) {
					setTimeout(function ()
					{
						location.replace(result.load);
					}, 1500);
				}

				if (result.table) {
					matchedTable(result.table, result.data);
				}
			},
			error: function (v)
			{
				let error = JSON.parse(v.responseText);
				$("#message").addClass('btn-danger');
				$('#message').html(error.message + ' Try Again.');
				setTimeout(function ()
				{
					removeClass('message', 'btn-danger')
					$('#message').html("");
				}, 1500);
			},
			complete: function ()
			{
				$('#message').html(result.message);
				$(loader).hide();
			}
		});
		e.preventDefault();
	});

	$('body').on('click', '.erase', function ()
	{
		let data = {
			"id": $(this).data('id'),
		};
		let result = {};
		let con = confirm("Are you sure ?");
		var url = window.location.pathname;
		url = url.replace('/laravel/inventory/', '');// For local only
		url = url + '/' + $(this).data('id');
		var curr_class = '';
		if (con == true) {
			$.ajax({
				type: "DELETE",
				url: url,
				dataType: 'json',
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				data: data,
				success: function (response)
				{
					result = response;
					console.log(result);
					$('html, body').animate({
						scrollTop: $("#message").offset().top
					}, 200);
					if (result.status == 1) {
						curr_class = 'btn-success';
						$("#message").addClass('btn-success');
					} else if (result.status == 0) {
						curr_class = 'btn-danger';
						$("#message").addClass('btn-danger');
					}
					setTimeout(function ()
					{
						removeClass('message', curr_class)
						$('#message').html("");
					}, 1500);
					if (result.load) {
						setTimeout(function ()
						{
							location.replace(result.load);
						}, 2000);
					}
				},
				error: function (er)
				{
					console.log(er);
				},
				complete: function ()
				{
					$('#message').html(result.message);
				}
			});
		}
	});

	$('body').on('click', '.edit', function ()
	{
		var url = window.location.pathname;
		// url = url.replace('laravel/inventory/', '');// For local only
		url = url + '/' + $(this).data('id') + '/edit';
		location.replace(url);
	});

	$('body').on('click', '.adjust', function ()
	{

	});

	function matchedTable(id, dataList)
	{
		let htmlCon = '';
		var data = dataList.product;
		if (id == 'product_list') {
			var x = 1;
			console.log(data);
			$.each(data, function (i)
			{
				htmlCon += '<tr id="row_id_' + data[i].row_id + '" style="--bs-table-accent-bg: #fff">';
				htmlCon += '<td style="width:50px">' + x + '</td>';
				htmlCon += '<td style="width:130px;"> ' + data[i].pro_color + '</td>';
				htmlCon += '<td> ' + data[i].pro_info.name + '</td>';
				htmlCon += '<td style="width:230px;"><ul>';
				htmlCon += '<li><span>Qty</span><span>' + data[i].purs_qty + ' ' + data[i].purs_units + '</span></li>';
				htmlCon += '<li><span>Cost</span><span>' + parseFloat(data[i].purs_cost).toFixed(2) + '/' + data[i].purs_units + '</span></li>';
				htmlCon += '<li><span>Tax</span><span>' + data[i].purs_tax_amount + ' ' + data[i].purs_tax_title + '</span></li>';
				htmlCon += '</ul></td>';
				htmlCon += '<td style="width:230px;"><ul>';
				htmlCon += '<li><span>Qty</span><span>' + data[i].sale_qty + ' ' + data[i].sale_units + '</span></li>';
				htmlCon += '<li><span>Price</span><span>' + parseFloat(data[i].sale_min).toFixed(2) + ' - ' + parseFloat(data[i].sale_max).toFixed(2) + '</span><li>';
				htmlCon += '<li><span>Tax</span><span>' + data[i].sale_tax_amount + ' ' + data[i].sale_tax_title + '</span></li>';
				htmlCon += '</ul></td>';
				htmlCon += '<td style="width:200px;"><ul>';
				htmlCon += '<li><span>Discount</span><span>' + data[i].sale_disc + '</span></li>';
				htmlCon += '<li><span>MFD</span><span>' + data[i].mfd + '</span></li>';
				htmlCon += '<li><span>EXP</span><span>' + data[i].exp + '</span></li>';
				htmlCon += '</ul></td>';
				htmlCon += '<td style="width:130px;text-align:right;">' + parseFloat(data[i].total_cost).toFixed(2) + '</td>';
				htmlCon += '<td style="width:100px;"><div class="row row-cols-auto g-3 mx-auto flex-center float-left" style="width:100px;">';
				htmlCon += '<button data-row="' + data[i].row_id + '" type="button" class="pro_edit btn-action btn-warning"> <i class="lni lni-pencil" style="margin-right:3px"></i></button>';
				htmlCon += '<button data-row="' + data[i].row_id + '" type="button" class="pro_erase btn-action btn-danger"> <i class="lni lni-trash" style="margin-right:3px"></i></button>';
				htmlCon += '</div ></td > ';
				htmlCon += '</tr>';
				x++;
			});
			$('#totalAmount').html(parseFloat(dataList.grand_total).toFixed(2));
			$('#totalCost').val(parseFloat(dataList.grand_total).toFixed(2));
			$('#product_list >tbody').html(htmlCon);
		}
		// console.log(data);
	}

});
